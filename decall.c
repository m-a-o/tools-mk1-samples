/*	Delayed call.
	Author: Miguel A. Osorio - Dec 25 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#ifdef MK1_ENABLE_EXTRA_CHECKS
#include <assert.h>
#endif

#include "d_array.h"
#include "custom_types.h"
#include "vdata.h"
#include "a_packer.h"
#include "mk1_aligned_alloc.h"
#include "decall.h"




/* Delayed call compilers */
#define DC_COMPILER_MIN_PAGE_SIZE 1024
struct dcc_cmd {
	va_size_t index;
	void* store;
};

struct dc_compiler {
	page_allocator_t pool;
	va_compiler_t bc;
	d_array_t staged_cmds;
};

static void dcc_delete_store(dc_compiler_t dcc)
{
	page_allocator_delete(dcc->pool);
	va_compiler_delete(dcc->bc);
	da_delete(dcc->staged_cmds);
}

static int dcc_init_store(dc_compiler_t dcc)
{
	dcc->pool = page_allocator_create(DC_COMPILER_MIN_PAGE_SIZE);
	dcc->bc = va_compiler_create(va_decl_pack_size_align_pair());
	dcc->staged_cmds = da_create(1, sizeof(struct dcc_cmd));
	if((dcc->pool != NULL) && (dcc->bc != NULL) && (dcc->staged_cmds != NULL))
		return 1;

	dcc_delete_store(dcc);

	return 0;
}

dc_compiler_t dc_compiler_create(void)
{
	dc_compiler_t dcc = (dc_compiler_t)malloc(sizeof(struct dc_compiler));

	if((dcc != NULL) && dcc_init_store(dcc))
		return dcc;

	dc_compiler_delete(dcc);

	return NULL;
}

void dc_compiler_delete(dc_compiler_t dcc)
{
	if(dcc != NULL) {
		dcc_delete_store(dcc);
		free(dcc);
	}
}

struct dc_ep {
	dc_exec_proc cmd;
};
DEFINE_ALIGNOF_STRUCT(dc_ep);

union dc_cmd_offsets {
	struct {
		va_size_t ep;
		va_size_t args;
	} item;
	va_size_t buf[2];
};

struct dc_cmd_stat {
	struct size_align_pair sz_al;
	union dc_cmd_offsets offsets;
};

static struct dc_cmd_stat dc_cmd_stat(const struct size_align_pair args_sz_al)
{
	static const struct size_align_pair cmd_ep_sz_al = { sizeof(struct dc_ep), ALIGNOF(dc_ep) };
	const struct va_decl_pack op_pack = va_decl_pack_size_align_pair();
	struct size_align_pair cmd_vas[2];
	struct dc_cmd_stat stat;

	cmd_vas[0] = cmd_ep_sz_al;
	cmd_vas[1] = args_sz_al;

	stat.sz_al = sizealignof_vas(cmd_vas, 2, op_pack);
	offsetsof_vas(stat.offsets.buf, cmd_vas, 2, op_pack);

	return stat;
}

static void* dc_cmd_args(void* cmd, const struct dc_cmd_stat cmd_stat)
{
	return (char*)cmd + cmd_stat.offsets.item.args;
}

/* Command args in dc compiler */
void* dcc_add_cmd(const dc_exec_proc cmd, const struct size_align_pair args_sz_al, dc_compiler_t dcc)
{
	struct dcc_cmd staged_cmd = { VA_INVALID_INDEX, NULL };
	const struct dc_cmd_stat cmd_stat = dc_cmd_stat(args_sz_al);

	staged_cmd.index = va_compiler_add(&(cmd_stat.sz_al), 1, dcc->bc);
	if(staged_cmd.index != VA_INVALID_INDEX) {
		if((staged_cmd.store = paged_alloc(cmd_stat.sz_al.size, cmd_stat.sz_al.align, dcc->pool)) != NULL) {
			((struct dc_ep*)staged_cmd.store)->cmd = cmd;
			if(da_add(&staged_cmd, dcc->staged_cmds) == DA_OK)
				return dc_cmd_args(staged_cmd.store, cmd_stat);
		}
	}

	return NULL;
}

int dc_stream_exec(vec_array_t dc)
{
	if(dc != NULL) {
		const struct size_align_pair* cmd_vas = va_desc_vas((va_desc_t)dc);
		va_size_t i, num_cmds = va_desc_vas_size((va_desc_t)dc);

		for(i = 0; i < num_cmds; ++i) {
			struct dc_ep* ep = (struct dc_ep*)vec_array_get(i, dc);

			ep->cmd(dc_cmd_args(ep, dc_cmd_stat(cmd_vas[i])));
		}

		return 1;
	}

	return 0;
}

vec_array_t dc_compile(dc_compiler_t dcc)
{
	vec_array_t stream = NULL;

	if((dcc != NULL) && ((stream = va_compile(dcc->bc)) != NULL)) {
		mem_int_t i, num_staged_cmds = da_num_items(dcc->staged_cmds);
		struct dcc_cmd* staged_cmds = da_buffer(dcc->staged_cmds);
		const struct size_align_pair* cmd_decls = (struct size_align_pair*)va_desc_vas((va_desc_t)stream);

		for(i = 0; i < num_staged_cmds; ++i)
			memcpy(vec_array_get(staged_cmds[i].index, stream), staged_cmds[i].store, cmd_decls[staged_cmds[i].index].size);
	}

	return stream;
}

static void dc_inline_exec(void* args)
{
	dc_stream_exec((vec_array_t)args);
}

void dcc_stream_cat(const vec_array_t dc, dc_compiler_t dcc)
{
	const struct size_align_pair dc_sz_al = sizealignof_vec_array(dc);
	vec_array_t staged_stream = dcc_add_cmd(dc_inline_exec, dc_sz_al, dcc);

	if(staged_stream != NULL)
		memcpy(staged_stream, dc, dc_sz_al.size);
}

void dc_compiler_reset(dc_compiler_t dcc)
{
	if(dcc != NULL) {
		page_allocator_reset(dcc->pool);
		va_compiler_reset(dcc->bc);
		da_clear(dcc->staged_cmds);
	}
}

