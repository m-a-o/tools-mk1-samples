/*	3D Coordinate frames.
	Author: Miguel A. Osorio - Feb 27 2017
*/

/*
	Copyright (c) 2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#include <math.h>

#include "v.h"
#include "cframe.h"




void cframe3_default(cframe3* cf3)
{
	vZero3D(cf3->pos);
	vSetIdentityQuaternion(cf3->rot);
	vSet3D(cf3->scale, 1.0f, 1.0f, 1.0f);
}

void cframe3_to_m4(const cframe3* cf3, Vmatrix4x4 M)
{
	/* Rotation */
	vQuaternionToSubMatrix4x3(cf3->rot, M);
	/* Translation */
	vSet4D(&(M[_L_]), cf3->pos[_X_], cf3->pos[_Y_], cf3->pos[_Z_], 1.0f);
	/* Scale */
	{
		Vmatrix4x4 S;

		vSetIdentityMatrix4x4(S);
		S[0] = cf3->scale[_X_];
		S[5] = cf3->scale[_Y_];
		S[10] = cf3->scale[_Z_];

		vMultMatrix4x4(M, S, M);
	}
}

static void m4_transpose_rot3x3(Vmatrix4x4 M)
{
	Vfloat t;

	t = M[1]; M[1] = M[4]; M[4] = t;
	t = M[2]; M[2] = M[8]; M[8] = t;
	t = M[6]; M[6] = M[9]; M[9] = t;
}

static void m4_rot_transform_v3D(const Vmatrix4x4 M, const Vvector3D v, Vvector3D res)
{
	Vvector4D r;

	r[0] = M[0] * v[0];
	r[1] = M[1] * v[0];
	r[2] = M[2] * v[0];

	r[0] += M[4] * v[1];
	r[1] += M[5] * v[1];
	r[2] += M[6] * v[1];

	r[0] += M[8] * v[2];
	r[1] += M[9] * v[2];
	r[2] += M[10] * v[2];

	vCopy3D(res, r);
}

void cframe3_to_m4_inv(const cframe3* cf3, Vmatrix4x4 M)
{
	Vvector3D t;

	/* Rotation */
	vQuaternionToSubMatrix4x3(cf3->rot, M);
	m4_transpose_rot3x3(M);
	m4_rot_transform_v3D(M, cf3->pos, t);
	/* Translation */
	vSet4D(&(M[_L_]), -t[_X_], -t[_Y_], -t[_Z_], 1.0f);
	/* Scale */
	{
		Vmatrix4x4 S;

		vSetIdentityMatrix4x4(S);
		S[0] = 1.0f / cf3->scale[_X_];
		S[5] = 1.0f / cf3->scale[_Y_];
		S[10] = 1.0f / cf3->scale[_Z_];

		vMultMatrix4x4(S, M, M);
	}
}

void cframe3_from_m4_noscale(const Vmatrix4x4 M, cframe3* cf3)
{
}

void cframe3_YPR(const Vfloat yaw, const Vfloat pitch, const Vfloat roll, cframe3* cf3)
{
	/* Half-angles */
	Vfloat half_yaw = vDegToRad(yaw * 0.5f);
	Vfloat half_pitch = vDegToRad(pitch * 0.5f);
	Vfloat half_roll = vDegToRad(roll * 0.5f);
	Vquaternion u;

	/* Accumulate rotations in main quaternion */
	/* Yaw */
	u[_X_] = 0.0f;
	u[_Y_] = sinf(-half_yaw);
	u[_Z_] = 0.0f;
	u[_W_] = cosf(-half_yaw);
	vMultQuaternion(u, cf3->rot, cf3->rot);

	/* Pitch */
	u[_X_] = sinf(-half_pitch);
	u[_Y_] = 0.0f;
	u[_Z_] = 0.0f;
	u[_W_] = cosf(-half_pitch);
	vMultQuaternion(u, cf3->rot, cf3->rot);

	/* Roll */
	u[_X_] = 0.0f;
	u[_Y_] = 0.0f;
	u[_Z_] = sinf(-half_roll);
	u[_W_] = cosf(-half_roll);
	vMultQuaternion(u, cf3->rot, cf3->rot);
}

void cframe3_get_axes(cframe3_axes* axes, const cframe3* cf3)
{
	Vmatrix4x4 R;

	vQuaternionToSubMatrix4x3(cf3->rot, R);

	vCopy3D(axes->right, &(R[_I_]));
	vCopy3D(axes->up, &(R[_J_]));
	vNeg3D(&(R[_K_]), axes->fwd);
}

void cframe3_set_axes(const cframe3_axes* axes, cframe3* cf3)
{
	Vmatrix4x4 R;

	vCopy3D(&(R[_I_]), axes->right);
	vCopy3D(&(R[_J_]), axes->up);
	vNeg3D(axes->fwd, &(R[_K_]));

	vMatrixToQuaternion(R, cf3->rot);
}

