/*	Delayed call.
	Author: Miguel A. Osorio - Dec 25 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#ifndef _DECALL_H_
#define _DECALL_H_

#ifdef __cplusplus
extern "C" {
#endif


/* cmd args */
#define DC_ARGS_NAME(cmd) cmd##_data
#define DC_ARGS_DEF(cmd) struct DC_ARGS_NAME(cmd)
#define DC_ARGS_ALIGNOF_DEF(cmd) DEFINE_ALIGNOF_STRUCT(DC_ARGS_NAME(cmd))
#define DC_ARGS_DEF_START(cmd) DC_ARGS_DEF(cmd) {
#define DC_ARGS_DEF_END(cmd) }; DC_ARGS_ALIGNOF_DEF(cmd)

/* cmd ep */
#define DC_ARGS(args, cmd) DC_ARGS_DEF(cmd)* args = (DC_ARGS_DEF(cmd)*)cmd_data
#define DC_EP_NAME(cmd) cmd##_EP
#define DC_EP_DEF(cmd) static void DC_EP_NAME(cmd)(void* cmd_data)

/* cmd spec */
#define DC_ARGS_SZ_AL_DEF(cmd) { sizeof(DC_ARGS_DEF(cmd)), ALIGNOF(DC_ARGS_NAME(cmd)) }
#define DC_ARGS_INSTANCE(args, cmd, args_sz_al, dcc) DC_ARGS_DEF(cmd)* args = dcc_add_cmd(DC_EP_NAME(cmd), args_sz_al, dcc)


typedef struct dc_compiler* dc_compiler_t;

/* Exec */
typedef void (*dc_exec_proc)(void* args);

/* Spec (type) */
typedef int (*dc_spec_proc)(void* data, dc_compiler_t dcc);

/* DCC2 */
void* dcc_add_cmd(const dc_exec_proc cmd, const struct size_align_pair args_sz_al, dc_compiler_t dcc);

/* DC streams */
dc_compiler_t dc_compiler_create(void);
void dc_compiler_delete(dc_compiler_t dcc);

void dcc_stream_cat(const vec_array_t dc, dc_compiler_t dcc);

vec_array_t dc_compile(dc_compiler_t dcc);
int dc_stream_exec(vec_array_t dc);
void dc_compiler_reset(dc_compiler_t dcc);

#ifdef __cplusplus
}
#endif

#endif

