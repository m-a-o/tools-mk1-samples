/*
	-=- Farm parallelism pattern tests -=-
*/

/*
	Copyright (c) 2010-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


#ifndef __FARM_H__
#define __FARM_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Farm execution options */
#define FARM_ISSET(options, flag) (((options) & (flag)) == (flag))

#define FARM_OPT_NO_WAIT (1 << 0)
#define FARM_OPT_PAYLOAD_REPLICATE (1 << 1)

/* Synch apparatus */
typedef struct farm_sync farm_sync_t;

/* Create farm coordination */
farm_sync_t* farm_create(int num_jobs);
void farm_delete(farm_sync_t* f_sync);

/* A single job instance - id's range from 0 to (num_jobs - 1) */
typedef int (*farm_job_proc)(int id, void* data);

/* A farm payload - this format supports a unique task for each worker */
typedef struct farm_payload {

	void* data;
	farm_job_proc job;

} farm_payload_t;

/* Farm status */
typedef enum {

	FARM_STAT_UNDEFINED = 0,
	FARM_STAT_SETUP,
	FARM_STAT_WORKING,
	FARM_STAT_DONE

} farm_status_t;

/*
	Launch farm. Defaults to FARM_OPT_WAIT. If you choose not to wait for farm
	work completion, note that it is only safe to release the payload buffer
	once any subsequent call to farm_check() returns STAT_WORKING!
*/
int farm_start(farm_sync_t* f_sync, farm_payload_t* payload, int options);
int farm_go(farm_sync_t* f_sync, void* data, farm_job_proc job, int options);

/*
	Check on farm. This function should never block. If the return value is
	STAT_UNDEFINED, try checking again at a later time. Note that farm status
	can only go from state to state in the following order:

		STAT_SETUP -> STAT_WORKING -> STAT_DONE

	So after this function returns, the current status might already have changed,
	but it will never be some previous state.
*/
farm_status_t farm_check(farm_sync_t* f_sync);

/*
	Wait for an async farm to complete.
*/
void farm_wait_for(farm_sync_t* f_sync);

/* Classic fork-join */
void fork_join(int n_jobs, void* data, farm_job_proc job, int options);

/* Parallel-for */
typedef int (*parallel_for_proc)(mem_int_t i, void* item, const void* state);	/* Procedure for single item */

void parallel_for(int n_jobs, mem_int_t n, void* items, size_t item_size, void* state, parallel_for_proc proc);



#ifdef __cplusplus
};
#endif

#endif

