/*	Custom types with alignment.
	Author: Miguel A. Osorio - Mar 26 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


/*
	TODO:
*/



#ifndef _TOOLKIT_TYPES_ALIGN_H_
#define _TOOLKIT_TYPES_ALIGN_H_

#ifdef __cplusplus
extern "C" {
#endif


#include "custom_types.h"
#include "a_packer.h"

/* Alignments */
DEFINE_ALIGNOF_TYPE(int_8);
DEFINE_ALIGNOF_TYPE(uint_8);
DEFINE_ALIGNOF_TYPE(int_16);
DEFINE_ALIGNOF_TYPE(uint_16);
DEFINE_ALIGNOF_TYPE(int_32);
DEFINE_ALIGNOF_TYPE(uint_32);

DEFINE_ALIGNOF_TYPE(float_32);
DEFINE_ALIGNOF_TYPE(float_64);


#ifdef __cplusplus
}
#endif

#endif

