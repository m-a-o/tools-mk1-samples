/*	Custom types
	Author: Miguel A. Osorio - Sep 05 2014
*/

/*
	Copyright (c) 2014-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


/*
	TODO:
*/



#ifndef _TOOLKIT_TYPES_H_
#define _TOOLKIT_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif


/* Sized types */
typedef signed char int_8;
typedef unsigned char uint_8;
typedef signed short int int_16;
typedef unsigned short int uint_16;
typedef signed int int_32;
typedef unsigned int uint_32;

typedef float float_32;
typedef double float_64;


#ifdef __cplusplus
}
#endif

#endif

