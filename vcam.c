/*	3D Virtual Camera.
	Author: Miguel A. Osorio - Feb 28 2017
*/

/*
	Copyright (c) 2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#include <math.h>

#include "v.h"
#include "cframe.h"
#include "vcam.h"




void vcam_default(vcam* cam)
{
	cframe3_default((cframe3*)cam);
	cam->fovy = 45.0f;
	cam->znear = 0.1f;
	cam->zfar = 1000.0f;
	cam->aspect = 4.0f / 3.0f;
}

void vcam_projection_m4(const vcam* cam, Vmatrix4x4 P)
{
	Vfloat32 xmin, xmax, ymin, ymax;
	Vfloat32 fovy = vDegToRad(cam->fovy);

	vSetIdentityMatrix4x4(P);

	/* For FOVy 0.0, we assume no projection */
	if(fovy == 0.0)
		return;

	ymax = cam->znear * (Vfloat32)tan(fovy * 0.5);
	ymin = -ymax;
	xmin = ymin * cam->aspect;
	xmax = ymax * cam->aspect;

	P[0] = (2.0 * cam->znear) / (xmax - xmin);
	P[5] = (2.0 * cam->znear) / (ymax - ymin);
	P[8] = (xmax + xmin) / (xmax - xmin);
	P[9] = (ymax + ymin) / (ymax - ymin);
	P[10] = -((cam->zfar + cam->znear) / (cam->zfar - cam->znear));
	P[11] = -1.0;
	P[14] = -((2.0 * cam->zfar * cam->znear) / (cam->zfar - cam->znear));
	P[15] = 0.0;
}

void vcam_viewproj_m4(const vcam* cam, Vmatrix4x4 VP)
{
	Vmatrix4x4 V;

	cframe3_to_m4_inv((cframe3*)cam, V);
	vcam_projection_m4(cam, VP);
	vMultMatrix4x4(VP, V, VP);
}

void vcam_aim(const Vvector3D target, const Vvector3D up, vcam* cam)
{
	cframe3_axes cam_axes;

	/* Find forward */
	vSub3D(target, cam->cf.pos, cam_axes.fwd);
	vNormalize3D(cam_axes.fwd);
	/* Find right */
	vCross3D(cam_axes.fwd, up, cam_axes.right);
	vNormalize3D(cam_axes.right);
	/* Find real up */
	vCross3D(cam_axes.right, cam_axes.fwd, cam_axes.up);
	/* Set camera coord frame */
	cframe3_set_axes(&cam_axes, (cframe3*)cam);
}

void vcam_orbit_target(const Vvector3D target, const Vfloat yaw, const Vfloat pitch, const Vfloat roll, vcam* cam)
{
	const Vfloat yaw_rad = vDegToRad(yaw);
	const Vfloat pitch_rad = vDegToRad(pitch);
	cframe3_axes cam_axes;

	cframe3_get_axes(&cam_axes, (cframe3*)cam);
	/* Translate position, relative to target */
	vSub3D(cam->cf.pos, target, cam->cf.pos);
	/* Apply YAW to Pos, about the Up axis */
	vRotate3D(cam->cf.pos, yaw_rad, cam_axes.up, cam->cf.pos);
	/* Apply PITCH to Pos, about the Right axis */
	vRotate3D(cam->cf.pos, pitch_rad, cam_axes.right, cam->cf.pos);
	/* Translate back to original position */
	vAdd3D(cam->cf.pos, target, cam->cf.pos);
	/* Accumulate rotations in quaternion */
	cframe3_YPR(yaw, pitch, roll, (cframe3*)cam);
}

