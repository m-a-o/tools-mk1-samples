/* Dynamic arrays */
/* Author: Miguel A. Osorio - Oct 19 2001 */

/*
	Copyright (c) 2001-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/



#ifndef _D_ARRAY_
#define _D_ARRAY_

#ifdef __cplusplus
extern "C" {
#endif



/* Return codes */
enum {

	DA_OK = 0,
	DA_ERROR,
	DA_OUT_OF_BOUNDS,
	DA_RESIZE_ERROR

};

/* Array creation flags */
#define DA_NIL 0x00
#define DA_AUTO_DOWNSIZE 0x01
#define DA_NO_OPTION DA_NIL


/* Fundamental */
typedef ptrdiff_t mem_int_t;
typedef size_t mem_uint_t;

/* A dynamic array */
typedef struct d_array_struct* d_array_t;


d_array_t da_create(mem_int_t n, mem_int_t isize);
d_array_t da_create_ex(mem_int_t n, mem_int_t isize, mem_int_t rsize_mult, int flags);

void da_delete(d_array_t array);

int da_add(const void* item, d_array_t array);
int da_addn(const void* items, mem_int_t n, d_array_t array);
int da_addi(const void* item, mem_int_t index, d_array_t array);
int da_addni(const void* items, mem_int_t n, mem_int_t index, d_array_t array);

int da_get(mem_int_t index, void* item, d_array_t array);
int da_getn(mem_int_t index, mem_int_t n, void* items, d_array_t array);

int da_remove(mem_int_t index, void* item, d_array_t array);
int da_removen(mem_int_t index, mem_int_t n, void* items, d_array_t array);

int da_replace(const void* item, mem_int_t index, d_array_t array);
int da_replacen(const void* items, mem_int_t n, mem_int_t index, d_array_t array);

mem_int_t da_num_items(d_array_t array);
mem_int_t da_item_size(d_array_t array);

void* da_buffer(d_array_t array);
const char* da_buffer_str(d_array_t array);

void da_clear(d_array_t array);
void da_reset(d_array_t array);
void da_truncate(mem_int_t n, d_array_t array);

mem_int_t da_curr_size(d_array_t array);

/*** Experimental ***/
int da_add_formatted(d_array_t array, mem_int_t index, const char* fmt, ...);
int da_cat_formatted(d_array_t array, const char* fmt, ...);

int da_pop_remove(mem_int_t index, void* item, d_array_t array);
int da_pop_removen(mem_int_t index, mem_int_t n, void* items, d_array_t array);


#ifdef __cplusplus
}
#endif

#endif

