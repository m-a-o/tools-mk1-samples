/* V - Vector library
   Author: Miguel A. Osorio */

/*
	Copyright (c) 2002-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


#ifndef _V_H_
#define _V_H_

#ifdef __cplusplus
extern "C" {
#endif

/* TO DO

- WARNING! - The vMatrixToQuaternion function is not yet fully tested - keep that in mind

- 4D functions
- Full double precision support - compiler switches suck for this!

   TO DO */
   


/*** Values ***/
#define V_FLOAT32_MIN 1.17549435e-38F
#define V_FLOAT32_MAX 3.40282347e+38F
#define V_FLOAT64_MIN 2.2250738585072014e-308
#define V_FLOAT64_MAX 1.7976931348623157e+308


/*** Data types ***/

/* Base types */
typedef char Vbyte;
typedef unsigned char Vubyte;
typedef short int Vint16;
typedef unsigned short int Vuint16;
typedef int Vint32;
typedef unsigned int Vuint32;
typedef float Vfloat32;
typedef double Vfloat64;

/* Default floating point precision */
typedef Vfloat32 Vfloat;
#define V_FLOAT_MIN V_FLOAT32_MIN
#define V_FLOAT_MAX V_FLOAT32_MAX

/* 2D vector data type */
typedef Vfloat Vvector2D[2];

/* 3D vector data type */
typedef Vfloat Vvector3D[3];

/* 4D vector data type */
typedef Vfloat Vvector4D[4];

/* 4x4 matrix type */
typedef Vfloat Vmatrix4x4[16];

/* Plane equation in 3D space (Ax + By + Cz + D = 0) */
typedef Vfloat Vplane3D[4];

/* Line equation in 2D space (Ax + By + C = 0) */
typedef Vfloat Vline2D[3];

/* Quaternions */
typedef Vfloat Vquaternion[4];


/* Vector coordinates indexes */
#define _X_ 0
#define _Y_ 1
#define _Z_ 2
#define _W_ 3

/* Texture coordinates indexes */
#define _S_ 0
#define _T_ 1
#define _R_ 2
#define _Q_ 3

/* Plane equation indexes */
#define _A_ 0
#define _B_ 1
#define _C_ 2
#define _D_ 3

/* 4x4 matrix vector components */
#define _I_ 0
#define _J_ 4
#define _K_ 8
#define _L_ 12

/* Boolean values */
#define V_FALSE 0
#define V_TRUE (!V_FALSE)

/* Return values */
#define V_INSIDE 0x01
#define V_OUTSIDE 0x02
#define V_INTERSECT 0x03


/*** Aliases ***/


/*** Macros ***/

/* Checks if s is zero within tolerance tol */
#define vIsZero(s, tol) ((s) < (tol))

/* Minimum and maximum between two numbers */
#define vMax(a, b) (((a) > (b)) ? (a) : (b))
#define vMin(a, b) (((a) < (b)) ? (a) : (b))


/*** Core functions ***/

/* Set null vector */
void vZero2D(Vvector2D v);
void vZero3D(Vvector3D v);

/* Negate vector */
void vNeg2D(const Vvector2D v, Vvector2D res);
void vNeg3D(const Vvector3D v, Vvector3D res);

/* Set vector coordinates */
void vSet2D(Vvector2D v, const Vfloat x, const Vfloat y);
void vSet3D(Vvector3D v, const Vfloat x, const Vfloat y, const Vfloat z);
void vSet4D(Vvector4D v, const Vfloat x, const Vfloat y, const Vfloat z, const Vfloat w);

/* Copy vectors (a = b)*/
void vCopy2D(Vvector2D a, const Vvector2D b);
void vCopy3D(Vvector3D a, const Vvector3D b);
void vCopy4D(Vvector4D a, const Vvector4D b);

/* Returns a . b */
Vfloat vDot2D(const Vvector2D a, const Vvector2D b);
Vfloat vDot3D(const Vvector3D a, const Vvector3D b);
Vfloat vDot4D(const Vvector3D a, const Vvector3D b);

/* Perp dot product (2D) */
void vPerp2D(const Vvector2D a, Vvector2D res);

/* Makes res = a x b */
void vCross3D(const Vvector3D a, const Vvector3D b, Vvector3D res);

/* Makes res = a + b */
void vAdd2D(const Vvector2D a, const Vvector2D b, Vvector2D res);
void vAdd3D(const Vvector3D a, const Vvector3D b, Vvector3D res);

/* Makes res = a - b */
void vSub2D(const Vvector2D a, const Vvector2D b, Vvector2D res);
void vSub3D(const Vvector3D a, const Vvector3D b, Vvector3D res);

/* Scales a by s, stores in res */
void vScale2D(const Vvector2D v, const Vfloat s, Vvector2D res);
void vScale3D(const Vvector3D v, const Vfloat s, Vvector3D res);

/* Calculates norm of vector a */
Vfloat vNorm2D(const Vvector2D v);
Vfloat vNorm3D(const Vvector3D v);

/* Calculates squared norm of vector a */
Vfloat vFastNorm2D(const Vvector2D v);
Vfloat vFastNorm3D(const Vvector3D v);

/* Normalizes a */
void vNormalize2D(Vvector2D a);
void vNormalize3D(Vvector3D a);

/* Checks if a == b */
Vint32 vIsEqual2D(const Vvector2D a, const Vvector2D b);
Vint32 vIsEqual3D(const Vvector3D a, const Vvector3D b);

/* Rotation about an arbitrary axis d (NOTE: assumes d is NORMALIZED) */
void vRotate3D(const Vvector3D v, const Vfloat angle, const Vvector3D d, Vvector3D res);

/* Rotations about the X, Y and Z axis */
void vRotateX3D(const Vvector3D a, const Vfloat angle, Vvector3D res);
void vRotateY3D(const Vvector3D a, const Vfloat angle, Vvector3D res);
void vRotateZ3D(const Vvector3D a, const Vfloat angle, Vvector3D res);

/* Calculate the equation of a 3D plane */
void vCreatePlane3D(Vvector3D normal, Vvector3D point, Vplane3D plane);

/* Calculate the equation of a 2D line */
void vCreateLine2D(Vvector2D normal, Vvector2D point, Vline2D line);

/* Evaluate point in relation to plane */
Vfloat vEvalPointAndPlane3D(Vvector3D p, Vplane3D plane);

/* Evaluate point in relation to line (2D) */
Vfloat vEvalPointAndLine2D(Vvector2D p, Vline2D line);


/*** Intersection routines ***/

/* Check if given point is inside a given triangle T(v0, v1, v2) */
Vint32 vPointInTriangle2D(Vvector2D point, Vvector2D v0, Vvector2D v1, Vvector2D v2);

/* Check if two line segments intersect (2D) */
Vint32 vSegSegIntersect2D(Vvector2D p0, Vvector2D p1, Vvector2D q0, Vvector2D q1);

/* Check if two triangles intersect (must supply normal to T2(v0, v1, v2)) (3D) */
Vint32 vTriTriIntersect3D(Vvector3D u0, Vvector3D u1, Vvector3D u2, Vvector3D v0, Vvector3D v1, Vvector3D v2, Vvector3D n2);

/* Check if a plane and an AABB intersect (3D)
   Note: this routine returns V_INSIDE (if AABB is on the opposite side of the plane's normal),
   V_OUTSIDE (AABB on the same side of the normal), or V_INTERSECT */
Vint32 vPlaneAABBIntersect3D(Vplane3D plane, Vvector3D bmin, Vvector3D bmax);

/* Check if two AABBs intersect */
Vint32 vAABBIntersect3D(Vvector3D amin, Vvector3D amax, Vvector3D bmin, Vvector3D bmax);

/* Check if a triangle intersects a box
   Note: the box is specified by it's center and a half-length vector, and the triangle's normal must be given */
Vint32 vTriBoxIntersect3D(Vvector3D c, Vvector3D h, Vvector3D u0, Vvector3D u1, Vvector3D u2, Vvector3D n);

/* Check if an AABB intersects an OBB
   Note: h specifies the OBB's half length vector and the matrix is a compact way of specifying the OBB's axis set */
Vint32 vAABBIntersectOBB3D(Vvector3D amax, Vvector3D pos, Vvector3D h, Vmatrix4x4 m);

/* Check if a ray intersects an OBB
   Note: the OBB is specified just as above, and the function returns V_TRUE or V_FALSE and sets the "t"
   parameter in case of intersection; the ray is defined as an origin "o" and a NORMALIZED direction "d" */
Vint32 vRayIntersectOBB3D(Vfloat *t, Vvector3D o, Vvector3D d, Vvector3D pos, Vvector3D h, Vmatrix4x4 m);


/*** Unit conversions ***/

/* Degrees to radians */
Vfloat vDegToRad(Vfloat angle);

/* Radians to degrees */
Vfloat vRadToDeg(Vfloat angle);


/*** Quaternions ***/

/* Setting the identity quaternion */
void vSetIdentityQuaternion(Vquaternion q);

/* Copy quaternion (a = b) */
void vCopyQuaternion(Vquaternion a, Vquaternion b);

/* Convert (axis, angle) pair to quaternion representation */
void vAxisAngleToQuaternion(Vvector3D axis, Vfloat angle, Vquaternion q);

/* Quaternion multiplication (a.b = res) */
void vMultQuaternion(Vquaternion a, Vquaternion b, Vquaternion res);

/* Generate a rotation matrix from a given quaternion */
void vQuaternionToMatrix(Vquaternion q, Vmatrix4x4 m);

/* Generate a quaternion from a given rotation matrix */
void vMatrixToQuaternion(Vmatrix4x4 m, Vquaternion q);


/*** Matrices ***/

/* Set identity matrix */
void vSetIdentityMatrix4x4(Vmatrix4x4 m);

/* Transpose matrix */
void vTransposeMatrix4x4(Vmatrix4x4 m);


/*** WIP! ***/
void vCopyMatrix4x4(Vmatrix4x4 a, const Vmatrix4x4 b);
void vMultMatrix4x4(const Vmatrix4x4 a, const Vmatrix4x4 b, Vmatrix4x4 res);
void vMatrixTransformV4D(const Vmatrix4x4 T, const Vvector4D v, Vvector4D res);
void vQuaternionToSubMatrix4x3(const Vquaternion q, Vmatrix4x4 m);
Vfloat32 vClamp32(const Vfloat32 s, const Vfloat32 min, const Vfloat32 max);
Vfloat64 vClamp64(const Vfloat64 s, const Vfloat64 min, const Vfloat64 max);

void dumpVector4D(const Vvector4D v);
void dumpMatrix4x4(const Vmatrix4x4 m);

#ifdef __cplusplus
}
#endif

#endif

