/*	Game loop ticker
	Author: Miguel A. Osorio - Jan 19 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


/*
	Module inspired by Glenn Fiedler's most excellent "Fix Your Timestep!" article.
	(Please refer to http://gafferongames.com/game-physics/fix-your-timestep/ for
	more info).
*/


#include <stdio.h>

#include "custom_types.h"
#include "ticker.h"




struct ticker ticker_create(const float_64 delta, void* client_data, const tick_get_time_proc get_time)
{
	struct ticker tick = { { 0.0, 0.0, 0.0, 0.0 }, NULL, NULL, NULL, NULL, NULL };

	tick.tickCtl.delta = delta;

	if(get_time != NULL) {
		tick.tickCtl.curr = get_time(client_data);
		tick.getTime = get_time;
	}

	tick.clientData = client_data;

	return tick;
}

#define TICK_CLAMP_T(t, clamp) (((t) <= (clamp)) ? (t) : (clamp))
#define TICK_MAX_FRAME_T (0.2)
int ticker_tick(struct ticker* tick)
{
	float_64 t = tick->getTime(tick->clientData);
	float_64 frame_t = t - tick->tickCtl.curr;
	int break_code = 0;

	frame_t = TICK_CLAMP_T(frame_t, TICK_MAX_FRAME_T);
	tick->tickCtl.curr = t;
	tick->tickCtl.accum += frame_t;

	if(tick->updateFrame != NULL)
		if((break_code = tick->updateFrame(tick->tickCtl, frame_t, tick->clientData)))
			return break_code;

	/* UpdateTick callback cannot be NULL! */
	while(tick->tickCtl.accum >= tick->tickCtl.delta) {
		if((break_code = tick->updateTick(tick->tickCtl, tick->tickCtl.delta, tick->clientData)))
			return break_code;

		tick->tickCtl.elapsed += tick->tickCtl.delta;
		tick->tickCtl.accum -= tick->tickCtl.delta;
	}

	if(tick->updateBlend != NULL)
		if((break_code = tick->updateBlend(tick->tickCtl, tick->tickCtl.accum / tick->tickCtl.delta, tick->clientData)))
			return break_code;

	return break_code;
}

