/*	3D Virtual Camera.
	Author: Miguel A. Osorio - Feb 28 2017
*/

/*
	Copyright (c) 2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#ifndef _VCAM_H_
#define _VCAM_H_

#ifdef __cplusplus
extern "C" {
#endif


typedef struct vcam {
	cframe3 cf;
	Vfloat32 fovy;
	Vfloat32 znear;
	Vfloat32 zfar;
	Vfloat32 aspect;
} vcam;

typedef struct vcam_frustum {
	Vplane3D left, right;
	Vplane3D top, bottom;
	Vplane3D near, far;
} vcam_frustum;

void vcam_default(vcam* cam);
void vcam_projection_m4(const vcam* cam, Vmatrix4x4 P);
void vcam_viewproj_m4(const vcam* cam, Vmatrix4x4 VP);

void vcam_aim(const Vvector3D target, const Vvector3D up, vcam* cam);
void vcam_orbit_target(const Vvector3D target, const Vfloat yaw, const Vfloat pitch, const Vfloat roll, vcam* cam);


#ifdef __cplusplus
}
#endif

#endif

