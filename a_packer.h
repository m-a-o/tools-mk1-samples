/*	Array packer.
	Author: Miguel A. Osorio - Mar 13 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


#ifndef _ARRAY_PACKER_H_
#define _ARRAY_PACKER_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
	Compile-time alignment of types.
	Only one DEFINE_ALIGNOF for a given typename is allowed.
*/
/* Helpers */
#define ALIGNOF_STRUCT_NAME(typenam) alignof_##typenam##_t
#define ALIGNOF_CONST_NAME(typenam) ALIGNOF_##typenam
#define ALIGNOF_CONST_ENUM(typenam) enum { ALIGNOF_CONST_NAME(typenam) = sizeof(struct ALIGNOF_STRUCT_NAME(typenam)) - offsetof(struct ALIGNOF_STRUCT_NAME(typenam), c) }

/* API */
#define DEFINE_ALIGNOF_STRUCT(typenam) struct ALIGNOF_STRUCT_NAME(typenam) { struct typenam i; char c; }; ALIGNOF_CONST_ENUM(typenam)
#define DEFINE_ALIGNOF_TYPE(typenam) struct ALIGNOF_STRUCT_NAME(typenam)  { typenam i; char c; }; ALIGNOF_CONST_ENUM(typenam)
#define ALIGNOF(typenam) ALIGNOF_CONST_NAME(typenam)
/* Compile-time assert of type alignment */
#define ASSERT_ALIGNOF(typenam, ref) enum check_alignof_##stru_enum { ALIGNOF_CHECK_##typenam = 1 / (!!(ALIGNOF_test_struct == (ref))) }


/*
	Align to powers of 2.
	The initial address is shifted by up to [alignment - 1] bytes.
*/
const size_t offsetToAligned(const size_t alignment, const void *buf);
const void* alignToOffset(const size_t offset, const void *buf);
const void* alignTo(const size_t alignment, const void *buf);

/* Packed array header */
struct packed_array {
	size_t size;
	size_t alignment;
};

size_t packed_array_h_max_size(const struct packed_array h);
size_t packed_array_fmt_max_size(const struct packed_array *spec, const size_t n);

struct packed_array_it {
	const void* base;
	size_t offset;
};

struct packed_array_it packed_array_it_start(const void* buf);
size_t packed_array_walk(const struct packed_array h, struct packed_array_it* it);


#ifdef __cplusplus
}
#endif

#endif
