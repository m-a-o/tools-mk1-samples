/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


#include <stdio.h>

#include "mk1_aligned_alloc.h"




int main(int argc, char* argv[])
{
	page_allocator_t pa = page_allocator_create(128);
	void* p1;
	void* p2;
	void* p3;

	page_allocator_info_dump(pa);
	p1 = paged_alloc(3, 16, pa);
	p2 = paged_alloc(1, 16, pa);
	p3 = paged_alloc(5, 16, pa);
	page_allocator_info_dump(pa);

	printf("(p1, p2, p3) = (%p, %p, %p)\n", p1, p2, p3);

	page_allocator_reset(pa);

	page_allocator_info_dump(pa);
	p1 = paged_alloc(3, 16, pa);
	p2 = paged_alloc(1, 16, pa);
	p3 = paged_alloc(5, 16, pa);
	page_allocator_info_dump(pa);

	page_allocator_delete(pa);

	return 0;
}

