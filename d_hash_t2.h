/*	Dynamic hash tables MK2
	Author: Miguel A. Osorio - Aug 03 2015
*/

/*
	Copyright (c) 2015-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


/*
	TODO:

	- fixed and variable entry points must check if the passed table is of the correct type!
	- "const" parameter revision!
*/



#ifndef _D_HASH_TABLE_
#define _D_HASH_TABLE_

#include "custom_types.h"

#ifdef __cplusplus
extern "C" {
#endif



/* Return codes */
enum {

	DH_OK = 0,
	DH_REPLACE,
	DH_NOT_FOUND,
	DH_ERROR,
	DH_RESIZE_ERROR,
	DH_INVALID_PARAMETER_ERROR,
	DH_END,
	DH_OUT_OF_BOUNDS

};

/* Table types */
enum {

	DH_STATIC = 0,
	DH_DYNAMIC

};

/* Default maximum load factor (beyond which table resizing is done) */
#define DH_DEFAULT_MAX_LOAD (10.0f)

/* A hash table */
typedef struct d_hasht_struct* dh_table_t;


/* Functions */
dh_table_t dh_createf(int_32 table_size_pairs, int_32 key_size, int_32 value_size, int table_type);
dh_table_t dh_createv(int_32 table_size_pairs, int table_type);

void dh_delete(dh_table_t table);

void dh_set_type(int type, dh_table_t table);
void dh_set_max_load(float max_load, dh_table_t table);

int_32 dh_num_pairs(dh_table_t table);

int dh_addf(const void* key, const void* value, dh_table_t table);
int dh_addv(const void* key, int_32 key_size, const void* value, int_32 value_size, dh_table_t table);

int dh_getf(const void* key, void* value, dh_table_t table);
int dh_getv(const void* key, int_32 key_size, void* value, int_32* value_size, dh_table_t table);

int dh_remf(const void* key, dh_table_t table);
int dh_remv(const void* key, int_32 key_size, dh_table_t table);

/* dh_ipair_t is opaque! */
typedef struct dh_ipair_struct {
	uint_32 iChain;
	uint_32 iPair;
	dh_table_t table;
} dh_ipair_t;

int dh_ipairf(const void* key, dh_table_t table, dh_ipair_t* ipair);
int dh_ipairv(const void* key, const int_32 key_size, dh_table_t table, dh_ipair_t* ipair);
int dh_ipair_chk(const dh_ipair_t* ipair);
dh_ipair_t dh_ipair_begin(dh_table_t table);
int dh_ipair_next(dh_ipair_t* ipair);
int dh_ipair_get(void* key, int_32* key_size, void* value, int_32* value_size, const dh_ipair_t* ipair);

int dh_ipair_updatef(const void* value, const dh_ipair_t* ipair);
int dh_ipair_updatev(const void* value, int_32 value_size, const dh_ipair_t* ipair);

/*** Experimental ***/
const char* dh_rc_str(const int rc);
void dh_dump(dh_table_t table);
void dh_dump_ipair(dh_ipair_t ipair);

uint_32 ceilP2(uint_32 i);

#ifdef __cplusplus
}
#endif

#endif

