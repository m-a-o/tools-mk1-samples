/*	Dynamic hash tables MK2
	Author: Miguel A. Osorio - Aug 03 2015
*/

/*
	Copyright (c) 2015-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "hash_64.h"
#include "d_hash_t2.h"


/*
	TODO:

	- Initial table size should take load factor into account?
	- Revise signed vs unsigned indexes, sizes, quantities, etc!

*/

typedef struct hash_64 hash_64;

static uint_32 hashMaskFromTableSize(const uint_32 table_size_pow2)
{
	return(table_size_pow2 - 1);
}

/*
	From 'Hacker's Delight' by Henry S. Warren, Jr..
*/
/* Round up to the next power of 2 */
uint_32 ceilP2(uint_32 i)
{
	i -= 1;
	i |= (i >> 1);
	i |= (i >> 2);
	i |= (i >> 4);
	i |= (i >> 8);
	i |= (i >> 16);

	return(i + 1);
}

typedef struct pair_size_64_struct {
	uint_32 key;
	uint_32 value;
} pair_size_64;

static uint_32 pairSize(const pair_size_64 pair_size)
{
	return pair_size.key + pair_size.value;
}

static uint_32 parallelBufferSize(const uint_32 num_pairs, const uint_32 pair_size)
{
	return ceilP2(num_pairs) * (sizeof(hash_64) + pair_size);
}

static int dynamicBufferResize(unsigned char** buffer, const size_t new_size)
{
	void* new_buffer = realloc(*buffer, new_size);

	if(new_buffer == NULL)
		return DH_RESIZE_ERROR;

	*buffer = (unsigned char*)new_buffer;

	return DH_OK;
}

static int parallelBufferResize(const int_32 to_num_pairs, const uint_32 pair_size, unsigned char** curr_buffer)
{
	return dynamicBufferResize(curr_buffer, parallelBufferSize(to_num_pairs, pair_size));
}

static void parallelBufferDelete_Any(unsigned char* buffer)
{
	free(buffer);
}

typedef struct packed_parallel_arrays_struct {
	hash_64* hashes;
	unsigned char* pairs;
	uint_32 capacityPairs;
} packed_parallel_arrays;

static packed_parallel_arrays parallelArraysFromBuffer(const unsigned char* buffer, const uint_32 buffer_capacity)
{
	packed_parallel_arrays p_arrays;
	
	p_arrays.hashes = (hash_64*)buffer;
	p_arrays.pairs = (unsigned char*)buffer + (sizeof(hash_64) * buffer_capacity);
	p_arrays.capacityPairs = buffer_capacity;

	return p_arrays;
}

/*
	The "pairs" packed array will store key and value packed arrays for fixed-size pairs.
	Variable-size pairs store a (pair_size, buffer_ptr) tuple instead - key value buffers
	are allocated separately.
*/

/*
	Memory layout for fixed-size pairs
*/
static unsigned char* getKeyArray(const packed_parallel_arrays p_arrays)
{
	return p_arrays.pairs;
}

static unsigned char* getValueArray(const packed_parallel_arrays p_arrays, const pair_size_64 pair_size)
{
	return getKeyArray(p_arrays) + (pair_size.key * p_arrays.capacityPairs);
}

static unsigned char* getKeyPointer_Fixed(const int_32 i, const packed_parallel_arrays p_arrays, const pair_size_64 pair_size)
{
	return (unsigned char*)(getKeyArray(p_arrays) + (pair_size.key * i));
}

static unsigned char* getValuePointer_Fixed(const int_32 i, const packed_parallel_arrays p_arrays, const pair_size_64 pair_size)
{
	return (unsigned char*)(getValueArray(p_arrays, pair_size) + (pair_size.value * i));
}

typedef struct pair_buf_pointers_struct {
	unsigned char* pair;
	unsigned char* key;
	unsigned char* value;
} pair_buf_pointers;

/*
	Memory layout for variable-size pairs
*/
typedef struct pair_variable_store_struct {
	uint_32 pairBufferSize;
	unsigned char* pair;
} pair_variable_store;

static int pairVariableStoreUpdate(const pair_size_64 pair_size, pair_variable_store* p_store)
{
	int rc;

	if((p_store->pair != NULL) && (pairSize(pair_size) <= p_store->pairBufferSize))
		return DH_OK;

	rc = dynamicBufferResize(&(p_store->pair), pairSize(pair_size));
	if(rc != DH_OK)
		return rc;

	p_store->pairBufferSize = pairSize(pair_size);

	return DH_OK;
}

static void pairVariableStoreDelete(pair_variable_store* p_store)
{
	free(p_store->pair);
}

typedef struct pair_data_indirect_struct {
	pair_size_64 pairSize;
	pair_variable_store store;
} pair_data_indirect;

static unsigned char* getPairPointer(const int_32 i, const packed_parallel_arrays p_arrays)
{
	return p_arrays.pairs + (sizeof(pair_data_indirect) * i);
}

static unsigned char* getKeyPointer_Variable(const unsigned char* pair_pointer, const pair_size_64 pair_size)
{
	return ((pair_data_indirect*)pair_pointer)->store.pair;
}

static unsigned char* getValuePointer_Variable(const unsigned char* pair_pointer, const pair_size_64 pair_size)
{
	pair_data_indirect* pair_data_ind = (pair_data_indirect*)pair_pointer;

	return pair_data_ind->store.pair + pair_data_ind->pairSize.key;
}

static pair_buf_pointers getPairBufPointers_Fixed(const int_32 i, const packed_parallel_arrays p_arrays, const pair_size_64 pair_size)
{
	pair_buf_pointers p_pointers;

	p_pointers.pair = NULL;
	p_pointers.key = getKeyPointer_Fixed(i, p_arrays, pair_size);
	p_pointers.value = getValuePointer_Fixed(i, p_arrays, pair_size);

	return p_pointers;
}

static pair_buf_pointers getPairBufPointers_Variable(const int_32 i, const packed_parallel_arrays p_arrays, const pair_size_64 pair_size)
{
	pair_buf_pointers p_pointers;
	pair_data_indirect* pair_pointer = (pair_data_indirect*)getPairPointer(i, p_arrays);

	p_pointers.pair = (unsigned char*)pair_pointer;
	p_pointers.key = getKeyPointer_Variable((unsigned char*)pair_pointer, pair_pointer->pairSize);
	p_pointers.value = getValuePointer_Variable((unsigned char*)pair_pointer, pair_pointer->pairSize);

	return p_pointers;
}

typedef struct hash_pair_data_struct {
	hash_64 hash;
	unsigned char* key;
	unsigned char* value;
	pair_size_64 pairSize;
} hash_pair_data;

static hash_pair_data pairDataCreate(const hash_64 key_hash, const void* key, const uint_32 key_size, const void* value, const uint_32 value_size)
{
	hash_pair_data pair_data;
	
	pair_data.hash = key_hash;
	pair_data.key = (unsigned char*)key;
	pair_data.value = (unsigned char*)value;
	pair_data.pairSize.key = key_size;
	pair_data.pairSize.value = value_size;

	return pair_data;
}

static void parallelArraysReplacePairKey_Fixed(const int_32 i, const hash_pair_data pair_data, packed_parallel_arrays p_arrays)
{
	memcpy(getKeyPointer_Fixed(i, p_arrays, pair_data.pairSize), pair_data.key, pair_data.pairSize.key);
}

static void parallelArraysReplacePairKey_Variable(const int_32 i, const hash_pair_data pair_data, packed_parallel_arrays p_arrays)
{
	pair_data_indirect* p_data_ind = (pair_data_indirect*)getPairPointer(i, p_arrays);

	pairVariableStoreUpdate(pair_data.pairSize, &(p_data_ind->store));
	memcpy(getKeyPointer_Variable((unsigned char*)p_data_ind, pair_data.pairSize), pair_data.key, pair_data.pairSize.key);
	p_data_ind->pairSize.key = pair_data.pairSize.key;
}

static void parallelArraysReplacePairValue_Fixed(const int_32 i, const hash_pair_data pair_data, packed_parallel_arrays p_arrays)
{
	memcpy(getValuePointer_Fixed(i, p_arrays, pair_data.pairSize), pair_data.value, pair_data.pairSize.value);
}

static void parallelArraysReplacePairValue_Variable(const int_32 i, const hash_pair_data pair_data, packed_parallel_arrays p_arrays)
{
	pair_data_indirect* p_data_ind = (pair_data_indirect*)getPairPointer(i, p_arrays);

	pairVariableStoreUpdate(pair_data.pairSize, &(p_data_ind->store));
	p_data_ind->pairSize.value = pair_data.pairSize.value;
	if(pair_data.pairSize.value > 0)
		memcpy(getValuePointer_Variable((unsigned char*)p_data_ind, pair_data.pairSize), pair_data.value, pair_data.pairSize.value);
}

typedef void (*p_arrays_replace_pair_component_proc)(const int_32 i, const hash_pair_data pair_data, packed_parallel_arrays p_arrays);

static void parallelArraysWritePair_Fixed(const int_32 i, const hash_pair_data pair_data, packed_parallel_arrays p_arrays)
{
	p_arrays.hashes[i] = pair_data.hash;

	parallelArraysReplacePairKey_Fixed(i, pair_data, p_arrays);
	parallelArraysReplacePairValue_Fixed(i, pair_data, p_arrays);
}

static void parallelArraysWritePair_Variable(const int_32 i, const hash_pair_data pair_data, packed_parallel_arrays p_arrays)
{
	p_arrays.hashes[i] = pair_data.hash;

	parallelArraysReplacePairKey_Variable(i, pair_data, p_arrays);
	parallelArraysReplacePairValue_Variable (i, pair_data, p_arrays);
}

typedef void (*p_arrays_write_pair_proc)(const int_32 i, const hash_pair_data pair_data, packed_parallel_arrays p_arrays);

/* Copy last pair to removed position */
static void parallelArraysCopyLastPair_Fixed(const int_32 i, const int_32 last_pair_i, const pair_size_64 pair_size, packed_parallel_arrays p_arrays)
{
	const pair_buf_pointers pair_pointers = getPairBufPointers_Fixed(i, p_arrays, pair_size);
	const pair_buf_pointers last_pair_pointers = getPairBufPointers_Fixed(last_pair_i, p_arrays, pair_size);

	memcpy(pair_pointers.value, last_pair_pointers.value, pair_size.value);
	memcpy(pair_pointers.key, last_pair_pointers.key, pair_size.key);
}

static void parallelArraysCopyLastPair_Variable(const int_32 i, const int_32 last_pair_i, const pair_size_64 pair_size, packed_parallel_arrays p_arrays)
{
	memcpy(getPairPointer(i, p_arrays), getPairPointer(last_pair_i, p_arrays), sizeof(pair_data_indirect));
}

typedef void (*p_arrays_copy_last_pair_proc)(const int_32 i, const int_32 last_pair_i, const pair_size_64 pair_size, packed_parallel_arrays p_arrays);

/* No bounds checking! */
static int_32 parallelArraysPopRemovePair(const int_32 i, const pair_size_64 pair_size, packed_parallel_arrays p_arrays, const int_32 num_pairs, p_arrays_copy_last_pair_proc copy_last_pair)
{
	int_32 last_pair_index = num_pairs - 1;

	if(i < last_pair_index) {
		/* Copy last pair to removed position, will truncate */
		p_arrays.hashes[i] = p_arrays.hashes[last_pair_index];
		copy_last_pair(i, last_pair_index, pair_size, p_arrays);
	}

	/* Truncate */
	return last_pair_index;
}

static int_32 parallelArraysPopRemovePair_Fixed(const int_32 i, const pair_size_64 pair_size, packed_parallel_arrays p_arrays, const int_32 num_pairs)
{
	return parallelArraysPopRemovePair(i, pair_size, p_arrays, num_pairs, parallelArraysCopyLastPair_Fixed);
}

static int_32 parallelArraysPopRemovePairIndirect(const int_32 i, const pair_size_64 pair_size, packed_parallel_arrays p_arrays, const int_32 num_pairs)
{
	const int_32 lpi = parallelArraysPopRemovePair(i, pair_size, p_arrays, num_pairs, parallelArraysCopyLastPair_Variable);
	memset(getPairPointer(lpi, p_arrays), 0, sizeof(pair_data_indirect));

	return lpi;
}

static int_32 parallelArraysPopRemovePair_Variable(const int_32 i, const pair_size_64 pair_size, packed_parallel_arrays p_arrays, const int_32 num_pairs)
{
	pair_data_indirect* p_data = (pair_data_indirect*)getPairPointer(i, p_arrays);

	pairVariableStoreDelete(&(p_data->store));
	return parallelArraysPopRemovePairIndirect(i, pair_size, p_arrays, num_pairs);
}

typedef int_32 (*p_arrays_pop_remove_pair_proc)(const int_32 i, const pair_size_64 pair_size, packed_parallel_arrays p_arrays, const int_32 num_pairs);

/* Hash table chain */
typedef struct hash_chain_struct {
	int_32 numPairs;
	int_32 capacityPairs;
	unsigned char* parallelArrays;
} hash_chain;

static hash_chain chainNewEmptyStore(void)
{
	hash_chain chain = { 0, 0, NULL };

	return chain;
}

static packed_parallel_arrays chainParallelArrays(const hash_chain chain)
{
	return parallelArraysFromBuffer(chain.parallelArrays, chain.capacityPairs);
}

static void chainDelete_Fixed(hash_chain* chain)
{
	if(chain->parallelArrays != NULL)
		parallelBufferDelete_Any(chain->parallelArrays);

	*chain = chainNewEmptyStore();
}

static void chainDelete_Variable(hash_chain* chain)
{
	if(chain->parallelArrays != NULL) {
		pair_data_indirect* pairs_ind = (pair_data_indirect*)(chainParallelArrays(*chain).pairs);
		int_32 i;

		for(i = 0; i < chain->numPairs; ++i)
			pairVariableStoreDelete(&(pairs_ind[i].store));

		parallelBufferDelete_Any(chain->parallelArrays);
	}

	*chain = chainNewEmptyStore();
}

typedef void (*chain_delete_proc)(hash_chain* chain);

/* Linear search returns match index if key was found in chain, or -1 if not found */
static int_32 chainSearch_Fixed_CheckHash(const hash_pair_data pair_data, const hash_chain* chain)
{
	packed_parallel_arrays p_arrays = chainParallelArrays(*chain);
	int_32 i;

	for(i = 0; i < chain->numPairs; ++i)
		if((p_arrays.hashes[i].low == pair_data.hash.low) && (p_arrays.hashes[i].high == pair_data.hash.high))
			if(memcmp(getKeyPointer_Fixed(i, p_arrays, pair_data.pairSize), pair_data.key, pair_data.pairSize.key) == 0)
				return i;

	return -1;
}

static int_32 chainSearch_Fixed_NoHashCheck(const hash_pair_data pair_data, const hash_chain* chain)
{
	packed_parallel_arrays p_arrays = chainParallelArrays(*chain);
	int_32 i;

	for(i = 0; i < chain->numPairs; ++i)
		if(memcmp(getKeyPointer_Fixed(i, p_arrays, pair_data.pairSize), pair_data.key, pair_data.pairSize.key) == 0)
			return i;

	return -1;
}

static int_32 chainSearch_Variable(const hash_pair_data pair_data, const hash_chain* chain)
{
	packed_parallel_arrays p_arrays = chainParallelArrays(*chain);
	int_32 i;

	for(i = 0; i < chain->numPairs; ++i)
		if((p_arrays.hashes[i].low == pair_data.hash.low) && (p_arrays.hashes[i].high == pair_data.hash.high)) {
			pair_data_indirect* p_data_ind = (pair_data_indirect*)getPairPointer(i, p_arrays);

			if((p_data_ind->pairSize.key == pair_data.pairSize.key) && (memcmp(getKeyPointer_Variable((unsigned char*)p_data_ind, pair_data.pairSize), pair_data.key, pair_data.pairSize.key) == 0))
				return i;
		}

	return -1;
}

typedef int_32 (*chain_search_proc)(const hash_pair_data pair_data, const hash_chain* chain);

/*
	Layout updates occur between resizing the chain's buffer and updating it's tracked capacity!
*/
static void updateChainLayout_Fixed(const pair_size_64 pair_size, hash_chain* chain, const int_32 rsz_capacity_pairs)
{
	if(chain->capacityPairs > 0) {
		const pair_buf_pointers first_pair_pre_rsz = getPairBufPointers_Fixed(0, parallelArraysFromBuffer(chain->parallelArrays, chain->capacityPairs), pair_size);
		const pair_buf_pointers first_pair_post_rsz = getPairBufPointers_Fixed(0, parallelArraysFromBuffer(chain->parallelArrays, rsz_capacity_pairs), pair_size);
	
		/* Copy pre-resize to post-resize, in back-to-front layout order, excluding the first */
		/* Values */
		memcpy(first_pair_post_rsz.value, first_pair_pre_rsz.value, chain->capacityPairs * pair_size.value);
		/* Keys */
		memcpy(first_pair_post_rsz.key, first_pair_pre_rsz.key, chain->capacityPairs * pair_size.key);
	}
}

static void updateChainLayout_Variable(const pair_size_64 pair_size, hash_chain* chain, const int_32 rsz_capacity_pairs)
{
	if(chain->capacityPairs > 0) {
		const pair_buf_pointers first_pair_pre_rsz = getPairBufPointers_Variable(0, parallelArraysFromBuffer(chain->parallelArrays, chain->capacityPairs), pair_size);
		const pair_buf_pointers first_pair_post_rsz = getPairBufPointers_Variable(0, parallelArraysFromBuffer(chain->parallelArrays, rsz_capacity_pairs), pair_size);
	
		/* Copy pre-resize to post-resize, in back-to-front layout order, excluding the first */
		memcpy(first_pair_post_rsz.pair, first_pair_pre_rsz.pair, chain->capacityPairs * sizeof(pair_data_indirect));
	}

	/* TODO: proper chain-pair clearing function! */
	memset(getPairPointer(chain->numPairs, parallelArraysFromBuffer(chain->parallelArrays, rsz_capacity_pairs)), 0, (rsz_capacity_pairs - chain->numPairs) * sizeof(pair_data_indirect));
}

typedef void (*chain_update_layout_proc)(const pair_size_64 pair_size, hash_chain* chain, const int_32 rsz_capacity_pairs);

/* chainResize also initializes the chain store if currently empty */
#define DH_MAX(a, b) (((a) > (b)) ? (a) : (b))
static int chainResize(hash_chain* chain, const pair_size_64 pair_size, const uint_32 combined_pair_size, const chain_update_layout_proc chain_update_layout)
{
	const int_32 rsz_capacity_pairs = DH_MAX(1, chain->capacityPairs * 2);
	int rc = parallelBufferResize(rsz_capacity_pairs, combined_pair_size, &(chain->parallelArrays));

	if(rc != DH_OK)
		return rc;

	/* Rearrange layout */
	chain_update_layout(pair_size, chain, rsz_capacity_pairs);
	chain->capacityPairs = rsz_capacity_pairs;

	return DH_OK;
}

static int chainIncrementStore(const pair_size_64 pair_size, const uint_32 combined_pair_size, hash_chain* chain, const chain_update_layout_proc chain_update_layout)
{
	int rc = DH_OK;

	/* Grow dynamic storage? */
	if((chain->parallelArrays == NULL) || ((chain->numPairs + 1) > chain->capacityPairs))
		return chainResize(chain, pair_size, combined_pair_size, chain_update_layout);

	return rc;
}

static int chainAppendPair(const hash_pair_data pair_data, hash_chain* chain, const uint_32 chain_pair_size, const p_arrays_write_pair_proc write_pair, const chain_update_layout_proc chain_update_layout)
{
	int rc = chainIncrementStore(pair_data.pairSize, chain_pair_size, chain, chain_update_layout);

	if(rc == DH_OK) {
		write_pair(chain->numPairs, pair_data, chainParallelArrays(*chain));
		++(chain->numPairs);
	}

	return rc;
}

static int chainAppendPair_Fixed(const hash_pair_data pair_data, hash_chain* chain)
{
	return chainAppendPair(pair_data, chain, pairSize(pair_data.pairSize), parallelArraysWritePair_Fixed, updateChainLayout_Fixed);
}

static int chainAppendPair_Variable(const hash_pair_data pair_data, hash_chain* chain)
{
	return chainAppendPair(pair_data, chain, sizeof(pair_data_indirect), parallelArraysWritePair_Variable, updateChainLayout_Variable);
}

typedef int (*chain_append_pair_proc)(const hash_pair_data pair_data, hash_chain* chain);

static int chainAddPair(const hash_pair_data pair_data, hash_chain* chain, const chain_search_proc chain_search, const p_arrays_replace_pair_component_proc pair_replace_value, const chain_append_pair_proc chain_append_pair)
{
	int_32 pair_match;

	/* Search for pair in chain */
	pair_match = chain_search(pair_data, chain);

	if(pair_match >= 0) {
		/* Replace pair */
		pair_replace_value(pair_match, pair_data, chainParallelArrays(*chain));

		return DH_REPLACE;
	}

	/* New pair */
	return chain_append_pair(pair_data, chain);
}

static int chainAddPair_Fixed(const hash_pair_data pair_data, hash_chain* chain, const chain_search_proc chain_search)
{
	return chainAddPair(pair_data, chain, chain_search, parallelArraysReplacePairValue_Fixed, chainAppendPair_Fixed);
}

static int chainAddPair_Variable(const hash_pair_data pair_data, hash_chain* chain, const chain_search_proc chain_search)
{
	return chainAddPair(pair_data, chain, chain_search, parallelArraysReplacePairValue_Variable, chainAppendPair_Variable);
}

typedef int (*chain_add_pair_proc)(const hash_pair_data pair_data, hash_chain* chain, const chain_search_proc chain_search);

static void chainReplacePairValue_Fixed(const int_32 i, const hash_pair_data pair_data, hash_chain* chain)
{
	parallelArraysReplacePairValue_Fixed(i, pair_data, chainParallelArrays(*chain));
}

static void chainReplacePairValue_Variable(const int_32 i, const hash_pair_data pair_data, hash_chain* chain)
{
	parallelArraysReplacePairValue_Variable(i, pair_data, chainParallelArrays(*chain));
}

typedef void (*chain_replace_pair_value_proc)(const int_32 i, const hash_pair_data pair_data, hash_chain* chain);

static void chainPopRemovePair(const int_32 i, const pair_size_64 pair_size, hash_chain* chain, const p_arrays_pop_remove_pair_proc p_arrays_pop_remove_pair)
{
	p_arrays_pop_remove_pair(i, pair_size, chainParallelArrays(*chain), chain->numPairs);
	--(chain->numPairs);
}

static void chainPopRemovePair_Fixed(const int_32 i, const pair_size_64 pair_size, hash_chain* chain)
{
	chainPopRemovePair(i, pair_size, chain, parallelArraysPopRemovePair_Fixed);
}

static void chainPopRemovePair_Variable(const int_32 i, const pair_size_64 pair_size, hash_chain* chain)
{
	chainPopRemovePair(i, pair_size, chain, parallelArraysPopRemovePair_Variable);
}

typedef void (*chain_pop_remove_pair_proc)(const int_32 i, const pair_size_64 pair_size, hash_chain* chain);

static void chainCopyKeyBuffer_Fixed(const int_32 i, void* key_buf, const pair_size_64 pair_size, const hash_chain chain)
{
	memcpy(key_buf, getKeyPointer_Fixed(i, chainParallelArrays(chain), pair_size), pair_size.key);
}

static void chainCopyKeyBuffer_Variable(const int_32 i, void* key_buf, const pair_size_64 pair_size, const hash_chain chain)
{
	memcpy(key_buf, getKeyPointer_Variable(getPairPointer(i, chainParallelArrays(chain)), pair_size), pair_size.key);
}

static void chainCopyValueBuffer_Fixed(const int_32 i, void* value_buf, const pair_size_64 pair_size, const hash_chain chain)
{
	memcpy(value_buf, getValuePointer_Fixed(i, chainParallelArrays(chain), pair_size), pair_size.value);
}

static void chainCopyValueBuffer_Variable(const int_32 i, void* value_buf, const pair_size_64 pair_size, const hash_chain chain)
{
	memcpy(value_buf, getValuePointer_Variable(getPairPointer(i, chainParallelArrays(chain)), pair_size), pair_size.value);
}

typedef void (*chain_copy_key_buffer_proc)(const int_32 i, void* value_buf, const pair_size_64 pair_size, const hash_chain chain);
typedef void (*chain_copy_value_buffer_proc)(const int_32 i, void* value_buf, const pair_size_64 pair_size, const hash_chain chain);

typedef struct chain_ops_struct* chain_ops_p;
typedef void (*chain_forward_pair_proc)(const int_32 i, const hash_pair_data pair_data, hash_chain* from_chain, hash_chain* to_chain, chain_ops_p ops);

typedef hash_pair_data (*chain_pair_data_raw_proc)(const int_32 i, const hash_chain chain, const dh_table_t table);

typedef struct chain_ops_struct {
	chain_search_proc chain_search;
	chain_append_pair_proc chain_append_pair;
	chain_add_pair_proc chain_add_pair;
	chain_pop_remove_pair_proc chain_pop_remove_pair;
	chain_delete_proc chain_delete;
	chain_copy_key_buffer_proc chain_copy_key_buffer;
	chain_copy_value_buffer_proc chain_copy_value_buffer;
	chain_pair_data_raw_proc chain_pair_data_raw;
	chain_forward_pair_proc chain_forward_pair;
	chain_replace_pair_value_proc chain_replace_pair_value;
} chain_ops;

static void chainForwardPair_Fixed(const int_32 i, const hash_pair_data pair_data, hash_chain* from_chain, hash_chain* to_chain, const chain_ops_p ops)
{
	/* Copy to destination chain */
	ops->chain_append_pair(pair_data, to_chain);
	/* Remove from source chain */
	ops->chain_pop_remove_pair(i, pair_data.pairSize, from_chain);
}

static void chainForwardPair_Variable(const int_32 i, const hash_pair_data pair_data, hash_chain* from_chain, hash_chain* to_chain, const chain_ops_p ops)
{
	packed_parallel_arrays p_arrays_from, p_arrays_to;

	/* Copy to destination chain - will init an uninitialized store */
	chainIncrementStore(pair_data.pairSize, sizeof(pair_data_indirect), to_chain, updateChainLayout_Variable);
	p_arrays_from = chainParallelArrays(*from_chain), p_arrays_to = chainParallelArrays(*to_chain);
	p_arrays_to.hashes[to_chain->numPairs] = p_arrays_from.hashes[i];
	((pair_data_indirect*)(p_arrays_to.pairs))[to_chain->numPairs] = ((pair_data_indirect*)(p_arrays_from.pairs))[i];
	++(to_chain->numPairs);

	/* Remove from source chain */
	parallelArraysPopRemovePairIndirect(i, pair_data.pairSize, p_arrays_from, from_chain->numPairs);
	--(from_chain->numPairs);
}

/* Main hash table data type */
struct d_hasht_struct {
	hash_chain* table;					/* Dynamic array for table */
	int_32 tableSize;					/* Number of buckets in table */
	pair_size_64 pairSize;
	float maxLoad;						/* Maximum allowed load factor before resizing table */
	int_32 numPairs;					/* Number of currently stored pairs */
	int type;
	chain_ops ops; 
};

static hash_pair_data chainPairDataRaw(const int_32 i, pair_size_64 pair_size, const packed_parallel_arrays p_arrays, unsigned char* key_pointer, unsigned char* value_pointer)
{
	hash_pair_data pair_data;

	pair_data.hash = p_arrays.hashes[i];
	pair_data.key = key_pointer;
	pair_data.value = value_pointer;
	pair_data.pairSize = pair_size;

	return pair_data;
}

static hash_pair_data chainPairDataRaw_Fixed(const int_32 i, const hash_chain chain, const dh_table_t table)
{
	const packed_parallel_arrays p_arrays = chainParallelArrays(chain);
	pair_buf_pointers p_pointers = getPairBufPointers_Fixed(i, p_arrays, table->pairSize);

	return chainPairDataRaw(i, table->pairSize, p_arrays, p_pointers.key, p_pointers.value);
}

static hash_pair_data chainPairDataRaw_Variable(const int_32 i, const hash_chain chain, const dh_table_t table)
{
	const packed_parallel_arrays p_arrays = chainParallelArrays(chain);
	const pair_size_64 dummy_pair_size = { 0, 0 };
	pair_buf_pointers p_pointers = getPairBufPointers_Variable(i, p_arrays, dummy_pair_size);

	return chainPairDataRaw(i, ((pair_data_indirect*)(p_pointers.pair))->pairSize, p_arrays, p_pointers.key, p_pointers.value);
}

static const chain_ops g_chainOpsFixed = {
	NULL,
	chainAppendPair_Fixed,
	chainAddPair_Fixed,
	chainPopRemovePair_Fixed,
	chainDelete_Fixed,
	chainCopyKeyBuffer_Fixed,
	chainCopyValueBuffer_Fixed,
	chainPairDataRaw_Fixed,
	chainForwardPair_Fixed,
	chainReplacePairValue_Fixed
};

static const chain_ops g_chainOpsVariable = {
	chainSearch_Variable,
	chainAppendPair_Variable,
	chainAddPair_Variable,
	chainPopRemovePair_Variable,
	chainDelete_Variable,
	chainCopyKeyBuffer_Variable,
	chainCopyValueBuffer_Variable,
	chainPairDataRaw_Variable,
	chainForwardPair_Variable,
	chainReplacePairValue_Variable
};

/* The pair (key_size = 0, value_size = 0) is interpreted as variable-size pairs */
static struct d_hasht_struct tableNewEmptyStore(const int_32 table_size, const int_32 key_size, const int_32 value_size)
{
	struct d_hasht_struct table;
	
	table.table = NULL;
	table.tableSize = table_size;
	table.pairSize.key = key_size;
	table.pairSize.value = value_size;
	table.maxLoad = DH_DEFAULT_MAX_LOAD;
	table.numPairs = 0;

	if((key_size == 0) && (value_size == 0))
		table.ops = g_chainOpsVariable;

	else {
		table.ops = g_chainOpsFixed;
		table.ops.chain_search = (key_size <= sizeof(hash_64)) ? chainSearch_Fixed_NoHashCheck : chainSearch_Fixed_CheckHash;
	}

	return table;
}

static void tableInitStore(hash_chain* table_store, const int_32 num_chains)
{
	int_32 i;

	for(i = 0; i < num_chains; ++i)
		table_store[i] = chainNewEmptyStore();
}

static dh_table_t dh_create_unchecked(int_32 table_size_pairs, int_32 key_size, int_32 value_size, int table_type)
{
	dh_table_t new_table;
	uint_32 initial_table_size = ceilP2((table_size_pairs > 0) ? table_size_pairs : 1);

	/* Create new table */
	new_table = (dh_table_t)malloc(sizeof(struct d_hasht_struct));
	if(new_table == NULL)
		return(new_table);

	*new_table = tableNewEmptyStore(initial_table_size, key_size, value_size);
	new_table->type = table_type;

	/* Create table store */
	new_table->table = (hash_chain*)malloc(initial_table_size * sizeof(hash_chain));
	if(new_table->table == NULL) {
		free(new_table);
		return(NULL);
	}

	tableInitStore(new_table->table, new_table->tableSize);

	return(new_table);
}

dh_table_t dh_createf(int_32 table_size_pairs, int_32 key_size, int_32 value_size, int table_type)
{
	if((key_size > 0) && (value_size > 0))
		return dh_create_unchecked(table_size_pairs, key_size, value_size, table_type);

	return NULL;
}

dh_table_t dh_createv(int_32 table_size_pairs, int table_type)
{
	return dh_create_unchecked(table_size_pairs, 0, 0, table_type);
}

void dh_delete(dh_table_t table)
{
	int_32 i;

	if(table == NULL)
		return;

	for(i = 0; i < table->tableSize; ++i)
		table->ops.chain_delete(&(table->table[i]));

	free(table->table);
	free(table);
}

void dh_set_max_load(float max_load, dh_table_t table)
{
	if((table == NULL) || (max_load <= 0.0f))
		return;

	table->maxLoad = max_load;
}

int_32 dh_num_pairs(dh_table_t table)
{
	/* TODO: maybe change this to -1 ? */
	if(table == NULL)
		return 0;

	return table->numPairs;
}

static uint_32 tableChainIndex(const hash_64 hash, const int_32 table_num_chains)
{
	/* Only using hash.low to index table! */
	return (hash.low & hashMaskFromTableSize(table_num_chains));
}

static int tableGrow(dh_table_t table)
{
	const int_32 capacity_pre_resize = table->tableSize;
	const int_32 capacity_post_resize = capacity_pre_resize * 2;
	int rc = dynamicBufferResize((unsigned char**)&(table->table), capacity_post_resize * sizeof(hash_chain));
	int curr_chain_index = 0;

	if(rc != DH_OK)
		return rc;

	tableInitStore(table->table + capacity_pre_resize, capacity_pre_resize);
	table->tableSize = capacity_post_resize;

	/*
		Scan the first prev_table_size (first half of new table)
		and possibly re-assign some chain entries. All chain items
		will either stay put or be relocated somewhere *forward* (always)
		in the table store.
	*/
	for(curr_chain_index = 0; curr_chain_index < capacity_pre_resize; ++curr_chain_index) {
		hash_chain* curr_chain = &(table->table[curr_chain_index]);
		const packed_parallel_arrays p_arrays = chainParallelArrays(*curr_chain);
		int_32 curr_pair = 0;

		while(curr_pair < curr_chain->numPairs) {
			const int_32 new_chain_index = tableChainIndex(p_arrays.hashes[curr_pair], capacity_post_resize);

			if(new_chain_index != curr_chain_index) {
				const hash_pair_data pair_data = table->ops.chain_pair_data_raw(curr_pair, *curr_chain, table);

				table->ops.chain_forward_pair(curr_pair, pair_data, curr_chain, &(table->table[new_chain_index]), &(table->ops));
			}
			else
				++curr_pair;
		}
	}

	return DH_OK;
}

static int dh_addv_unchecked(const void* key, int_32 key_size, const void* value, int_32 value_size, dh_table_t table)
{
	const hash_pair_data pair_data = pairDataCreate(hash_buffer((const unsigned char*)key, key_size), key, key_size, value, value_size);
	hash_chain* chain = &(table->table[tableChainIndex(pair_data.hash, table->tableSize)]);
	int rc = table->ops.chain_add_pair(pair_data, chain, table->ops.chain_search);

	if(rc != DH_OK)
		return rc;

	++(table->numPairs);

	/* Grow table here! */
	if(table->type == DH_DYNAMIC) {
		float current_load = ((float)table->numPairs) / ((float)table->tableSize);
		if(current_load >= table->maxLoad)
			return(tableGrow(table));
	}

	return rc;
}

int dh_addv(const void* key, int_32 key_size, const void* value, int_32 value_size, dh_table_t table)
{
	if((key != NULL) && (table != NULL))
		if((value_size == 0) || (value != NULL))
			return dh_addv_unchecked(key, key_size, value, value_size, table);

	return DH_INVALID_PARAMETER_ERROR;
}

int dh_addf(const void* key, const void* value, dh_table_t table)
{
	if((key != NULL) && (value != NULL) && (table != NULL))
		return dh_addv_unchecked(key, table->pairSize.key, value, table->pairSize.value, table);

	return DH_INVALID_PARAMETER_ERROR;
}

/* No bounds checking! */
static hash_pair_data iPairDataRaw(const dh_ipair_t* ipair, hash_chain** chain)
{
	*chain = &(ipair->table->table[ipair->iChain]);
	return ipair->table->ops.chain_pair_data_raw(ipair->iPair, **chain, ipair->table);
}

static int dh_ipairv_unchecked(const void* key, const int_32 key_size, dh_table_t table, dh_ipair_t* ipair)
{
	hash_pair_data pair_data = pairDataCreate(hash_buffer((const unsigned char*)key, key_size), key, key_size, NULL, table->pairSize.value);
	uint_32 chain_index = tableChainIndex(pair_data.hash, table->tableSize);
	hash_chain* chain = &(table->table[chain_index]);
	const int_32 pair_match = table->ops.chain_search(pair_data, chain);

	if(pair_match >= 0) {
		ipair->iChain = chain_index;
		ipair->iPair = pair_match;
		ipair->table = table;

		return DH_OK;
	}

	return DH_NOT_FOUND;
}

static void dh_getv_unchecked(void* value, int_32* value_size, const dh_ipair_t* ipair)
{
	hash_chain* chain;
	hash_pair_data pair_data = iPairDataRaw(ipair, &chain);

	if(value != NULL)
		ipair->table->ops.chain_copy_value_buffer(ipair->iPair, value, pair_data.pairSize, *chain);

	if(value_size != NULL)
		*value_size = pair_data.pairSize.value;
}

int dh_getv(const void* key, int_32 key_size, void* value, int_32* value_size, dh_table_t table)
{
	if((key != NULL) && (table != NULL)) {
		dh_ipair_t ipair;
		int rc = dh_ipairv_unchecked(key, key_size, table, &ipair);

		if(rc == DH_OK)
			dh_getv_unchecked(value, value_size, &ipair);

		return rc;
	}

	return DH_INVALID_PARAMETER_ERROR;
}

int dh_getf(const void* key, void* value, dh_table_t table)
{
	if((key != NULL) && (table != NULL)) {
		dh_ipair_t ipair;
		int rc = dh_ipairv_unchecked(key, table->pairSize.key, table, &ipair);

		if(rc == DH_OK)
			dh_getv_unchecked(value, NULL, &ipair);

		return rc;
	}

	return DH_INVALID_PARAMETER_ERROR;
}

static void dh_remv_unchecked(const dh_ipair_t* ipair)
{
	hash_chain* chain;
	const hash_pair_data pair_data = iPairDataRaw(ipair, &chain);

	ipair->table->ops.chain_pop_remove_pair(ipair->iPair, pair_data.pairSize, chain);
	--(ipair->table->numPairs);
}

int dh_remv(const void* key, int_32 key_size, dh_table_t table)
{
	if((key != NULL) && (table != NULL)) {
		dh_ipair_t ipair;
		int rc = dh_ipairv_unchecked(key, key_size, table, &ipair);

		if(rc == DH_OK)
			dh_remv_unchecked(&ipair);

		return rc;
	}

	return DH_INVALID_PARAMETER_ERROR;
}

int dh_remf(const void* key, dh_table_t table)
{
	if((key != NULL) && (table != NULL)) {
		dh_ipair_t ipair;
		int rc = dh_ipairv_unchecked(key, table->pairSize.key, table, &ipair);

		if(rc == DH_OK)
			dh_remv_unchecked(&ipair);

		return rc;
	}

	return DH_INVALID_PARAMETER_ERROR;
}

static int dh_ipair_next_unchecked(dh_ipair_t* ipair)
{
	++(ipair->iPair);
	while(ipair->iChain < ipair->table->tableSize) {
		if(ipair->iPair < ipair->table->table[ipair->iChain].numPairs)
			return(DH_OK);

		++(ipair->iChain);
		ipair->iPair = 0;
	}

	return DH_END;
}

dh_ipair_t dh_ipair_begin(dh_table_t table)
{
	dh_ipair_t ipair;
	
	ipair.iChain = 0;
	ipair.iPair = 0;
	ipair.table = table;

	if(dh_ipair_chk(&ipair) == DH_OUT_OF_BOUNDS)
		dh_ipair_next_unchecked(&ipair);

	return ipair;
}

int dh_ipair_next(dh_ipair_t* ipair)
{
	/* Advance iterator to next valid pair */
	if((ipair != NULL) && (ipair->table != NULL))
		return dh_ipair_next_unchecked(ipair);

	return DH_INVALID_PARAMETER_ERROR;
}

const char* dh_rc_str(const int rc)
{
	static const char* rc_str[] = {
		"DH_OK",
		"DH_REPLACE",
		"DH_NOT_FOUND",
		"DH_ERROR",
		"DH_RESIZE_ERROR",
		"DH_INVALID_PARAMETER_ERROR",
		"DH_END",
		"DH_OUT_OF_BOUNDS"
	};

	return rc_str[rc];
}

int dh_ipairv(const void* key, const int_32 key_size, dh_table_t table, dh_ipair_t* ipair)
{
	if((key != NULL) && (table != NULL) && (ipair != NULL))
		return dh_ipairv_unchecked(key, key_size, table, ipair);

	return DH_INVALID_PARAMETER_ERROR;
}

int dh_ipairf(const void* key, dh_table_t table, dh_ipair_t* ipair)
{
	if((key != NULL) && (table != NULL) && (ipair != NULL))
		return dh_ipairv_unchecked(key, table->pairSize.key, table, ipair);

	return DH_INVALID_PARAMETER_ERROR;
}

int dh_ipair_chk(const dh_ipair_t* ipair)
{
	if((ipair == NULL) || (ipair->table == NULL))
		return DH_INVALID_PARAMETER_ERROR;

	if(ipair->iChain >= ipair->table->tableSize)
		return DH_OUT_OF_BOUNDS;

	if(ipair->iPair >= ipair->table->table[ipair->iChain].numPairs)
		return DH_OUT_OF_BOUNDS;

	return DH_OK;
}

int dh_ipair_get(void* key, int_32* key_size, void* value, int_32* value_size, const dh_ipair_t* ipair)
{
	int rc = dh_ipair_chk(ipair);

	if(rc == DH_OK) {
		hash_chain* chain;
		hash_pair_data pair_data = iPairDataRaw(ipair, &chain);

		if(key != NULL)
			ipair->table->ops.chain_copy_key_buffer(ipair->iPair, key, pair_data.pairSize, *chain);

		if(key_size != NULL)
			*key_size = pair_data.pairSize.key;

		if(value != NULL)
			ipair->table->ops.chain_copy_value_buffer(ipair->iPair, value, pair_data.pairSize, *chain);

		if(value_size != NULL)
			*value_size = pair_data.pairSize.value;
	}

	return rc;
}

int dh_ipair_updatev(const void* value, int_32 value_size, const dh_ipair_t* ipair)
{
	if((value_size == 0) || (value != NULL)) {
		int rc = dh_ipair_chk(ipair);

		if(rc == DH_OK) {
			hash_chain* chain;
			hash_pair_data pair_data = iPairDataRaw(ipair, &chain);

			pair_data.value = (unsigned char*)value;
			pair_data.pairSize.value = value_size;
			ipair->table->ops.chain_replace_pair_value(ipair->iPair, pair_data, chain);
		}

		return rc;
	}

	return DH_INVALID_PARAMETER_ERROR;
}

int dh_ipair_updatef(const void* value, const dh_ipair_t* ipair)
{
	return dh_ipair_updatev(value, ipair->table->pairSize.value, ipair);
}


/*** Debug ***/
/*
	Text dumps!
*/
static void pairBufferDump(const unsigned char* buffer, int_32 len)
{
	int_32 i;

	for(i = 0; i < len; ++i)
		printf("%.2x", buffer[i]);
}

static void pairDump(const hash_pair_data pair_data)
{
	printf("\tpair: hash(low: %u, high: %u), size: (k:%d, v:%d), key: ", pair_data.hash.low, pair_data.hash.high, pair_data.pairSize.key, pair_data.pairSize.value);
	pairBufferDump(pair_data.key, pair_data.pairSize.key);
	printf(", value: ");
	pairBufferDump(pair_data.value, pair_data.pairSize.value);
	printf("\n");
}

static void chainDump(const hash_chain chain, const dh_table_t table, const chain_pair_data_raw_proc chain_pair_data_raw)
{
	int_32 i = 0;

	printf("chain: numPairs(%d), capacityPairs(%d), pairs:\n", chain.numPairs, chain.capacityPairs);
	for(i = 0; i < chain.numPairs; ++i)
		pairDump(chain_pair_data_raw(i, chain, table));

	printf("\n");
}

void dh_dump(dh_table_t table)
{
	printf("-=-=--=-=-=-=-=-=-=-=-= TABLE DUMP START --=-=-=-=-=-=-====-=-=-=\n");
	printf("table header: numPairs(%d), tableSize(%d), maxLoad(%f), pairSize(k:%d, v:%d)\n", table->numPairs, table->tableSize, table->maxLoad, table->pairSize.key, table->pairSize.value);

	/* Chains */
	{
		int_32 i;
		int_32 num_empty_chains = 0;
		int_32 num_used_chains = 0;

		for(i = 0; i < table->tableSize; ++i) {
			const hash_chain* curr_chain = &(table->table[i]);

			chainDump(*curr_chain, table, table->ops.chain_pair_data_raw);

			if((curr_chain->capacityPairs == 0) && (curr_chain->parallelArrays == NULL))
				++num_empty_chains;
			else
				++num_used_chains;
		}

		printf("table has %d live chains and %d unused chains\n", num_used_chains, num_empty_chains);
	}
}

void dh_dump_ipair(dh_ipair_t ipair)
{
	printf("ipair = (chain: %d, ipair: %d, table: %p)\n", ipair.iChain, ipair.iPair, ipair.table);
}

