/*	Aligned memory allocation.
	Author: Miguel A. Osorio - Nov 24 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#ifndef _MK1_ALIGNED_ALLOC_H_
#define _MK1_ALIGNED_ALLOC_H_

#ifdef __cplusplus
extern "C" {
#endif


void* mk1_aligned_alloc(const size_t size, const unsigned char alignment, void* (*alloc_f)(size_t size));
void mk1_aligned_free(void* buf, void (*free_f)(void * buf));

/*** Page allocator ***/
typedef struct rs_page_buckets* page_allocator_t;

page_allocator_t page_allocator_create(const size_t min_page_size);
void page_allocator_delete(page_allocator_t pa);
void* paged_alloc(const size_t size, const size_t align, page_allocator_t pa);
void page_allocator_reset(page_allocator_t pa);


struct page_bucket_info {
	size_t free;
	size_t used;
};

struct page_allocator_info {
	struct page_bucket_info page_buckets[32];
	size_t total_alloc_req;
	size_t total_alloc_waste;
};

struct page_allocator_info page_allocator_info(const page_allocator_t pa);
void page_allocator_info_dump(const page_allocator_t pa);


#ifdef __cplusplus
}
#endif

#endif

