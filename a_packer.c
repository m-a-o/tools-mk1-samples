/*	Array packer.
	Author: Miguel A. Osorio - Mar 13 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/



#include <stdio.h>

#include "a_packer.h"




/*
	Align to powers of 2. The initial address is shifted by up to
	[alignment - 1] bytes.
*/
const size_t offsetToAligned(const size_t alignment, const void *buf)
{
	size_t alignoff = 0;

	if(alignment > 0) {
		const size_t mask = alignment - 1;
		const size_t addr = (size_t)buf;
		alignoff = (addr & mask) ? alignment - (addr & mask) : 0;
	}

	return alignoff;
}

const void* alignToOffset(const size_t offset, const void *buf)
{
	return (const char*)buf + offset;
}

const void* alignTo(const size_t alignment, const void *buf)
{
	return (const char*)buf + offsetToAligned(alignment, buf);
}

size_t packed_array_h_max_size(const struct packed_array h)
{
	return h.size + (h.alignment - 1);
}

size_t packed_array_fmt_max_size(const struct packed_array *spec, const size_t n)
{
	size_t i, size;

	for(i = 0, size = 0; i < n; ++i)
		size += packed_array_h_max_size(spec[i]);

	return size;
}

struct packed_array_it packed_array_it_start(const void* buf)
{
	struct packed_array_it it;

	it.base = buf;
	it.offset = 0;

	return it;
}

size_t packed_array_walk(const struct packed_array h, struct packed_array_it* it)
{
	const size_t local_offset = offsetToAligned(h.alignment, (unsigned char*)it->base + it->offset);
	size_t curr_offset = it->offset + local_offset;

	it->offset += h.size + local_offset;

	return curr_offset;
}

