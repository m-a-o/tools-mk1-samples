/*
	-=- Farm parallelism pattern tests -=-
*/

/*
	Copyright (c) 2010-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


/*
	Testing the "Farm" (otherwise known as master-slave or boss-worker) parallelism pattern.
	The idea is to use a barrier to synch all slaves for starting jobs and then a condition
	variable to signal the master thread when all jobs are complete.

	Note the attempt at a more or less generic implementation. Jobs are arbitrary functions
	(using a common entry point), and can synchronize between themselves using other unique
	sync state. If each level uses it's own sync state for a Farm, nesting is allowed, making
	job hierarchies possible.

	BUT since this is my first attempt at trying somthing in parallel computing, it might
	have (read: "probably has") code bugs and/or worse, design bugs, which I've heard are the
	nasty ones. Plus, there are plenty of hacks scattered around.

	TODO:

	-> I'd like to explore this solution with some changes to thread management. Like keeping
	a pool of pre-spawned threads that wait for signals for starting and stopping. So instead of
	spawning threads on demand, we signal waiting ones so they wake up and do work, then go back
	to sleep. I can imagine needing some way of managing the work payloads, since data is passed
	when the threads are spawned, so...

	-> It is possible to "enforce" which farm_sync_t data is available to code on different
	levels. Some variables should only be accessed by the farm boss, while others by neither
	boss or workers. For each level, we could present an effective different type, but this is
	too verbose. Inside the API, one's supposed to know how not to botch the data!
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stddef.h>

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <process.h>
#else
#include <unistd.h>
#include <pthread.h>
#endif

#include "d_array.h"
#include "farm.h"


/* Farm resources */
#define FARM_RESOURCE_BARRIER (1 << 0)
#define FARM_RESOURCE_COND_END (1 << 1)
#define FARM_RESOURCE_COND_MUTEX (1 << 2)
#define FARM_RESOURCE_STAT_MUTEX (1 << 3)

/*
	Some POSIX platforms do not have barriers implemented!
	These are NOT generic! I've only implemented the basics I use!
*/
#ifdef __APPLE__
#ifdef __MACH__
#define MACOSX
#endif
#endif

#ifdef MACOSX
typedef struct {
	pthread_cond_t conditionVar;
	pthread_mutex_t mutex;
	int count;
	int waiting;
} pthread_barrier_t;

static int pthread_barrier_init(pthread_barrier_t* barrier, const void* attr, int count)
{
	pthread_mutex_init(&barrier->mutex, NULL);
	pthread_mutex_lock(&barrier->mutex);

	pthread_cond_init(&barrier->conditionVar, NULL);
	barrier->count = count;
	barrier->waiting = 0;

	pthread_mutex_unlock(&barrier->mutex);

	return 0;
}

static int pthread_barrier_destroy(pthread_barrier_t* barrier)
{
	pthread_mutex_lock(&barrier->mutex);
	pthread_cond_destroy(&barrier->conditionVar);
	pthread_mutex_unlock(&barrier->mutex);
	pthread_mutex_destroy(&barrier->mutex);

	return 0;
}

static int pthread_barrier_wait(pthread_barrier_t* barrier)
{
	pthread_mutex_lock(&barrier->mutex);

	++(barrier->waiting);
	if(barrier->waiting == barrier->count)
		pthread_cond_broadcast(&barrier->conditionVar);

	else
		while(barrier->waiting < barrier->count)
			pthread_cond_wait(&barrier->conditionVar, &barrier->mutex);

	pthread_mutex_unlock(&barrier->mutex);

	/* Always zero, careful if using the return value! */
	return 0;
}
#endif

/* Synch apparatus */
struct farm_sync {

	int totalWorkers;
#ifdef WIN32
	CONDITION_VARIABLE barrierCond;
	CRITICAL_SECTION barrierMutex;
	int barrierCount;

	uintptr_t bossTID;
	CRITICAL_SECTION statMutex;
#else
	pthread_t bossTID;
	pthread_barrier_t barrierStart;
	pthread_cond_t condEnd;
	pthread_mutex_t condMutex;
	pthread_mutex_t statMutex;
	int numActiveWorkers;		/* This is the actual condition variable; number of workers currently active */
#endif
	farm_status_t status; 
	struct {
		union {
			farm_payload_t single;
			farm_payload_t* many;
		} payload;	/* Only the boss ever uses this */
		int options;
	} boss;
	int resources;				/* Only the delete routine ever uses this... */

};

/* Farm creation */
farm_sync_t* farm_create(int num_jobs)
{
	farm_sync_t* f_sync = (farm_sync_t*)malloc(sizeof(farm_sync_t));

	if(f_sync == NULL)
		return(NULL);

	/* Start clean */
	memset(f_sync, 0x00, sizeof(farm_sync_t));

	/* Try init */
	do {

#ifdef WIN32
		InitializeCriticalSection(&(f_sync->barrierMutex));
		InitializeConditionVariable(&(f_sync->barrierCond));
		f_sync->barrierCount = 0;
#else
		/* Workers */
		if(pthread_barrier_init(&(f_sync->barrierStart), NULL, num_jobs + 1))
			break;
#endif
		f_sync->resources |= FARM_RESOURCE_BARRIER;

#ifndef WIN32
		if(pthread_cond_init(&(f_sync->condEnd), NULL))
			break;

		f_sync->resources |= FARM_RESOURCE_COND_END;

		if(pthread_mutex_init(&(f_sync->condMutex), NULL))
			break;

		f_sync->resources |= FARM_RESOURCE_COND_MUTEX;
#endif

#ifdef WIN32
		InitializeCriticalSection(&(f_sync->statMutex));
#else
		if(pthread_mutex_init(&(f_sync->statMutex), NULL))
			break;
#endif
		f_sync->resources |= FARM_RESOURCE_STAT_MUTEX;

		/* No locking needed here because the farm hasn't been born yet! */
#ifndef WIN32
		f_sync->numActiveWorkers = num_jobs;
#endif
		f_sync->totalWorkers = num_jobs;
		f_sync->status = FARM_STAT_SETUP;

		/* Ok! */
		return(f_sync);

	} while(0);

	/* Bogus */
	farm_delete(f_sync);
	return(NULL);
}

/* Farm deletion */
void farm_delete(farm_sync_t* f_sync)
{
	if(f_sync == NULL)
		return;

	if(FARM_ISSET(f_sync->resources, FARM_RESOURCE_BARRIER)) {
#ifdef WIN32
		DeleteCriticalSection(&(f_sync->barrierMutex));
#else
		pthread_barrier_destroy(&(f_sync->barrierStart));
#endif
	}

#ifndef WIN32
	if(FARM_ISSET(f_sync->resources, FARM_RESOURCE_COND_END))
		pthread_cond_destroy(&(f_sync->condEnd));

	if(FARM_ISSET(f_sync->resources, FARM_RESOURCE_COND_MUTEX))
		pthread_mutex_destroy(&(f_sync->condMutex));
#endif

	if(FARM_ISSET(f_sync->resources, FARM_RESOURCE_STAT_MUTEX)) {
#ifdef WIN32
		DeleteCriticalSection(&(f_sync->statMutex));
#else
		pthread_mutex_destroy(&(f_sync->statMutex));
#endif
	}

#ifdef WIN32
	if(f_sync->bossTID != 0)
		CloseHandle((HANDLE)(f_sync->bossTID));
#endif

	free(f_sync);

	return;
}

/* Single job payload */
typedef struct job_payload {

	int id;
	farm_sync_t* farmSync;
	void* data;
	farm_job_proc job;

} job_payload_t;

#ifdef WIN32
static void aux_win32_barrier_wait(farm_sync_t* f_sync, int n_jobs)
{
	EnterCriticalSection(&(f_sync->barrierMutex));

	++(f_sync->barrierCount);
	if(f_sync->barrierCount == n_jobs)
		WakeAllConditionVariable(&(f_sync->barrierCond));
	else
		while(f_sync->barrierCount < f_sync->totalWorkers)
			SleepConditionVariableCS(&(f_sync->barrierCond), &(f_sync->barrierMutex), INFINITE);

	LeaveCriticalSection(&(f_sync->barrierMutex));

	return;
}
#endif

/*
	Basic slave skeleton. Data received is a job_payload_t.
*/
#ifdef WIN32
static unsigned int __stdcall farm_worker(void* data)
#else
static void* farm_worker(void* data)
#endif
{
	/*
		Outline: start waiting on the barrier, do work, then the last worker signals
		the master thread using the condition variable
	*/

	/* Get work payload */
	job_payload_t* payload = (job_payload_t*)data;

	/* Wait for barrier lift */
#ifdef WIN32
	aux_win32_barrier_wait(payload->farmSync, payload->farmSync->totalWorkers + 1);
#else
	pthread_barrier_wait(&(payload->farmSync->barrierStart));
#endif

	/* Do work */
	/*
		Note that in this example, the jobs should not be able to access the synchronization controls
		for the worker threads. If we wish to synchronize between jobs, the payload->data should
		take care of providing controls for that...
	*/
	payload->job(payload->id, payload->data);

	/* Work done, we should update our control var, and the last thread turns off the lights! */
#ifndef WIN32
	pthread_mutex_lock(&(payload->farmSync->condMutex));
	--(payload->farmSync->numActiveWorkers);
	if(payload->farmSync->numActiveWorkers <= 0)
		pthread_cond_signal(&(payload->farmSync->condEnd));

	pthread_mutex_unlock(&(payload->farmSync->condMutex));
#endif

	/* At this point, we're done and can finish this thread */
#ifdef WIN32
	return(0);
#else
	return(NULL);
#endif
}

/*
	Basic master skeleton. Data received is a farm_sync_t*. Once the we setup
	all jobs, the client is free to modify the payload data, but we only actualy
	signal this a bit later on, when work has probably already started.
*/

#ifdef WIN32
static unsigned int __stdcall farm_boss(void* data)
#else
static void* farm_boss(void* data)
#endif
{
	/*
		Outline: the boss sets up the work payload, initializes the barrier and spawns all workers.
		Note that the last spawned worker will lift the barrier and start all jobs. The boss then
		goes to sleep waiting on the condition variable to signal.
	*/

	farm_sync_t* f_sync = (farm_sync_t*)data;
	int num_workers = f_sync->totalWorkers;
	int i, options = f_sync->boss.options;
	job_payload_t* jobs;
#ifdef WIN32
	uintptr_t* workers;
#else
	pthread_t* workers;
#endif

	/* Payload */
	jobs = (job_payload_t*)malloc(num_workers * sizeof(job_payload_t));
	for(i = 0; i < num_workers; ++i) {
		jobs[i].id = i;
		jobs[i].farmSync = f_sync;
		if(FARM_ISSET(options, FARM_OPT_PAYLOAD_REPLICATE)) {
			jobs[i].data = f_sync->boss.payload.single.data;
			jobs[i].job = f_sync->boss.payload.single.job;
		}
		else {
			jobs[i].data = f_sync->boss.payload.many[i].data;
			jobs[i].job = f_sync->boss.payload.many[i].job;
		}
	}

	/* Make sure nobody else attempts to use this... */
	memset(&(f_sync->boss.payload), 0, sizeof(f_sync->boss.payload));

	/* Threads */
#ifdef WIN32
	workers = (uintptr_t*)malloc(sizeof(uintptr_t) * num_workers);
#else
	workers = (pthread_t*)malloc(sizeof(pthread_t) * num_workers);
#endif

	/* Spawn workers */
	for(i = 0; i < num_workers; ++i) {
#ifdef WIN32
		workers[i] = _beginthreadex(NULL, 0, farm_worker, &jobs[i], 0, NULL);
#else
		pthread_create(&(workers[i]), NULL, farm_worker, &jobs[i]);
#endif
	}

	/* Make sure everyone syncs at this point (well defined states!)*/
#ifdef WIN32
	aux_win32_barrier_wait(f_sync, f_sync->totalWorkers + 1);
#else
	pthread_barrier_wait(&(f_sync->barrierStart));
#endif

	/* Work has just begun... */
#ifdef WIN32
	EnterCriticalSection(&(f_sync->statMutex));
#else
	pthread_mutex_lock(&(f_sync->statMutex));
#endif
	f_sync->status = FARM_STAT_WORKING;
#ifdef WIN32
	LeaveCriticalSection(&(f_sync->statMutex));
#else
	pthread_mutex_unlock(&(f_sync->statMutex));
#endif

	/* At this point, workers are already cranking at their jobs, so it's time for us to sit back and relax */
#ifdef WIN32
	WaitForMultipleObjects(num_workers, (HANDLE*)workers, 1, INFINITE);
#else
	pthread_mutex_lock(&(f_sync->condMutex));
	while(f_sync->numActiveWorkers != 0)
		pthread_cond_wait(&(f_sync->condEnd), &(f_sync->condMutex));

	pthread_mutex_unlock(&(f_sync->condMutex));
#endif

	/* Work has just finished... */
#ifdef WIN32
	EnterCriticalSection(&(f_sync->statMutex));
#else
	pthread_mutex_lock(&(f_sync->statMutex));
#endif
	f_sync->status = FARM_STAT_DONE;
#ifdef WIN32
	LeaveCriticalSection(&(f_sync->statMutex));
#else
	pthread_mutex_unlock(&(f_sync->statMutex));
#endif

	/* Getting here means all jobs are done and we're ready to wrap up this farm session. Have a nice day! */
#ifdef WIN32
	for(i = 0; i < num_workers; ++i)
		CloseHandle((HANDLE)workers[i]);
#endif
	free(jobs);
	free(workers);

	/* All done! */
#ifdef WIN32
	return(0);
#else
	return(NULL);
#endif
}

/* Launch farm */
int farm_start(farm_sync_t* f_sync, farm_payload_t* payload, int options)
{
	/* Basic sanity check */
	if(f_sync == NULL || payload == NULL)
		return(0);

	/* Setup payload */
	if(FARM_ISSET(options, FARM_OPT_PAYLOAD_REPLICATE))
		f_sync->boss.payload.single = *payload;
	else
		f_sync->boss.payload.many = payload;

	f_sync->boss.options = options;

	/* Spawn boss */
#ifdef WIN32
	f_sync->bossTID = _beginthreadex(NULL, 0, farm_boss, f_sync, 0, NULL);
#else
	pthread_create(&(f_sync->bossTID), NULL, farm_boss, f_sync);
#endif

	/* Option to wait for the farm to complete or just return to caller */
	if(!FARM_ISSET(options, FARM_OPT_NO_WAIT)) {
#ifdef WIN32
		WaitForSingleObject((HANDLE)(f_sync->bossTID), INFINITE);
#else
		pthread_join(f_sync->bossTID, NULL);
#endif
	}

	return(1);
}

int farm_go(farm_sync_t* f_sync, void* data, farm_job_proc job, int options)
{
	farm_payload_t payload;

	payload.data = data;
	payload.job = job;

	return(farm_start(f_sync, &payload, options | FARM_OPT_PAYLOAD_REPLICATE));
}

/* Check on farm */
farm_status_t farm_check(farm_sync_t* f_sync)
{
	farm_status_t curr_stat;

	/* Try to read status */
#ifdef WIN32
	if(!TryEnterCriticalSection(&(f_sync->statMutex)))
#else
	if(pthread_mutex_trylock(&(f_sync->statMutex)) != 0)
#endif
		return(FARM_STAT_UNDEFINED);

	curr_stat = f_sync->status;
#ifdef WIN32
	LeaveCriticalSection(&(f_sync->statMutex));
#else
	pthread_mutex_unlock(&(f_sync->statMutex));
#endif

	return(curr_stat);
}

/* Wait for farm to complete */
void farm_wait_for(farm_sync_t* f_sync)
{
#ifdef WIN32
	WaitForSingleObject((HANDLE)(f_sync->bossTID), INFINITE);
#else
	pthread_join(f_sync->bossTID, NULL);
#endif

	return;
}

void fork_join(int n_jobs, void* data, farm_job_proc job, int options)
{
	farm_sync_t* farm;

	farm = farm_create(n_jobs);
	farm_go(farm, data, job, options & (~FARM_OPT_NO_WAIT));

	farm_delete(farm);

	return;
}

typedef struct {
	mem_int_t offset;
	mem_int_t size;
} pfor_job_data_t;

typedef struct {
	void* items;
	size_t itemSize;
	mem_int_t n;
	struct {
		void* state;
		parallel_for_proc proc;
	} body;
	pfor_job_data_t* jobs;
} pfor_state_t;

static int auxParallelMForPROC(int id, void* data)
{
	pfor_state_t* for_state = (pfor_state_t*)data;
	pfor_job_data_t* payload = &(for_state->jobs[id]);
	mem_int_t i, iEnd;

	for(i = payload->offset, iEnd = payload->offset + payload->size; i < iEnd; ++i)
		for_state->body.proc(i, (char*)for_state->items + (i * for_state->itemSize), for_state->body.state);

	return 0;
}

void parallel_for(int n_jobs, mem_int_t n, void* items, size_t item_size, void* state, parallel_for_proc proc)
{
	pfor_state_t for_state;
	mem_int_t i, job_size, rem, istart;

	for_state.jobs = (pfor_job_data_t*)malloc(n_jobs * sizeof(pfor_job_data_t));
	memset(for_state.jobs, 0x00, n_jobs * sizeof(pfor_job_data_t));

	job_size = n / n_jobs;
	rem = n % n_jobs;
	for(i = 0, istart = 0; i < n_jobs; ++i) {
		mem_int_t j_sz = job_size + ((i < rem) ? 1 : 0);

		for_state.jobs[i].size = j_sz;
		for_state.jobs[i].offset = istart;

		istart += j_sz;
	}

	for_state.items = items;
	for_state.itemSize = item_size;
	for_state.n = n;
	for_state.body.state = state;
	for_state.body.proc = proc;

	fork_join(n_jobs, &for_state, auxParallelMForPROC, 0);

	free(for_state.jobs);

	return;
}

