/* V - Vector library
   Author: Miguel A. Osorio */

/*
	Copyright (c) 2002-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


#include <math.h>
#include <string.h>
/* DEBUG */
#include <stdio.h>
/* DEBUG */

#include "v.h"


/* PI */
#ifndef M_PI

#define M_PI 3.14159265358979323846f

#endif


/*** Core functions ***/

/* Set null vector */
void vZero2D(Vvector2D v)
{
	v[_X_] = v[_Y_] = 0.0f;
}


void vZero3D(Vvector3D v)
{
	v[_X_] = v[_Y_] = v[_Z_] = 0.0f;
}


/* Negation */
void vNeg2D(const Vvector2D v, Vvector2D res)
{
	res[_X_] = - v[_X_];
	res[_Y_] = - v[_Y_];
}


void vNeg3D(const Vvector3D v, Vvector3D res)
{
	res[_X_] = - v[_X_];
	res[_Y_] = - v[_Y_];
	res[_Z_] = - v[_Z_];
}


/* Assignment */
void vSet2D(Vvector2D v, const Vfloat x, const Vfloat y)
{
	v[_X_] = x;
	v[_Y_] = y;
}


void vSet3D(Vvector3D v, const Vfloat x, const Vfloat y, const Vfloat z)
{
	v[_X_] = x;
	v[_Y_] = y;
	v[_Z_] = z;
}


void vSet4D(Vvector4D v, const Vfloat x, const Vfloat y, const Vfloat z, const Vfloat w)
{
	v[_X_] = x;
	v[_Y_] = y;
	v[_Z_] = z;
	v[_W_] = w;
}


/* Copying */
void vCopy2D(Vvector2D a, const Vvector2D b)
{
	a[_X_] = b[_X_];
	a[_Y_] = b[_Y_];
}


void vCopy3D(Vvector3D a, const Vvector3D b)
{
	a[_X_] = b[_X_];
	a[_Y_] = b[_Y_];
	a[_Z_] = b[_Z_];
}


void vCopy4D(Vvector4D a, const Vvector4D b)
{
	a[_X_] = b[_X_];
	a[_Y_] = b[_Y_];
	a[_Z_] = b[_Z_];
	a[_W_] = b[_W_];
}


/* Dot product */
Vfloat vDot2D(const Vvector2D a, const Vvector2D b)
{
	return((a[_X_] * b[_X_]) + (a[_Y_] * b[_Y_]));
}


Vfloat vDot3D(const Vvector3D a, const Vvector3D b)
{
	return((a[_X_] * b[_X_]) + (a[_Y_] * b[_Y_]) + (a[_Z_] * b[_Z_]));
}

Vfloat vDot4D(const Vvector3D a, const Vvector3D b)
{
	return((a[_X_] * b[_X_]) + (a[_Y_] * b[_Y_]) + (a[_Z_] * b[_Z_]) + (a[_W_] * b[_W_]));
}


/* Perp dot product (2D) */
void vPerp2D(const Vvector2D a, Vvector2D res)
{
	Vfloat swap;

	swap = a[_X_];
	res[_X_] = - a[_Y_];
	res[_Y_] = swap;
}


/* Cross product */
void vCross3D(const Vvector3D a, const Vvector3D b, Vvector3D res)
{
	Vvector3D a_copy, b_copy;

	vCopy3D(a_copy, a);
	vCopy3D(b_copy, b);

	res[_X_] = (a_copy[_Y_] * b_copy[_Z_]) - (a_copy[_Z_] * b_copy[_Y_]);
	res[_Y_] = (a_copy[_Z_] * b_copy[_X_]) - (a_copy[_X_] * b_copy[_Z_]);
	res[_Z_] = (a_copy[_X_] * b_copy[_Y_]) - (a_copy[_Y_] * b_copy[_X_]);
}


/* Addition */
void vAdd2D(const Vvector2D a, const Vvector2D b, Vvector2D res)
{
	res[_X_] = a[_X_] + b[_X_];
	res[_Y_] = a[_Y_] + b[_Y_];
}


void vAdd3D(const Vvector3D a, const Vvector3D b, Vvector3D res)
{
	res[_X_] = a[_X_] + b[_X_];
	res[_Y_] = a[_Y_] + b[_Y_];
	res[_Z_] = a[_Z_] + b[_Z_];
}


/* Subtraction */
void vSub2D(const Vvector2D a, const Vvector2D b, Vvector2D res)
{
	res[_X_] = a[_X_] - b[_X_];
	res[_Y_] = a[_Y_] - b[_Y_];
}


void vSub3D(const Vvector3D a, const Vvector3D b, Vvector3D res)
{
	res[_X_] = a[_X_] - b[_X_];
	res[_Y_] = a[_Y_] - b[_Y_];
	res[_Z_] = a[_Z_] - b[_Z_];
}


/* Scaling */
void vScale2D(const Vvector2D v, const Vfloat s, Vvector2D res)
{
	res[_X_] = v[_X_] * s;
	res[_Y_] = v[_Y_] * s;
}


void vScale3D(const Vvector3D v, const Vfloat s, Vvector3D res)
{
	res[_X_] = v[_X_] * s;
	res[_Y_] = v[_Y_] * s;
	res[_Z_] = v[_Z_] * s;
}


/* Norm */
Vfloat vNorm2D(const Vvector2D v)
{
	return((Vfloat)sqrt((v[_X_] * v[_X_]) + (v[_Y_] * v[_Y_])));
}


Vfloat vNorm3D(const Vvector3D v)
{
	return((Vfloat)sqrt((v[_X_] * v[_X_]) + (v[_Y_] * v[_Y_]) + (v[_Z_] * v[_Z_])));
}


/* Squared norm */
Vfloat vFastNorm2D(const Vvector2D v)
{
	return((v[_X_] * v[_X_]) + (v[_Y_] * v[_Y_]));
}


Vfloat vFastNorm3D(const Vvector3D v)
{
	return((v[_X_] * v[_X_]) + (v[_Y_] * v[_Y_]) + (v[_Z_] * v[_Z_]));
}


/* Normalization */
void vNormalize2D(Vvector2D v)
{
	vScale2D(v, 1.0f / vNorm2D(v), v);
}


/* Checks if a == b */
void vNormalize3D(Vvector3D v)
{
	vScale3D(v, 1.0f / vNorm3D(v), v);
}


/* Checks if a == b */
Vint32 vIsEqual2D(const Vvector2D a, const Vvector2D b)
{
	return((a[_X_] == b[_X_]) && (a[_Y_] == b[_Y_]));
}


Vint32 vIsEqual3D(const Vvector3D a, const Vvector3D b)
{
	return((a[_X_] == b[_X_]) && (a[_Y_] == b[_Y_]) && (a[_Z_] == b[_Z_]));
}


/* Rotation about an arbitrary axis u (NORMALIZED) */
void vRotate3D(const Vvector3D p, const Vfloat angle, const Vvector3D u, Vvector3D res)
{
	Vfloat x, y, z;
	Vfloat sine, cosine;

	/* Initial setup */
	x = y = z = 0.0f;

	sine = (Vfloat)sin(angle);
	cosine = (Vfloat)cos(angle);

	/* Do rotation */
	x += (cosine + (1.0f - cosine) * u[_X_] * u[_X_]) * p[_X_];
	x += ((1.0f - cosine) * u[_X_] * u[_Y_] - u[_Z_] * sine) * p[_Y_];
	x += ((1.0f - cosine) * u[_X_] * u[_Z_] + u[_Y_] * sine) * p[_Z_];

	y += ((1.0f - cosine) * u[_X_] * u[_Y_] + u[_Z_] * sine) * p[_X_];
	y += (cosine + (1.0f - cosine) * u[_Y_] * u[_Y_]) * p[_Y_];
	y += ((1.0f - cosine) * u[_Y_] * u[_Z_] - u[_X_] * sine) * p[_Z_];

	z += ((1.0f - cosine) * u[_X_] * u[_Z_] - u[_Y_] * sine) * p[_X_];
	z += ((1.0f - cosine) * u[_Y_] * u[_Z_] + u[_X_] * sine) * p[_Y_];
	z += (cosine + (1.0f - cosine) * u[_Z_] * u[_Z_]) * p[_Z_];

	/* Return result in res */
	vSet3D(res, x, y, z);
}


/* Rotation about the X axis */
void vRotateX3D(const Vvector3D a, const Vfloat angle, Vvector3D res)
{
	Vfloat y, z;
	Vfloat sine, cosine;

	res[_X_] = a[_X_];

	y = a[_Y_];
	z = a[_Z_];

	sine = (Vfloat)sin(angle);
	cosine = (Vfloat)cos(angle);

	/* Rotate vector around X axis */
	res[_Y_] = (y * cosine) - (z * sine);
	res[_Z_] = (y * sine) + (z * cosine);
}


/* Rotation about the Y axis */
void vRotateY3D(const Vvector3D a, const Vfloat angle, Vvector3D res)
{
	Vfloat x, z;
	Vfloat sine, cosine;

	res[_Y_] = a[_Y_];

	x = a[_X_];
	z = a[_Z_];

	sine = (Vfloat)sin(angle);
	cosine = (Vfloat)cos(angle);

	/* Rotate vector around Y axis */
	res[_X_] = (x * cosine) + (z * sine);
	res[_Z_] = (-x * sine) + (z * cosine);
}


/* Rotation about the Z axis */
void vRotateZ3D(const Vvector3D a, const Vfloat angle, Vvector3D res)
{
	Vfloat x, y;
	Vfloat sine, cosine;

	res[_Z_] = a[_Z_];

	x = a[_X_];
	y = a[_Y_];
	
	sine = (Vfloat)sin(angle);
	cosine = (Vfloat)cos(angle);

	/* Rotate vector around Z axis */
	res[_X_] = (x * cosine) - (y * sine);
	res[_Y_] = (x * sine) + (y * cosine);
}


/* Calculate the equation of a 3D plane */
void vCreatePlane3D(Vvector3D normal, Vvector3D point, Vplane3D plane)
{
	plane[_A_] = normal[_X_];
	plane[_B_] = normal[_Y_];
	plane[_C_] = normal[_Z_];
	plane[_D_] = - (plane[_A_] * point[_X_]) - (plane[_B_] * point[_Y_]) - (plane[_C_] * point[_Z_]);
}


/* Calculate the equation of a 2D line */
void vCreateLine2D(Vvector2D normal, Vvector2D point, Vline2D line)
{
	line[_A_] = normal[_X_];
	line[_B_] = normal[_Y_];
	line[_C_] = - (line[_A_] * point[_X_]) - (line[_B_] * point[_Y_]);
}



/* Evaluate point in relation to plane */
Vfloat vEvalPointAndPlane3D(Vvector3D p, Vplane3D plane)
{
	return((plane[_A_] * p[_X_]) + (plane[_B_] * p[_Y_]) + (plane[_C_] * p[_Z_]) + plane[_D_]);
}


/* Evaluate point in relation to line (2D) */
Vfloat vEvalPointAndLine2D(Vvector2D p, Vline2D line)
{
	return((line[_A_] * p[_X_]) + (line[_B_] * p[_Y_]) + line[_C_]);
}


/*** Intersection routines ***/
/* Check if given point is inside a given triangle T(v0, v1, v2) */
Vint32 vPointInTriangle2D(Vvector2D point, Vvector2D v0, Vvector2D v1, Vvector2D v2)
{
	Vfloat u, v;
	Vfloat ca, ea, db;

	ca = v1[_X_] - v0[_X_];
	ea = v2[_X_] - v0[_X_];
	db = v1[_Y_] - v0[_Y_];

	v = ((ca * point[_Y_]) - (db * point[_X_]) + (db * v0[_X_]) - (ca * v0[_Y_])) / ((ca * (v2[_Y_] - v0[_Y_])) - (db * ea));
	u = (point[_X_] - (ea * v) - v0[_X_]) / ca;

	return((u >= 0.0f) && (v >= 0.0f) && ((u + v) <= 1.0f));
}


/* Check if two line segments intersect (2D) */
Vint32 vSegSegIntersect2D(Vvector2D p0, Vvector2D p1, Vvector2D q0, Vvector2D q1)
{
	Vvector2D a, b, c;
	Vfloat d, e, f;

	vSub2D(q1, q0, a);
	vSub2D(p1, p0, b);
	vSub2D(p0, q0, c);

	vPerp2D(b, b);
	f = vDot2D(a, b);
	vPerp2D(a, a);
	d = vDot2D(c, a);

	if(f > 0.0f) {
		if(d < 0.0f || d > f)
			return V_FALSE;
	}
	else {
		if(d > 0.0f || d < f)
			return V_FALSE;
	}

	e = vDot2D(c, b);

	if(f > 0.0f) {
		if(e < 0.0f || e > f)
			return V_FALSE;
	}
	else {
		if(e > 0.0f || e < f)
			return V_FALSE;
	}

	return V_TRUE;
}


/* Check if two triangles intersect (must supply normal to T2(v0, v1, v2)) (3D) */
Vint32 vTriTriIntersect3D(Vvector3D u0, Vvector3D u1, Vvector3D u2, Vvector3D v0, Vvector3D v1, Vvector3D v2, Vvector3D n2)
{
	Vplane3D planeT2;
	Vfloat d0, d1, d2;
	Vvector3D p_3D, q_3D;
	Vfloat nx, ny, nz;
	Vvector2D u0_2D, u1_2D, u2_2D, v0_2D, v1_2D, v2_2D;
	Vvector2D p_2D, q_2D;
	Vvector3D projection_plane_normal;
	Vbyte invert_winding = V_FALSE;
	Vvector2D line_normal;
	Vline2D L0, L1, L2;
	Vfloat PtoL0, PtoL1, PtoL2, QtoL0, QtoL1, QtoL2;

	/* Calculate T2's plane and trivially reject if T1 is entirely on either side of it */
	vCreatePlane3D(n2, v0, planeT2);

	d0 = vEvalPointAndPlane3D(u0, planeT2);
	d1 = vEvalPointAndPlane3D(u1, planeT2);
	d2 = vEvalPointAndPlane3D(u2, planeT2);

	if((d0 > 0.0f && d1 > 0.0f && d2 > 0.0f) || (d0 < 0.0f && d1 < 0.0f && d2 < 0.0f))
		return V_FALSE;

	/* Solution for co-planar triangles */
	if(d0 == 0.0f && d1 == 0.0f && d2 == 0.0f) {
		/* Project both triangles onto either x = 0, y = 0, z = 0 (maximizing triangles' areas) */
		nx = (Vfloat)fabs(n2[_X_]);
		ny = (Vfloat)fabs(n2[_Y_]);
		nz = (Vfloat)fabs(n2[_Z_]);

		if(nx >= ny && nx >= nz) {
			/* Project onto x = 0 */
			/* T1 */
			vSet2D(u0_2D, u0[_Y_], u0[_Z_]);
			vSet2D(u1_2D, u1[_Y_], u1[_Z_]);
			vSet2D(u2_2D, u2[_Y_], u2[_Z_]);
			/* T2 */
			vSet2D(v0_2D, v0[_Y_], v0[_Z_]);
			vSet2D(v1_2D, v1[_Y_], v1[_Z_]);
			vSet2D(v2_2D, v2[_Y_], v2[_Z_]);
		}
		else if(ny >= nx && ny >= nz) {
			/* Project onto y = 0 */
			/* T1 */
			vSet2D(u0_2D, u0[_X_], u0[_Z_]);
			vSet2D(u1_2D, u1[_X_], u1[_Z_]);
			vSet2D(u2_2D, u2[_X_], u2[_Z_]);
			/* T2 */
			vSet2D(v0_2D, v0[_X_], v0[_Z_]);
			vSet2D(v1_2D, v1[_X_], v1[_Z_]);
			vSet2D(v2_2D, v2[_X_], v2[_Z_]);
		}
		else {
			/* Project onto z = 0 */
			/* T1 */
			vSet2D(u0_2D, u0[_X_], u0[_Y_]);
			vSet2D(u1_2D, u1[_X_], u1[_Y_]);
			vSet2D(u2_2D, u2[_X_], u2[_Y_]);
			/* T2 */
			vSet2D(v0_2D, v0[_X_], v0[_Y_]);
			vSet2D(v1_2D, v1[_X_], v1[_Y_]);
			vSet2D(v2_2D, v2[_X_], v2[_Y_]);
		}
		/* Test each edge of T1 against each edge of T2 */
		/* u0 - u1 */
		if(vSegSegIntersect2D(u0_2D, u1_2D, v0_2D, v1_2D))
			return V_TRUE;

		if(vSegSegIntersect2D(u0_2D, u1_2D, v1_2D, v2_2D))
			return V_TRUE;

		if(vSegSegIntersect2D(u0_2D, u1_2D, v2_2D, v0_2D))
			return V_TRUE;

		/* u1 - u2 */
		if(vSegSegIntersect2D(u1_2D, u2_2D, v0_2D, v1_2D))
			return V_TRUE;

		if(vSegSegIntersect2D(u1_2D, u2_2D, v1_2D, v2_2D))
			return V_TRUE;

		if(vSegSegIntersect2D(u1_2D, u2_2D, v2_2D, v0_2D))
			return V_TRUE;

		/* u2 - u0 */
		if(vSegSegIntersect2D(u2_2D, u0_2D, v0_2D, v1_2D))
			return V_TRUE;

		if(vSegSegIntersect2D(u2_2D, u0_2D, v1_2D, v2_2D))
			return V_TRUE;

		if(vSegSegIntersect2D(u2_2D, u0_2D, v2_2D, v0_2D))
			return V_TRUE;

		/* Test if either triangle is contained in the other */
		if(vPointInTriangle2D(u0_2D, v0_2D, v1_2D, v2_2D) || vPointInTriangle2D(v0_2D, u0_2D, u1_2D, u2_2D))
			return V_TRUE;

		/* No intersection */
		return V_FALSE;
	} /* End co-planar triangles */

	/* Compute a line segment PQ that is the intersection of T1 with T2's plane;
	   find the two pairs of T1's vertices that reside on oppsite sides of T2's plane */
	if((d0 > 0.0f && d1 > 0.0f) || (d0 < 0.0f && d1 < 0.0f)) {
		/* Hook point is u2 */
		/* Calculate P */
		vSub3D(u0, u2, p_3D);
		vScale3D(p_3D, d2 / (d2 - d0), p_3D);
		vAdd3D(p_3D, u2, p_3D);
		/* Calculate Q */
		vSub3D(u1, u2, q_3D);
		vScale3D(q_3D, d2 / (d2 - d1), q_3D);
		vAdd3D(q_3D, u2, q_3D);
	}
	else if((d0 > 0.0f && d2 > 0.0f) || (d0 < 0.0f && d2 < 0.0f)) {
		/* Hook point is u1 */
		/* Calculate P */
		vSub3D(u0, u1, p_3D);
		vScale3D(p_3D, d1 / (d1 - d0), p_3D);
		vAdd3D(p_3D, u1, p_3D);
		/* Calculate Q */
		vSub3D(u2, u1, q_3D);
		vScale3D(q_3D, d1 / (d1 - d2), q_3D);
		vAdd3D(q_3D, u1, q_3D);
	}
	else {
		/* Hook point is u0 */
		/* Calculate P */
		vSub3D(u1, u0, p_3D);
		vScale3D(p_3D, d0 / (d0 - d1), p_3D);
		vAdd3D(p_3D, u0, p_3D);
		/* Calculate Q */
		vSub3D(u2, u0, q_3D);
		vScale3D(q_3D, d0 / (d0 - d2), q_3D);
		vAdd3D(q_3D, u0, q_3D);
	}

	/* Project PQ and T2 onto either x = 0, y = 0, z = 0 */
	nx = (Vfloat)fabs(n2[_X_]);
	ny = (Vfloat)fabs(n2[_Y_]);
	nz = (Vfloat)fabs(n2[_Z_]);

	if(nx >= ny && nx >= nz) {
		/* Project onto x = 0 */
		/* PQ */
		vSet2D(p_2D, p_3D[_Y_], p_3D[_Z_]);
		vSet2D(q_2D, q_3D[_Y_], q_3D[_Z_]);
		/* T2 */
		vSet2D(v0_2D, v0[_Y_], v0[_Z_]);
		vSet2D(v1_2D, v1[_Y_], v1[_Z_]);
		vSet2D(v2_2D, v2[_Y_], v2[_Z_]);
		/* Set vertex ordering */
		vSet3D(projection_plane_normal, -1.0f, 0.0f, 0.0f);
		if(vDot3D(projection_plane_normal, n2) <= 0.0f)
			invert_winding = V_TRUE;
	}
	else if(ny >= nx && ny >= nz) {
		/* Project onto y = 0 */
		/* PQ */
		vSet2D(p_2D, p_3D[_X_], p_3D[_Z_]);
		vSet2D(q_2D, q_3D[_X_], q_3D[_Z_]);
		/* T2 */
		vSet2D(v0_2D, v0[_X_], v0[_Z_]);
		vSet2D(v1_2D, v1[_X_], v1[_Z_]);
		vSet2D(v2_2D, v2[_X_], v2[_Z_]);
		/* Set vertex ordering */
		vSet3D(projection_plane_normal, 0.0f, 1.0f, 0.0f);
		if(vDot3D(projection_plane_normal, n2) <= 0.0f)
			invert_winding = V_TRUE;
	}
	else {
		/* Project onto z = 0 */
		/* PQ */
		vSet2D(p_2D, p_3D[_X_], p_3D[_Y_]);
		vSet2D(q_2D, q_3D[_X_], q_3D[_Y_]);
		/* T2 */
		vSet2D(v0_2D, v0[_X_], v0[_Y_]);
		vSet2D(v1_2D, v1[_X_], v1[_Y_]);
		vSet2D(v2_2D, v2[_X_], v2[_Y_]);
		/* Set vertex ordering */
		vSet3D(projection_plane_normal, 0.0f, 0.0f, -1.0f);
		if(vDot3D(projection_plane_normal, n2) <= 0.0f)
			invert_winding = V_TRUE;
	}

	/* Calculate edges' line equations and reject if PQ lies out of any on */
	/* L0 */
	if(invert_winding)
		vSub2D(v0_2D, v1_2D, line_normal);
	else
		vSub2D(v1_2D, v0_2D, line_normal);

	vPerp2D(line_normal, line_normal);
	vCreateLine2D(line_normal, v0_2D, L0);

	PtoL0 = vEvalPointAndLine2D(p_2D, L0);
	QtoL0 = vEvalPointAndLine2D(q_2D, L0);

	if(PtoL0 > 0.0f && QtoL0 > 0.0f)
		return V_FALSE;

	/* L1 */
	if(invert_winding)
		vSub2D(v1_2D, v2_2D, line_normal);
	else
		vSub2D(v2_2D, v1_2D, line_normal);
		
	vPerp2D(line_normal, line_normal);
	vCreateLine2D(line_normal, v1_2D, L1);

	PtoL1 = vEvalPointAndLine2D(p_2D, L1);
	QtoL1 = vEvalPointAndLine2D(q_2D, L1);

	if(PtoL1 > 0.0f && QtoL1 > 0.0f)
		return V_FALSE;

	/* L2 */
	if(invert_winding)
		vSub2D(v2_2D, v0_2D, line_normal);
	else
		vSub2D(v0_2D, v2_2D, line_normal);

	vPerp2D(line_normal, line_normal);
	vCreateLine2D(line_normal, v2_2D, L2);

	PtoL2 = vEvalPointAndLine2D(p_2D, L2);
	QtoL2 = vEvalPointAndLine2D(q_2D, L2);

	if(PtoL2 > 0.0f && QtoL2 > 0.0f)
		return V_FALSE;

	/* If either P or Q is inside all three edge lines, triangles intersect */
	if((PtoL0 <= 0.0f && PtoL1 <= 0.0f && PtoL2 <= 0.0f) || (QtoL0 <= 0.0f && QtoL1 <= 0.0f && QtoL2 <= 0.0f))
		return V_TRUE;

	/* Now test each edge of projected T2 with PQ */
	/* v0 - v1 */
	if(vSegSegIntersect2D(p_2D, q_2D, v0_2D, v1_2D))
		return V_TRUE;

	/* v1 - v2 */
	if(vSegSegIntersect2D(p_2D, q_2D, v1_2D, v2_2D))
		return V_TRUE;

	/* v2 - v0 */
	if(vSegSegIntersect2D(p_2D, q_2D, v2_2D, v0_2D))
		return V_TRUE;

	/* Reaching this point, triangles do not intersect */
	return V_FALSE;
}


/* Check if a plane and an AABB intersect (3D) */
Vint32 vPlaneAABBIntersect3D(Vplane3D plane, Vvector3D bmin, Vvector3D bmax)
{
	Vvector3D vmin, vmax;

	/* First find the box axis that is most aligned with the plane's normal */
	/* X */
	if(plane[_A_] >= 0.0f) {
		vmin[_X_] = bmin[_X_];
		vmax[_X_] = bmax[_X_];
	}
	else {
		vmin[_X_] = bmax[_X_];
		vmax[_X_] = bmin[_X_];
	}

	/* Y */
	if(plane[_B_] >= 0.0f) {
		vmin[_Y_] = bmin[_Y_];
		vmax[_Y_] = bmax[_Y_];
	}
	else {
		vmin[_Y_] = bmax[_Y_];
		vmax[_Y_] = bmin[_Y_];
	}

	/* Z */
	if(plane[_C_] >= 0.0f) {
		vmin[_Z_] = bmin[_Z_];
		vmax[_Z_] = bmax[_Z_];
	}
	else {
		vmin[_Z_] = bmax[_Z_];
		vmax[_Z_] = bmin[_Z_];
	}

	/* Check on which side of plane is the box */
	if(vEvalPointAndPlane3D(vmin, plane) > 0.0f)
		return V_OUTSIDE;

	if(vEvalPointAndPlane3D(vmax, plane) < 0.0f)
		return V_INSIDE;

	/* Box and plane intersect */
	return V_INTERSECT;
}


/* Check if two AABBs intersect */
Vint32 vAABBIntersect3D(Vvector3D amin, Vvector3D amax, Vvector3D bmin, Vvector3D bmax)
{
	/* X */
	if((amin[_X_] > bmax[_X_]) || (bmin[_X_] > amax[_X_]))
		return V_FALSE;

	/* Y */
	if((amin[_Y_] > bmax[_Y_]) || (bmin[_Y_] > amax[_Y_]))
		return V_FALSE;

	/* Z */
	if((amin[_Z_] > bmax[_Z_]) || (bmin[_Z_] > amax[_Z_]))
		return V_FALSE;

	/* Intersection found */
	return V_TRUE;
}


/* Check if a triangle intersects a box */
Vint32 vTriBoxIntersect3D(Vvector3D c, Vvector3D h, Vvector3D u0, Vvector3D u1, Vvector3D u2, Vvector3D n)
{
	Vvector3D v0, v1, v2;
	Vfloat p0, p1, r;
	Vplane3D tplane;
	Vvector3D amin, bmin, bmax;

	/* Translate triangle so that box is centered at origin */
	vSub3D(u0, c, v0);
	vSub3D(u1, c, v1);
	vSub3D(u2, c, v2);

	/* Separating axis theorem, test against first three axis (i, j, k) - AABB against minimal AABB containing triangle */
	/* Negation of half length vector */
	amin[_X_] = -h[_X_];
	amin[_Y_] = -h[_Y_];
	amin[_Z_] = -h[_Z_];

	/* Triangle minimal AABB */
	bmax[_X_] = vMax(vMax(v0[_X_], v1[_X_]), v2[_X_]);
	bmax[_Y_] = vMax(vMax(v0[_Y_], v1[_Y_]), v2[_Y_]);
	bmax[_Z_] = vMax(vMax(v0[_Z_], v1[_Z_]), v2[_Z_]);

	bmin[_X_] = vMin(vMin(v0[_X_], v1[_X_]), v2[_X_]);
	bmin[_Y_] = vMin(vMin(v0[_Y_], v1[_Y_]), v2[_Y_]);
	bmin[_Z_] = vMin(vMin(v0[_Z_], v1[_Z_]), v2[_Z_]);

	if(!vAABBIntersect3D(amin, h, bmin, bmax))
		return V_FALSE;

	/* Next, use triangle normal as axis */
	vCreatePlane3D(n, v0, tplane);

	if(vPlaneAABBIntersect3D(tplane, amin, h) != V_INTERSECT)
		return V_FALSE;

	/* Next nine axis - edges x [i, j, k] */
	/* i x edge0 */
	p0 = (v0[_Z_] * v1[_Y_]) - (v0[_Y_] * v1[_Z_]);
	p1 = (v2[_Y_] * (v0[_Z_] - v1[_Z_])) + (v2[_Z_] * (v1[_Y_] - v0[_Y_]));
	r = (h[_Y_] * (Vfloat)fabs(v0[_Z_] - v1[_Z_])) + (h[_Z_] * (Vfloat)fabs(v1[_Y_] - v0[_Y_]));
	if((vMin(p0, p1) > r) || (vMax(p0, p1) < -r))
		return V_FALSE;

	/* i x edge1 */
	p0 = (v0[_Y_] * (v1[_Z_] - v2[_Z_])) + (v0[_Z_] * (v2[_Y_] - v1[_Y_]));
	p1 = (v1[_Z_] * v2[_Y_]) - (v1[_Y_] * v2[_Z_]);
	r = (h[_Y_] * (Vfloat)fabs(v1[_Z_] - v2[_Z_])) + (h[_Z_] * (Vfloat)fabs(v2[_Y_] - v1[_Y_]));
	if((vMin(p0, p1) > r) || (vMax(p0, p1) < -r))
		return V_FALSE;

	/* i x edge2 */
	p0 = (v0[_Y_] * v2[_Z_]) - (v0[_Z_] * v2[_Y_]);
	p1 = (v1[_Y_] * (v2[_Z_] - v0[_Z_])) + (v1[_Z_] * (v0[_Y_] - v2[_Y_]));
	r = (h[_Y_] * (Vfloat)fabs(v2[_Z_] - v0[_Z_])) + (h[_Z_] * (Vfloat)fabs(v0[_Y_] - v2[_Y_]));
	if((vMin(p0, p1) > r) || (vMax(p0, p1) < -r))
		return V_FALSE;

	/* j x edge0 */
	p0 = (v0[_X_] * v1[_Z_]) - (v0[_Z_] * v1[_X_]);
	p1 = (v2[_X_] * (v1[_Z_] - v0[_Z_])) + (v2[_Z_] * (v0[_X_] - v1[_X_]));
	r = (h[_X_] * (Vfloat)fabs(v1[_Z_] - v0[_Z_])) + (h[_Z_] * (Vfloat)fabs(v0[_X_] - v1[_X_]));
	if((vMin(p0, p1) > r) || (vMax(p0, p1) < -r))
		return V_FALSE;

	/* j x edge1 */
	p0 = (v0[_X_] * (v2[_Z_] - v1[_Z_])) + (v0[_Z_] * (v1[_X_] - v2[_X_]));
	p1 = (v1[_X_] * v2[_Z_]) - (v1[_Z_] * v2[_X_]);
	r = (h[_X_] * (Vfloat)fabs(v2[_Z_] - v1[_Z_])) + (h[_Z_] * (Vfloat)fabs(v1[_X_] - v2[_X_]));
	if((vMin(p0, p1) > r) || (vMax(p0, p1) < -r))
		return V_FALSE;

	/* j x edge2 */
	p0 = (v0[_Z_] * v2[_X_]) - (v0[_X_] * v2[_Z_]);
	p1 = (v1[_X_] * (v0[_Z_] - v2[_Z_])) + (v1[_Z_] * (v2[_X_] - v0[_X_]));
	r = (h[_X_] * (Vfloat)fabs(v0[_Z_] - v2[_Z_])) + (h[_Z_] * (Vfloat)fabs(v2[_X_] - v0[_X_]));
	if((vMin(p0, p1) > r) || (vMax(p0, p1) < -r))
		return V_FALSE;

	/* k x edge0 */
	p0 = (v0[_Y_] * v1[_X_]) - (v0[_X_] * v1[_Y_]);
	p1 = (v2[_X_] * (v0[_Y_] - v1[_Y_])) + (v2[_Y_] * (v1[_X_] - v0[_X_]));
	r = (h[_X_] * (Vfloat)fabs(v0[_Y_] - v1[_Y_])) + (h[_Y_] * (Vfloat)fabs(v1[_X_] - v0[_X_]));
	if((vMin(p0, p1) > r) || (vMax(p0, p1) < -r))
		return V_FALSE;

	/* k X edge1 */
	p0 = (v0[_X_] * (v1[_Y_] - v2[_Y_])) + (v0[_Y_] * (v2[_X_] - v1[_X_]));
	p1 = (v1[_Y_] * v2[_X_]) - (v1[_X_] * v2[_Y_]);
	r = (h[_X_] * (Vfloat)fabs(v1[_Y_] - v2[_Y_])) + (h[_Y_] * (Vfloat)fabs(v2[_X_] - v1[_X_]));
	if((vMin(p0, p1) > r) || (vMax(p0, p1) < -r))
		return V_FALSE;

	/* k x edge2 */
	p0 = (v0[_X_] * v2[_Y_]) - (v0[_Y_] * v2[_X_]);
	p1 = (v1[_X_] * (v2[_Y_] - v0[_Y_])) + (v1[_Y_] * (v0[_X_] - v2[_X_]));
	r = (h[_X_] * (Vfloat)fabs(v2[_Y_] - v0[_Y_])) + (h[_Y_] * (Vfloat)fabs(v0[_X_] - v2[_X_]));
	if((vMin(p0, p1) > r) || (vMax(p0, p1) < -r))
		return V_FALSE;

	/* If no separating axis was found, there is intersection */
	return V_TRUE;
}


/* Check if an AABB intersects an OBB */
Vint32 vAABBIntersectOBB3D(Vvector3D amax, Vvector3D pos, Vvector3D h, Vmatrix4x4 m)
{
	Vvector3D l;
	Vvector3D Ar, Au, Ad;
	Vfloat ra, rb;

	/* First three axis (local) */
	if((Vfloat)fabs(pos[_X_]) > (amax[_X_] + h[_X_] * (Vfloat)fabs(m[0]) + h[_Y_] * (Vfloat)fabs(m[4]) + h[_Z_] * (Vfloat)fabs(m[8])))
		return V_FALSE;

	if((Vfloat)fabs(pos[_Y_]) > (amax[_Y_] + h[_X_] * (Vfloat)fabs(m[1]) + h[_Y_] * (Vfloat)fabs(m[5]) + h[_Z_] * (Vfloat)fabs(m[9])))
		return V_FALSE;

	if((Vfloat)fabs(pos[_Z_]) > (amax[_Z_] + h[_X_] * (Vfloat)fabs(m[2]) + h[_Y_] * (Vfloat)fabs(m[6]) + h[_Z_] * (Vfloat)fabs(m[10])))
		return V_FALSE;

	/* Next three axis (b) */
	if((Vfloat)fabs(pos[_X_] * m[0] + pos[_Y_] * m[1] + pos[_Z_] * m[2]) > (h[_X_] + amax[_X_] * (Vfloat)fabs(m[0]) + amax[_Y_] * (Vfloat)fabs(m[1]) + amax[_Z_] * (Vfloat)fabs(m[2])))
		return V_FALSE;

	if((Vfloat)fabs(pos[_X_] * m[4] + pos[_Y_] * m[5] + pos[_Z_] * m[6]) > (h[_Y_] + amax[_X_] * (Vfloat)fabs(m[4]) + amax[_Y_] * (Vfloat)fabs(m[5]) + amax[_Z_] * (Vfloat)fabs(m[6])))
		return V_FALSE;

	if((Vfloat)fabs(pos[_X_] * m[8] + pos[_Y_] * m[9] + pos[_Z_] * m[10]) > (h[_Z_] + amax[_X_] * (Vfloat)fabs(m[8]) + amax[_Y_] * (Vfloat)fabs(m[9]) + amax[_Z_] * (Vfloat)fabs(m[10])))
		return V_FALSE;

	/* Last nine tests (local edges x b edges) */
	vSet3D(Ar, 1.0f, 0.0f, 0.0f);
	vSet3D(Au, 0.0f, 1.0f, 0.0f);
	vSet3D(Ad, 0.0f, 0.0f, 1.0f);

	/* Combined axis 1 */
	vCross3D(Ar, &m[0], l);
	ra = amax[_X_] * (Vfloat)fabs(l[_X_]) + amax[_Y_] * (Vfloat)fabs(l[_Y_]) + amax[_Z_] * (Vfloat)fabs(l[_Z_]);
	rb = h[_X_] * (Vfloat)fabs(vDot3D(&m[0], l)) + h[_Y_] * (Vfloat)fabs(vDot3D(&m[4], l)) + h[_Z_] * (Vfloat)fabs(vDot3D(&m[8], l));
	if((Vfloat)fabs(vDot3D(pos, l)) > (ra + rb))
		return V_FALSE;

	/* Combined axis 2 */
	vCross3D(Ar, &m[4], l);
	ra = amax[_X_] * (Vfloat)fabs(l[_X_]) + amax[_Y_] * (Vfloat)fabs(l[_Y_]) + amax[_Z_] * (Vfloat)fabs(l[_Z_]);
	rb = h[_X_] * (Vfloat)fabs(vDot3D(&m[0], l)) + h[_Y_] * (Vfloat)fabs(vDot3D(&m[4], l)) + h[_Z_] * (Vfloat)fabs(vDot3D(&m[8], l));
	if((Vfloat)fabs(vDot3D(pos, l)) > (ra + rb))
		return V_FALSE;

	/* Combined axis 3 */
	vCross3D(Ar, &m[8], l);
	ra = amax[_X_] * (Vfloat)fabs(l[_X_]) + amax[_Y_] * (Vfloat)fabs(l[_Y_]) + amax[_Z_] * (Vfloat)fabs(l[_Z_]);
	rb = h[_X_] * (Vfloat)fabs(vDot3D(&m[0], l)) + h[_Y_] * (Vfloat)fabs(vDot3D(&m[4], l)) + h[_Z_] * (Vfloat)fabs(vDot3D(&m[8], l));
	if((Vfloat)fabs(vDot3D(pos, l)) > (ra + rb))
		return V_FALSE;

	/* Combined axis 4 */
	vCross3D(Au, &m[0], l);
	ra = amax[_X_] * (Vfloat)fabs(l[_X_]) + amax[_Y_] * (Vfloat)fabs(l[_Y_]) + amax[_Z_] * (Vfloat)fabs(l[_Z_]);
	rb = h[_X_] * (Vfloat)fabs(vDot3D(&m[0], l)) + h[_Y_] * (Vfloat)fabs(vDot3D(&m[4], l)) + h[_Z_] * (Vfloat)fabs(vDot3D(&m[8], l));
	if((Vfloat)fabs(vDot3D(pos, l)) > (ra + rb))
		return V_FALSE;

	/* Combined axis 5 */
	vCross3D(Au, &m[4], l);
	ra = amax[_X_] * (Vfloat)fabs(l[_X_]) + amax[_Y_] * (Vfloat)fabs(l[_Y_]) + amax[_Z_] * (Vfloat)fabs(l[_Z_]);
	rb = h[_X_] * (Vfloat)fabs(vDot3D(&m[0], l)) + h[_Y_] * (Vfloat)fabs(vDot3D(&m[4], l)) + h[_Z_] * (Vfloat)fabs(vDot3D(&m[8], l));
	if((Vfloat)fabs(vDot3D(pos, l)) > (ra + rb))
		return V_FALSE;

	/* Combined axis 6 */
	vCross3D(Au, &m[8], l);
	ra = amax[_X_] * (Vfloat)fabs(l[_X_]) + amax[_Y_] * (Vfloat)fabs(l[_Y_]) + amax[_Z_] * (Vfloat)fabs(l[_Z_]);
	rb = h[_X_] * (Vfloat)fabs(vDot3D(&m[0], l)) + h[_Y_] * (Vfloat)fabs(vDot3D(&m[4], l)) + h[_Z_] * (Vfloat)fabs(vDot3D(&m[8], l));
	if((Vfloat)fabs(vDot3D(pos, l)) > (ra + rb))
		return V_FALSE;

	/* Combined axis 7 */
	vCross3D(Ad, &m[0], l);
	ra = amax[_X_] * (Vfloat)fabs(l[_X_]) + amax[_Y_] * (Vfloat)fabs(l[_Y_]) + amax[_Z_] * (Vfloat)fabs(l[_Z_]);
	rb = h[_X_] * (Vfloat)fabs(vDot3D(&m[0], l)) + h[_Y_] * (Vfloat)fabs(vDot3D(&m[4], l)) + h[_Z_] * (Vfloat)fabs(vDot3D(&m[8], l));
	if((Vfloat)fabs(vDot3D(pos, l)) > (ra + rb))
		return V_FALSE;

	/* Combined axis 8 */
	vCross3D(Ad, &m[4], l);
	ra = amax[_X_] * (Vfloat)fabs(l[_X_]) + amax[_Y_] * (Vfloat)fabs(l[_Y_]) + amax[_Z_] * (Vfloat)fabs(l[_Z_]);
	rb = h[_X_] * (Vfloat)fabs(vDot3D(&m[0], l)) + h[_Y_] * (Vfloat)fabs(vDot3D(&m[4], l)) + h[_Z_] * (Vfloat)fabs(vDot3D(&m[8], l));
	if((Vfloat)fabs(vDot3D(pos, l)) > (ra + rb))
		return V_FALSE;

	/* Combined axis 9 */
	vCross3D(Ad, &m[8], l);
	ra = amax[_X_] * (Vfloat)fabs(l[_X_]) + amax[_Y_] * (Vfloat)fabs(l[_Y_]) + amax[_Z_] * (Vfloat)fabs(l[_Z_]);
	rb = h[_X_] * (Vfloat)fabs(vDot3D(&m[0], l)) + h[_Y_] * (Vfloat)fabs(vDot3D(&m[4], l)) + h[_Z_] * (Vfloat)fabs(vDot3D(&m[8], l));
	if((Vfloat)fabs(vDot3D(pos, l)) > (ra + rb))
		return V_FALSE;

	/* If all fails, we have a collision */
	return V_TRUE;
}


/* Check if a ray intersects an OBB */
Vint32 vRayIntersectOBB3D(Vfloat *t, Vvector3D o, Vvector3D d, Vvector3D pos, Vvector3D h, Vmatrix4x4 m)
{
	Vfloat t_min = V_FLOAT_MIN, t1;
	Vfloat t_max = V_FLOAT_MAX, t2;
	Vvector3D p;
	Vfloat e, f, inv_f;
	Vint32 i;

	vSub3D(pos, o, p);

	/* Loop for each OBB axis */
	for(i = 0; i < 3; ++i) {
		e = vDot3D(&m[i*4], p);
		f = vDot3D(&m[i*4], d);
		if(fabs(f) > 1.0e-20f) {
			inv_f = 1.0f / f;
			t1 = (e + h[i]) * inv_f;
			t2 = (e - h[i]) * inv_f;
			if(t1 < t2) {
				if(t1 > t_min)
					t_min = t1;

				if(t2 < t_max)
					t_max = t2;
			}
			else {
				if(t2 > t_min)
					t_min = t2;

				if(t1 < t_max)
					t_max = t1;
			}

			if(t_min > t_max)
				return V_FALSE;

			if(t_max < 0.0f)
				return V_FALSE;

		}
		else if(((-e -h[i]) > 0.0f) || ((-e + h[i]) < 0.0f))
			return V_FALSE;

	}

	if(t_min > 0.0f) {
		*t = t_min;

		return V_TRUE;
	}
	else {
		*t = t_max;

		return V_TRUE;
	}
}


/*** Unit conversions ***/

/* Degrees to radians */
Vfloat vDegToRad(Vfloat angle)
{
	static const Vfloat k = M_PI / 180.0f;

	return(angle * k);
}


/* Radians to degrees */
Vfloat vRadToDeg(Vfloat angle)
{
	static const Vfloat k = 180.0f / M_PI;

	return(angle * k);
}


/*** Quaternions ***/

/* Setting the identity quaternion */
void vSetIdentityQuaternion(Vquaternion q)
{
	/* Set scalar component */
	q[_W_] = 1.0f;
	/* Set vector component */
	q[_X_] = q[_Y_] = q[_Z_] = 0.0f;
}


/* Copy quaternion (a = b) */
void vCopyQuaternion(Vquaternion a, Vquaternion b)
{
	a[_X_] = b[_X_];
	a[_Y_] = b[_Y_];
	a[_Z_] = b[_Z_];
	a[_W_] = b[_W_];
}


/* Convert (axis, angle) pair to quaternion representation */
void vAxisAngleToQuaternion(Vvector3D axis, Vfloat angle, Vquaternion q)
{
	Vfloat sine;

	/* Half angle */
	angle *= 0.5f;
	/* Set scalar component */
	q[_W_] = (Vfloat)cos(angle);
	/* Set vector component */
	sine = (Vfloat)sin(angle);
	q[_X_] = axis[_X_] * sine;
	q[_Y_] = axis[_Y_] * sine;
	q[_Z_] = axis[_Z_] * sine;
}


/*
	Many thanks to
	The Matrix and Quaternions FAQ!
*/
/* Quaternion multiplication (a.b = res) */
void vMultQuaternion(Vquaternion a, Vquaternion b, Vquaternion res)
{
	Vquaternion t;
	Vfloat sa, sb, xa, xb, ya, yb, za, zb;

	sa = a[_W_];	sb = b[_W_];
	xa = a[_X_];	xb = b[_X_];
	ya = a[_Y_];	yb = b[_Y_];
	za = a[_Z_];	zb = b[_Z_];

	t[_W_] = (sa * sb) - (xa * xb) - (ya * yb) - (za * zb);
	t[_X_] = (sa * xb) + (xa * sb) + (ya * zb) - (za * yb);
	t[_Y_] = (sa * yb) + (ya * sb) + (za * xb) - (xa * zb);
	t[_Z_] = (sa * zb) + (za * sb) + (xa * yb) - (ya * xb);

	/* Copy to result (in-place operation) */
	vCopyQuaternion(res, t);
}


/* Generate a rotation matrix from a given quaternion */
void vQuaternionToMatrix(Vquaternion q, Vmatrix4x4 m)
{
	vQuaternionToSubMatrix4x3(q, m);
	vSet4D(&(m[_L_]), 0.0f, 0.0f, 0.0f, 1.0f);
}


/* Generate a quaternion from a given rotation matrix */
void vMatrixToQuaternion(Vmatrix4x4 m, Vquaternion q)
{
	Vfloat T, S;

	/* Calculate trace of matrix */
	T = 1.0f + m[0] + m[5] + m[10];

	/* Check if t is greater than zero (with tolerance) */
	if( T > 0.00000001f) {
		S = 2.0f * sqrt(T);
		q[_W_] = 0.25f * S;
		q[_X_] = (m[9] - m[6]) / S;
		q[_Y_] = (m[2] - m[8]) / S;
		q[_Z_] = (m[4] - m[1]) / S;

		return;
	}

	if(m[0] > m[5] && m[0] > m[10]) {
		S = 2.0f * sqrt(1.0f + m[0] - m[5] - m[10]);
		q[_W_] = (m[6] - m[9]) / S;
		q[_X_] = 0.25f * S;
		q[_Y_] = (m[4] + m[1]) / S;
		q[_Z_] = (m[2] + m[8]) / S;

		return;
	}

	else if(m[5] > m[10]) {
		S = 2.0f * sqrt(1.0f + m[5] - m[0] - m[10]);
		q[_W_] = (m[2] - m[8]) / S;
		q[_X_] = (m[4] + m[1]) / S;
		q[_Y_] = 0.25f * S;
		q[_Z_] = (m[9] + m[6]) / S;

		return;
	}

	else {
		S = 2.0f * sqrt(1.0f + m[10] - m[0] - m[5]);
		q[_W_] = (m[4] - m[1]) / S;
		q[_X_] = (m[2] + m[8]) / S;
		q[_Y_] = (m[9] + m[6]) / S;
		q[_Z_] = 0.25f * S;

		return;
	}
}


/*** Matrices ***/

/* Set identity matrix */
void vSetIdentityMatrix4x4(Vmatrix4x4 m)
{
	vSet4D(&m[_I_], 1.0, 0.0, 0.0, 0.0);
	vSet4D(&m[_J_], 0.0, 1.0, 0.0, 0.0);
	vSet4D(&m[_K_], 0.0, 0.0, 1.0, 0.0);
	vSet4D(&m[_L_], 0.0, 0.0, 0.0, 1.0);
}


/* Transpose matrix */
void vTransposeMatrix4x4(Vmatrix4x4 m)
{
	Vfloat temp;

	temp = m[1]; m[1] = m[4]; m[4] = temp;
	temp = m[2]; m[2] = m[8]; m[8] = temp;
	temp = m[3]; m[3] = m[12]; m[12] = temp;
	temp = m[6]; m[6] = m[9]; m[9] = temp;
	temp = m[7]; m[7] = m[13]; m[13] = temp;
	temp = m[11]; m[11] = m[14]; m[14] = temp;
}


/*** WIP! ***/
void vCopyMatrix4x4(Vmatrix4x4 a, const Vmatrix4x4 b)
{
	memcpy(a, b, sizeof(Vmatrix4x4));
}

void vMultMatrix4x4(const Vmatrix4x4 a, const Vmatrix4x4 b, Vmatrix4x4 res)
{
	Vmatrix4x4 a_transpose, r;

	vCopyMatrix4x4(a_transpose, a);
	vTransposeMatrix4x4(a_transpose);

	r[0] = vDot4D(&(a_transpose[_I_]), &(b[_I_]));
	r[1] = vDot4D(&(a_transpose[_J_]), &(b[_I_]));
	r[2] = vDot4D(&(a_transpose[_K_]), &(b[_I_]));
	r[3] = vDot4D(&(a_transpose[_L_]), &(b[_I_]));

	r[4] = vDot4D(&(a_transpose[_I_]), &(b[_J_]));
	r[5] = vDot4D(&(a_transpose[_J_]), &(b[_J_]));
	r[6] = vDot4D(&(a_transpose[_K_]), &(b[_J_]));
	r[7] = vDot4D(&(a_transpose[_L_]), &(b[_J_]));

	r[8] = vDot4D(&(a_transpose[_I_]), &(b[_K_]));
	r[9] = vDot4D(&(a_transpose[_J_]), &(b[_K_]));
	r[10] = vDot4D(&(a_transpose[_K_]), &(b[_K_]));
	r[11] = vDot4D(&(a_transpose[_L_]), &(b[_K_]));

	r[12] = vDot4D(&(a_transpose[_I_]), &(b[_L_]));
	r[13] = vDot4D(&(a_transpose[_J_]), &(b[_L_]));
	r[14] = vDot4D(&(a_transpose[_K_]), &(b[_L_]));
	r[15] = vDot4D(&(a_transpose[_L_]), &(b[_L_]));

	vCopyMatrix4x4(res, r);
}

void vMatrixTransformV4D(const Vmatrix4x4 T, const Vvector4D v, Vvector4D res)
{
	Vvector4D r;

	r[0] = T[0] * v[0];
	r[1] = T[1] * v[0];
	r[2] = T[2] * v[0];
	r[3] = T[3] * v[0];

	r[0] += T[4] * v[1];
	r[1] += T[5] * v[1];
	r[2] += T[6] * v[1];
	r[3] += T[7] * v[1];

	r[0] += T[8] * v[2];
	r[1] += T[9] * v[2];
	r[2] += T[10] * v[2];
	r[3] += T[11] * v[2];

	r[0] += T[12] * v[3];
	r[1] += T[13] * v[3];
	r[2] += T[14] * v[3];
	r[3] += T[15] * v[3];

	vCopy4D(res, r);
}

void vQuaternionToSubMatrix4x3(const Vquaternion q, Vmatrix4x4 m)
{
	Vfloat a, b, c, s;
	Vfloat a2, b2, c2, ab, sc, ac, sb, bc, sa;

	/* Set operands */
	a = q[_X_];
	b = q[_Y_];
	c = q[_Z_];
	s = q[_W_];

	a2 = a * a;
	b2 = b * b;
	c2 = c * c;
	ab = a * b;
	sc = s * c;
	ac = a * c;
	sb = s * b;
	bc = b * c;
	sa = s * a;

	/* I */
	m[0] = 1.0f - 2.0f * (b2 + c2);
	m[1] = 2.0f * (ab - sc);
	m[2] = 2.0f * (ac + sb);
	m[3] = 0.0f;
	/* J */
	m[4] = 2.0f * (ab + sc);
	m[5] = 1.0f - 2.0f * (a2 + c2);
	m[6] = 2.0f * (bc - sa);
	m[7] = 0.0f;
	/* K */
	m[8] = 2.0f * (ac - sb);
	m[9] = 2.0f * (bc + sa);
	m[10] = 1.0f - 2.0f * (a2 + b2);
	m[11] = 0.0f;
}

/*
	Clamp ops inspired by http://stackoverflow.com/a/16659263, many thanks!
*/
Vfloat32 vClamp32(const Vfloat32 s, const Vfloat32 min, const Vfloat32 max)
{
	const Vfloat32 t = (s < min) ? min : s;

	return (t > max) ? max : t;
}

Vfloat64 vClamp64(const Vfloat64 s, const Vfloat64 min, const Vfloat64 max)
{
	const Vfloat64 t = (s < min) ? min : s;

	return (t > max) ? max : t;
}

void dumpVector4D(const Vvector4D v)
{
	printf("(%4f, %4f, %4f, %4f)\n", v[_X_], v[_Y_], v[_Z_], v[_W_]);
}

void dumpMatrix4x4(const Vmatrix4x4 m)
{
	dumpVector4D(&(m[_I_]));
	dumpVector4D(&(m[_J_]));
	dumpVector4D(&(m[_K_]));
	dumpVector4D(&(m[_L_]));
}

