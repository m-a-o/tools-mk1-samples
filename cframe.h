/*	3D Coordinate frames.
	Author: Miguel A. Osorio - Feb 27 2017
*/

/*
	Copyright (c) 2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#ifndef _CFRAME_H_
#define _CFRAME_H_

#ifdef __cplusplus
extern "C" {
#endif


typedef struct cframe3D {
	Vvector3D pos;
	Vquaternion rot;
	Vvector3D scale;
} cframe3;

typedef struct cframe3D_axes {
	Vvector3D up;
	Vvector3D right;
	Vvector3D fwd;
} cframe3_axes;


void cframe3_default(cframe3* cf3);

void cframe3_to_m4(const cframe3* cf3, Vmatrix4x4 M);
void cframe3_to_m4_inv(const cframe3* cf3, Vmatrix4x4 M);

void cframe3_YPR(const Vfloat yaw, const Vfloat pitch, const Vfloat roll, cframe3* cf3);

void cframe3_get_axes(cframe3_axes* axes, const cframe3* cf3);
void cframe3_set_axes(const cframe3_axes* axes, cframe3* cf3);


#ifdef __cplusplus
}
#endif

#endif

