/*	Game loop ticker
	Author: Miguel A. Osorio - Jan 19 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


/*
	Module inspired by Glenn Fiedler's most excellent "Fix Your Timestep!" article.
	(Please refer to http://gafferongames.com/game-physics/fix-your-timestep/ for
	more info).
*/


#ifndef _TICKER_H_
#define _TICKER_H_

#ifdef __cplusplus
extern "C" {
#endif


/*
	Time in seconds.
*/

struct tick_ctl {
	float_64 elapsed;
	float_64 delta;
	float_64 curr;
	float_64 accum;
};

typedef float_64 (*tick_get_time_proc)(void* client_data);
typedef int (*tick_proc)(const struct tick_ctl, const float_64, void* client_data);

struct ticker {
	struct tick_ctl tickCtl;
	tick_get_time_proc getTime;
	tick_proc updateFrame;
	/* UpdateTick callback cannot be NULL! */
	tick_proc updateTick;
	tick_proc updateBlend;
	void* clientData;
};


struct ticker ticker_create(const float_64 delta, void* client_data, const tick_get_time_proc get_time);
int ticker_tick(struct ticker* tick);


#ifdef __cplusplus
}
#endif

#endif

