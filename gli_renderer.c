/*	GLI renderer.
	Author: Miguel A. Osorio - Mar 20 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef MK1_ENABLE_EXTRA_CHECKS
#include <assert.h>
#endif

#ifdef GLI_IOS
#include <OpenGLES/ES3/gl.h>
#include <OpenGLES/ES3/glext.h>
#else
#include <GL/gl3.h>
#include <GL/gl3ext.h>
#endif

#include "gli.h"
#include "d_array.h"
#include "custom_types.h"
#include "custom_types_align.h"
#include "hash_64.h"
#include "vdata.h"
#include "texture.h"

#ifdef MK1_ENABLE_EXTRA_CHECKS
#include "a_packer.h"
#endif

#include "d_hash_t2.h"
#include "mk1_aligned_alloc.h"
#include "decall.h"
#include "gli_renderer.h"




struct gl__vertex_array_store {
	GLuint buffer_h;
	va_desc_t va_desc;
	uint_32 n_verts;
};

static void gl__arraybuffer_set_default(void)
{
	GLES2_glBindBuffer(GL_ARRAY_BUFFER, 0);
}

gl__vertex_array_store_p gl__vertex_data_create(const uint_32 n_verts, const va_desc_t vdata)
{
	gl__vertex_array_store_p vdata_gl = (gl__vertex_array_store_p)malloc(sizeof(struct gl__vertex_array_store));

	if(vdata_gl != NULL) {
		vdata_gl->va_desc = va_desc_clone(vdata);
		vdata_gl->n_verts = n_verts;

#ifndef GLI_IOS
		{
			GLuint vao;
			GLVAO_glGenVertexArrays(1, &vao);
			GLVAO_glBindVertexArray(vao);
		}
#endif
		GLES2_glGenBuffers(1, &(vdata_gl->buffer_h));
		GLES2_glBindBuffer(GL_ARRAY_BUFFER, vdata_gl->buffer_h);
		GLES2_glBufferData(GL_ARRAY_BUFFER, va_desc_sizeof_vas(vdata_gl->va_desc), NULL, GL_STATIC_DRAW);
		gl__arraybuffer_set_default();
	}

	return vdata_gl;
}

void gl__vertex_data_delete(gl__vertex_array_store_p vdata_gl)
{
	if(vdata_gl != NULL) {
		GLES2_glDeleteBuffers(1, &(vdata_gl->buffer_h));
		va_desc_delete(vdata_gl->va_desc);
		free(vdata_gl);
	}
}

void gl__vertex_data_update(gl__vertex_array_store_p vdata_gl, const size_t offset, const void* data, const size_t size)
{
	GLES2_glBindBuffer(GL_ARRAY_BUFFER, vdata_gl->buffer_h);
	GLES2_glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
	gl__arraybuffer_set_default();
}

static int __readLocalFile(const char* filename, d_array_t buf)
{
	FILE* fp;
	char temp[2048];
	size_t rc;

	if((fp = fopen(filename, "rb")) == NULL)
		return 0;

	while((rc = fread(temp, sizeof(char), sizeof(temp), fp)) != 0)
		da_addn(temp, rc, buf);

	fclose(fp);
	return 1;
}

/*
	VERT_SHADER = GL_VERTEX_SHADER,
	FRAG_SHADER = GL_FRAGMENT_SHADER
*/
static GLenum gl__shaderTypeToGLType[] = {
	GL_VERTEX_SHADER,
	GL_FRAGMENT_SHADER
};

const char* gl__shader_type_str(const enum gl__shader_type sh_type)
{
	switch(sh_type) {
		case VERT_SHADER:
			return ".vert";
		case FRAG_SHADER:
			return ".frag";
	}
}

struct gl__shader {
	enum gl__shader_type type;
	GLuint sh_h;
};

gl__shader_p gl__shader_create(const enum gl__shader_type sh_type)
{
	gl__shader_p gl_sh = (gl__shader_p)malloc(sizeof(struct gl__shader));

	if(gl_sh != NULL) {
		gl_sh->sh_h = GLES2_glCreateShader(gl__shaderTypeToGLType[sh_type]);
		gl_sh->type = sh_type;
	}

	return gl_sh;
}

void gl__shader_delete(gl__shader_p gl_sh)
{
	if(gl_sh != NULL) {
		GLES2_glDeleteShader(gl_sh->sh_h);
		free(gl_sh);
	}
}

int gl__shader_file_src(const char* filename, gl__shader_p gl_sh)
{
	d_array_t file_buf = da_create(1024, sizeof(uint_8));
	int rc = 0;

	if(__readLocalFile(filename, file_buf)) {
		const char* src[1];

		src[0] = da_buffer(file_buf);
		GLES2_glShaderSource(gl_sh->sh_h, 1, src, NULL);
		rc = 1;
	}

	da_delete(file_buf);

	return rc;
}

int gl__shader_compile(gl__shader_p gl_sh, d_array_t log)
{
	GLint status;

	da_clear(log);
	GLES2_glCompileShader(gl_sh->sh_h);
	GLES2_glGetShaderiv(gl_sh->sh_h, GL_COMPILE_STATUS, &status);

	if(status == GL_FALSE) {
		if(log != NULL) {
			GLint log_len;

			GLES2_glGetShaderiv(gl_sh->sh_h, GL_INFO_LOG_LENGTH, &log_len);
			da_addn(NULL, log_len + 1, log);
			GLES2_glGetShaderInfoLog(gl_sh->sh_h, log_len, NULL, da_buffer(log));
			da_add_formatted(log, 0, "Error compiling %s shader:\n\n", gl__shader_type_str(gl_sh->type));
			da_cat_formatted(log, "\n");
		}

		return 0;
	}

	return 1;
}

size_t gl__shader_h(const gl__shader_p gl_sh)
{
	return (gl_sh != NULL) ? gl_sh->sh_h : 0;
}


/* Hard limits! */
#define GL__PROG_MAX_PARAMS 32
struct gl__prog_params {
	GLint n_params;
	va_desc_t params[GL__PROG_MAX_PARAMS];
	GLint locs[GL__PROG_MAX_PARAMS];
};

struct gl__prog {
	GLuint prg_h;
	GLint n_params;
	struct gl__prog_params attribs;
	struct gl__prog_params uniforms;
	vat_namespace _namespace;
};

static void gl__prog_delete_params(struct gl__prog_params* params)
{
	size_t i;

	for(i = 0; i < params->n_params; ++i)
		va_desc_delete(params->params[i]);

	params->n_params = 0;
}

static void gl__prog_delete_all_params(gl__prog_p gl_prg)
{
	gl__prog_delete_params(&(gl_prg->attribs));
	gl__prog_delete_params(&(gl_prg->uniforms));

	gl_prg->n_params = 0;
}

static int gl__locate_prog_param(const struct vector_array_decl* vafmt_p, const struct gl__prog_params* params)
{
	GLint i;

	for(i = 0; i < params->n_params; ++i)
		if((vafmt_p->name.id) == (((struct vector_array_decl*)va_desc_vas(params->params[i]))[0].name.id))
			if((vafmt_p->type.type) == (((struct vector_array_decl*)va_desc_vas(params->params[i]))[0].type.type))
				return i;

	return -1;
}

gl__prog_p gl__prog_create(gl__shader_p* sh_components, size_t n_components, vat_namespace _namespace, d_array_t log)
{
	gl__prog_p gl_prg = (gl__prog_p)malloc(sizeof(struct gl__prog));

	da_clear(log);

	if(gl_prg != NULL) {
		size_t i;
		GLint status;

		gl_prg->prg_h = GLES2_glCreateProgram();

		for(i = 0; i < n_components; ++i)
			GLES2_glAttachShader(gl_prg->prg_h, sh_components[i]->sh_h);

		GLES2_glLinkProgram(gl_prg->prg_h);
		GLES2_glGetProgramiv(gl_prg->prg_h, GL_LINK_STATUS, &status);
		if(status == GL_FALSE) {
			if(log != NULL) {
				GLint log_len;

				GLES2_glGetProgramiv(gl_prg->prg_h, GL_INFO_LOG_LENGTH, &log_len);
				da_addn(NULL, log_len + 1, log);
				GLES2_glGetProgramInfoLog(gl_prg->prg_h, log_len, NULL, da_buffer(log));
				da_add_formatted(log, 0, "Error linking program:\n\n");
				da_cat_formatted(log, "\n");
			}

			for(i = 0; i < n_components; ++i)
				GLES2_glDetachShader(gl_prg->prg_h, sh_components[i]->sh_h);

			GLES2_glDeleteProgram(gl_prg->prg_h);
			free(gl_prg);
			gl_prg = NULL;
		}

		else {
			gl_prg->n_params = 0;
			gl_prg->attribs.n_params = 0;
			gl_prg->uniforms.n_params = 0;
			gl_prg->_namespace = _namespace;
		}
	}

	gl__prog_refresh_params(gl_prg);

	return gl_prg;
}

static enum vector_array_t gl__vattrTypeToVAType(const GLenum type)
{
	/* TODO: add more supported types! */

	switch(type) {
		case GL_FLOAT:
			return VEC_ARRAY_FLOAT;
		case GL_FLOAT_VEC2:
			return VEC_ARRAY_FLOAT2;
		case GL_FLOAT_VEC3:
			return VEC_ARRAY_FLOAT3;
		case GL_FLOAT_VEC4:
			return VEC_ARRAY_FLOAT4;
		case GL_FLOAT_MAT3:
			return VEC_ARRAY_MFLOAT3;
		case GL_FLOAT_MAT4:
			return VEC_ARRAY_MFLOAT4;
		case GL_UNSIGNED_INT:
		case GL_SAMPLER_2D:
		case GL_SAMPLER_3D:
		case GL_SAMPLER_CUBE:
		case GL_SAMPLER_2D_ARRAY:
			return VEC_ARRAY_UINT;
		default:
			return NUM_VEC_ARRAY_TYPES;
	}
}

typedef void (*gl__getActiveParamProc)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
typedef GLint (*gl__getParamLocationProc)(GLuint program, const char* name);

struct gl__refresh_params_control {
	GLenum active_vars;
	GLenum max_active_var_len;
	gl__getActiveParamProc get_active_var;
	gl__getParamLocationProc get_var_loc;
};

static const struct gl__refresh_params_control gl__gen_refresh_params_control_attribs(void)
{
	struct gl__refresh_params_control pctrl;

	pctrl.active_vars = GL_ACTIVE_ATTRIBUTES;
	pctrl.max_active_var_len = GL_ACTIVE_ATTRIBUTE_MAX_LENGTH;
	pctrl.get_active_var = GLES2_glGetActiveAttrib;
	pctrl.get_var_loc = GLES2_glGetAttribLocation;

	return pctrl;
}

static const struct gl__refresh_params_control gl__gen_refresh_params_control_uniforms(void)
{
	struct gl__refresh_params_control pctrl;

	pctrl.active_vars = GL_ACTIVE_UNIFORMS;
	pctrl.max_active_var_len = GL_ACTIVE_UNIFORM_MAX_LENGTH;
	pctrl.get_active_var = GLES2_glGetActiveUniform;
	pctrl.get_var_loc = GLES2_glGetUniformLocation;

	return pctrl;
}

#define VATTR_NAME_MAX_LEN 128
static uint_32 gl__prog_refresh_params_ex(gl__prog_p gl_prg, const struct gl__refresh_params_control params_ctrl, struct gl__prog_params* params)
{
	GLint act, i, max_len;
	char attrib_name[VATTR_NAME_MAX_LEN];

	GLES2_glGetProgramiv(gl_prg->prg_h, params_ctrl.active_vars, &act);
	GLES2_glGetProgramiv(gl_prg->prg_h, params_ctrl.max_active_var_len, &max_len);
	max_len = (max_len < sizeof(attrib_name)) ? max_len : sizeof(attrib_name);
	/*
		TODO:

		- Only the first GL__PROG_MAX_PARAMS active parameters are bound!
		(Remember, hard limits!)
	*/
	act = ((act < (GL__PROG_MAX_PARAMS - gl_prg->n_params)) ? act : (GL__PROG_MAX_PARAMS - gl_prg->n_params));
	for(i = 0; i < act; ++i) {
		GLsizei name_len;
		GLint size;
		GLenum type;
		struct vector_array_decl param_decl;

		params_ctrl.get_active_var(gl_prg->prg_h, i, max_len, &name_len, &size, &type, attrib_name);

		param_decl.type.type = gl__vattrTypeToVAType(type);
		param_decl.type.size = size;
		param_decl.name.id = vat_register_name_to_id(attrib_name, gl_prg->_namespace);
		params->params[i] = va_desc_create(&param_decl, 1, va_decl_pack_default());
		params->locs[i] = params_ctrl.get_var_loc(gl_prg->prg_h, attrib_name);
	}

	params->n_params += act;

	return act;
}

void gl__prog_refresh_params(gl__prog_p gl_prg)
{
	if(gl_prg != NULL) {
		gl__prog_delete_all_params(gl_prg);
		gl_prg->n_params += gl__prog_refresh_params_ex(gl_prg, gl__gen_refresh_params_control_attribs(), &(gl_prg->attribs));
		gl_prg->n_params += gl__prog_refresh_params_ex(gl_prg, gl__gen_refresh_params_control_uniforms(), &(gl_prg->uniforms));
	}
}

size_t gl__prog_handle(const gl__prog_p gl_prg)
{
	return (gl_prg != NULL) ? gl_prg->prg_h : 0;
}

/*
va_block_t* gl__prog_params(const gl__prog_p gl_prg, const enum gl__prog_param_type param_type)
{
	return (param_type == PARAM_VERTEX_ATTRIB) ? gl_prg->attribs.params : gl_prg->uniforms.params;
}
*/




/* Experimental / placeholders */
struct gl__vattrPointerParams {
	GLenum type;
	GLint size;
	GLsizei stride;
	int num_components;
	int component_off;
};

static struct gl__vattrPointerParams gl__vaTypeToVattrPointerParams[] = {
	{ GL_FLOAT, 1, 0, 1, 0 },
	{ GL_BYTE, 1, 0, 1, 0 },
	{ GL_UNSIGNED_BYTE, 1, 0, 1, 0 },
	{ GL_SHORT, 1, 0, 1, 0 },
	{ GL_UNSIGNED_SHORT, 1, 0, 1, 0 },
	{ GL_FIXED, 1, 0, 1, 0 },

	{ GL_FLOAT, 2, 0, 1, 0 },
	{ GL_FLOAT, 3, 0, 1, 0 },
	{ GL_FLOAT, 4, 0, 1, 0 },

	{ GL_FLOAT, 3, sizeof(GLfloat) * 9, 3, sizeof(GLfloat) * 3 },
	{ GL_FLOAT, 4, sizeof(GLfloat) * 16, 4, sizeof(GLfloat) * 4 },

	{ GL_UNSIGNED_INT, 1, 0, 1, 0 }
};

/* Draw */
/*
	In this example, vertex data is king, and attemps to bind its attribute arrays if the
	requested shader has them.
*/
#define GL__MAX_VERTEX_DATA_ATTRIBS 32
static void gl__vertex_data_as_current(const gl__vertex_array_store_p vdata_gl, const gl__prog_p gl_prg)
{
	if(vdata_gl != NULL && gl_prg != NULL) {
		va_desc_t vfmt = vdata_gl->va_desc;
		size_t i;

		GLES2_glBindBuffer(GL_ARRAY_BUFFER, vdata_gl->buffer_h);

		/* TODO: this will explode if vdata_gl has more than GL__MAX_VERTEX_DATA_ATTRIBS attributes! */
		for(i = 0; i < va_desc_vas_size(vfmt); ++i) {
			/* Dynamic attribute mapping */
			struct vector_array_decl* curr_vafmt = &(((struct vector_array_decl*)va_desc_vas(vfmt))[i]);
			GLint attr_index = gl__locate_prog_param(curr_vafmt, &(gl_prg->attribs));

			if(attr_index >= 0) {
				const struct gl__vattrPointerParams params = gl__vaTypeToVattrPointerParams[curr_vafmt->type.type];
				const GLuint attr_loc = gl_prg->attribs.locs[attr_index];
				int component;

				for(component = 0; component < params.num_components; ++component) {
					GLES2_glEnableVertexAttribArray(attr_loc + component);
					GLES2_glVertexAttribPointer(attr_loc + component, params.size, params.type, GL_FALSE, params.stride, \
												(void*)(size_t)(va_desc_array_offset(i, vfmt) + (params.component_off * component)));
				}
			}
		}
	}
}

static void gl__vertex_data_draw_raw(const gl__vertex_array_store_p vdata_gl)
{
	if(vdata_gl != NULL)
		GLES2_glDrawArrays(GL_TRIANGLES, 0, vdata_gl->n_verts);
}

static void gl__vertex_data_remove_current(const gl__vertex_array_store_p vdata_gl, const gl__prog_p gl_prg)
{
	if(vdata_gl != NULL) {
		GLuint attrib_index;
		va_desc_t vfmt = vdata_gl->va_desc;

		for(attrib_index = 0; attrib_index < va_desc_vas_size(vfmt); ++attrib_index) {
			/* Dynamic attribute mapping */
			GLint attr_index = gl__locate_prog_param(&(((struct vector_array_decl*)va_desc_vas(vfmt))[attrib_index]), &(gl_prg->attribs));

			if(attr_index >= 0)
				GLES2_glDisableVertexAttribArray(gl_prg->attribs.locs[attr_index]);
		}

		gl__arraybuffer_set_default();
	}
}

void gl__vertex_data_render_single_raw(const gl__vertex_array_store_p vdata_gl, const gl__prog_p gl_prg)
{
	gl__vertex_data_as_current(vdata_gl, gl_prg);
	gl__vertex_data_draw_raw(vdata_gl);
	gl__vertex_data_remove_current(vdata_gl, gl_prg);
}

/* Uniforms */
typedef void (*gl__setUniformVectorDataIntProc)(GLint loc, GLsizei count, const GLint* value);

static void gl__uniformTypeToSetUniformIntDataProc(const uint_32 loc, const struct vector_array_decl va_decl, const void* data)
{
	gl__setUniformVectorDataIntProc procs[NUM_VEC_ARRAY_TYPES];

	procs[VEC_ARRAY_FLOAT] = NULL;
	procs[VEC_ARRAY_BYTE] = NULL;
	procs[VEC_ARRAY_UBYTE] = NULL;
	procs[VEC_ARRAY_SHORT] = NULL;
	procs[VEC_ARRAY_USHORT] = NULL;
	procs[VEC_ARRAY_FIXED] = NULL;
	procs[VEC_ARRAY_FLOAT2] = NULL;
	procs[VEC_ARRAY_FLOAT3] = NULL;
	procs[VEC_ARRAY_FLOAT4] = NULL;
	procs[VEC_ARRAY_MFLOAT3] = NULL;
	procs[VEC_ARRAY_MFLOAT4] = NULL;
	procs[VEC_ARRAY_UINT] = GLES2_glUniform1iv;

	if(procs[va_decl.type.type] != NULL)
		procs[va_decl.type.type](loc, va_decl.type.size, data);
}

typedef void (*gl__uniformTypeToSetUniformProc)(const uint_32 loc, const struct vector_array_decl va_decl, const void* data);
static const gl__uniformTypeToSetUniformProc gl__uniformTypeToSetUniform_LUT[] = {
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	gl__uniformTypeToSetUniformIntDataProc
};

/* Textures */
struct gl__texture {
	union {
		struct tex2d_params _2d;
		struct texCube_params cube;
	} src;
	GLuint tex_h;
	GLenum target;
	GLenum fmt;
	GLenum mag_filter;
	GLenum min_filter;
	GLenum wrap_s;
	GLenum wrap_t;
};

static GLenum gl__texFmtToGL[] = {
	GL_RGB,
	GL_RGBA
};

static struct gl__texture gl__texture_defaults(void)
{
	struct gl__texture tex;

	tex.tex_h = 0;
	tex.target = GL_TEXTURE_2D;
	tex.fmt = GL_RGB;
	tex.mag_filter = GL_NEAREST;
	tex.min_filter = GL_NEAREST;
	tex.wrap_s = GL_REPEAT;
	tex.wrap_t = GL_REPEAT;

	return tex;
}

gl__texture_p gl__texture_create_2d(const struct tex2d_params src)
{
	gl__texture_p tex = (gl__texture_p)malloc(sizeof(struct gl__texture));

	if(tex != NULL) {
		*tex = gl__texture_defaults();

		tex->src._2d = src;
		tex->target = GL_TEXTURE_2D;
		tex->fmt = gl__texFmtToGL[src.imgfmt];
		GLES2_glGenTextures(1, &(tex->tex_h));

		GLES2_glBindTexture(tex->target, tex->tex_h);

		GLES2_glTexParameteri(tex->target, GL_TEXTURE_MAG_FILTER, tex->mag_filter);
		GLES2_glTexParameteri(tex->target, GL_TEXTURE_MIN_FILTER, tex->min_filter);
		GLES2_glTexParameteri(tex->target, GL_TEXTURE_WRAP_S, tex->wrap_s);
		GLES2_glTexParameteri(tex->target, GL_TEXTURE_WRAP_T, tex->wrap_t);

		GLES2_glTexImage2D(tex->target, 0, tex->fmt, src.width_px, src.height_px, 0, tex->fmt, GL_UNSIGNED_BYTE, NULL);

		GLES2_glBindTexture(tex->target, 0);

		return tex;
	}

	return NULL;
}

gl__texture_p gl__texture_create_cube(const struct texCube_params src)
{
	return NULL;
}

void gl__texture_update_2d(gl__texture_p tex, const uint_32 x, const uint_32 y, const uint_32 w, const uint_32 h, const void* data)
{
	if(tex != NULL) {
		GLES2_glBindTexture(tex->target, tex->tex_h);
		GLES2_glTexSubImage2D(tex->target, 0, x, y, (w == 0) ? tex->src._2d.width_px : w, (h == 0) ? tex->src._2d.height_px : h, tex->fmt, GL_UNSIGNED_BYTE, data);
		GLES2_glBindTexture(tex->target, 0);
	}
}

void gl__texture_delete(gl__texture_p tex)
{
	if(tex != NULL) {
		GLES2_glDeleteTextures(1, &(tex->tex_h));
		free(tex);
	}
}

typedef void (*gl__active_tex_proc)(const size_t i, const size_t base, const struct active_set_item tex, void* data);
static void gl__active_textures(const active_set_t tex_set, const size_t base, gl__active_tex_proc active_tex, void* data)
{
	const size_t num_active = active_set_num_active(tex_set);
	size_t i;

	for(i = 0; i < num_active; ++i)
		active_tex(i, base, active_set_geti(i, tex_set), data);
}

static void gl__active_tex_bind(const size_t i, const size_t base, const struct active_set_item tex, void* data)
{
	const gl__texture_p gltex = (gl__texture_p)(tex.data);

	GLES2_glActiveTexture(base + i);
	GLES2_glBindTexture(gltex->target, gltex->tex_h);
}

void gl__bind_textures(const active_set_t tex_set, const size_t base)
{
	if(tex_set != NULL)
		gl__active_textures(tex_set, GL_TEXTURE0 + base, gl__active_tex_bind, NULL);
}

static struct vector_array_decl gl__gen_tex_uniform(const char* name, vat_namespace _namespace)
{
	struct vector_array_decl utex;

	utex.type.type = VEC_ARRAY_UINT;
	utex.type.size = 1;
	utex.name.id = vat_register_name_to_id(name, _namespace);

	return utex;
}

static void gl__active_tex_uniforms(const size_t i, const size_t base, const struct active_set_item tex, void* data)
{
	gl__prog_p gl_prg = (gl__prog_p)data;

	/* Dynamic attribute mapping */
	const struct vector_array_decl ufmt = gl__gen_tex_uniform(tex.name_str, gl_prg->_namespace);
	GLint uniform_index = gl__locate_prog_param(&ufmt, &(gl_prg->uniforms));

	if(uniform_index >= 0) {
		const uint_32 tex_img_unit = base + i;

		gl__uniformTypeToSetUniform_LUT[ufmt.type.type](gl_prg->uniforms.locs[uniform_index], ufmt, &tex_img_unit);
	}
}

void gl__bind_tex_uniforms(const active_set_t tex_set, const size_t base, gl__prog_p gl_prg)
{
	if((tex_set != NULL) && (gl_prg != NULL))
		gl__active_textures(tex_set, base, gl__active_tex_uniforms, gl_prg);
}

/* State functions */
enum gl__state_func_ops {
	OP_DEPTH_RANGE = 0,
	OP_VIEWPORT,
	OP_LINE_WIDTH,
	OP_FRONT_FACE,
	OP_CULL_FACE,
	OP_ENABLE,
	OP_DISABLE,
	OP_POLYGON_OFFSET,
	OP_SCISSOR,
	OP_SAMPLE_COVERAGE,
	OP_STENCIL_FUNC,
	OP_STENCIL_FUNC_SEPARATE,
	OP_STENCIL_OP,
	OP_STENCIL_OP_SEPARATE,
	OP_DEPTH_FUNC,
	OP_BLEND_EQUATION,
	OP_BLEND_EQUATION_SEPARATE,
	OP_BLEND_FUNC_SEPARATE,
	OP_BLEND_FUNC,
	OP_BLEND_COLOR,
	OP_COLOR_MASK,
	OP_DEPTH_MASK,
	OP_STENCIL_MASK,
	OP_STENCIL_MASK_SEPARATE,
	OP_CLEAR,
	OP_CLEAR_COLOR,
	OP_CLEAR_DEPTH,
	OP_CLEAR_STENCIL,
	OP_UNIFORM1FV,
	OP_UNIFORM2FV,
	OP_UNIFORM3FV,
	OP_UNIFORM4FV,
	OP_UNIFORM1IV,
	OP_UNIFORMMATRIX3FV,
	OP_UNIFORMMATRIX4FV,

	NUM_STATE_FUNC_OPS
};

DC_ARGS_DEF_START(OP_DEPTH_RANGE)
	GLclampf n, f;
DC_ARGS_DEF_END(OP_DEPTH_RANGE);

DC_ARGS_DEF_START(OP_VIEWPORT)
	GLint x, y;
	GLsizei w, h;
DC_ARGS_DEF_END(OP_VIEWPORT);

DC_ARGS_DEF_START(OP_LINE_WIDTH)
	GLfloat width;
DC_ARGS_DEF_END(OP_LINE_WIDTH);

DC_ARGS_DEF_START(OP_FRONT_FACE)
	GLenum dir;
DC_ARGS_DEF_END(OP_FRONT_FACE);

DC_ARGS_DEF_START(OP_CULL_FACE)
	GLenum mode;
DC_ARGS_DEF_END(OP_CULL_FACE);

DC_ARGS_DEF_START(OP_ENABLE)
	GLenum cap;
DC_ARGS_DEF_END(OP_ENABLE);

DC_ARGS_DEF_START(OP_DISABLE)
	GLenum cap;
DC_ARGS_DEF_END(OP_DISABLE);

DC_ARGS_DEF_START(OP_POLYGON_OFFSET)
	GLfloat factor, units;
DC_ARGS_DEF_END(OP_POLYGON_OFFSET);

DC_ARGS_DEF_START(OP_SCISSOR)
	GLint left, bottom;
	GLsizei width, height;
DC_ARGS_DEF_END(OP_SCISSOR);

DC_ARGS_DEF_START(OP_SAMPLE_COVERAGE)
	GLclampf value;
	GLboolean invert;
DC_ARGS_DEF_END(OP_SAMPLE_COVERAGE);

DC_ARGS_DEF_START(OP_STENCIL_FUNC)
	GLenum func;
	GLint ref;
	GLuint mask;
DC_ARGS_DEF_END(OP_STENCIL_FUNC);

DC_ARGS_DEF_START(OP_STENCIL_FUNC_SEPARATE)
	GLenum face, func;
	GLint ref;
	GLuint mask;
DC_ARGS_DEF_END(OP_STENCIL_FUNC_SEPARATE);

DC_ARGS_DEF_START(OP_STENCIL_OP)
	GLenum sfail, dpfail, dppass;
DC_ARGS_DEF_END(OP_STENCIL_OP);

DC_ARGS_DEF_START(OP_STENCIL_OP_SEPARATE)
	GLenum face, sfail, dpfail, dppass;
DC_ARGS_DEF_END(OP_STENCIL_OP_SEPARATE);

DC_ARGS_DEF_START(OP_DEPTH_FUNC)
	GLenum func;
DC_ARGS_DEF_END(OP_DEPTH_FUNC);

DC_ARGS_DEF_START(OP_BLEND_EQUATION)
	GLenum mode;
DC_ARGS_DEF_END(OP_BLEND_EQUATION);

DC_ARGS_DEF_START(OP_BLEND_EQUATION_SEPARATE)
	GLenum modeRGB, modeAlpha;
DC_ARGS_DEF_END(OP_BLEND_EQUATION_SEPARATE);

DC_ARGS_DEF_START(OP_BLEND_FUNC_SEPARATE)
	GLenum srcRGB, dstRGB, srcAlpha, dstAlpha;
DC_ARGS_DEF_END(OP_BLEND_FUNC_SEPARATE);

DC_ARGS_DEF_START(OP_BLEND_FUNC)
	GLenum src, dst;
DC_ARGS_DEF_END(OP_BLEND_FUNC);

DC_ARGS_DEF_START(OP_BLEND_COLOR)
	GLclampf red, green, blue, alpha;
DC_ARGS_DEF_END(OP_BLEND_COLOR);

DC_ARGS_DEF_START(OP_COLOR_MASK)
	GLboolean r, g, b, a;
DC_ARGS_DEF_END(OP_COLOR_MASK);

DC_ARGS_DEF_START(OP_DEPTH_MASK)
	GLboolean mask;
DC_ARGS_DEF_END(OP_DEPTH_MASK);

DC_ARGS_DEF_START(OP_STENCIL_MASK)
	GLuint mask;
DC_ARGS_DEF_END(OP_STENCIL_MASK);

DC_ARGS_DEF_START(OP_STENCIL_MASK_SEPARATE)
	GLenum face;
	GLuint mask;
DC_ARGS_DEF_END(OP_STENCIL_MASK_SEPARATE);

DC_ARGS_DEF_START(OP_CLEAR)
	GLbitfield buf;
DC_ARGS_DEF_END(OP_CLEAR);

DC_ARGS_DEF_START(OP_CLEAR_COLOR)
	GLclampf r, g, b, a;
DC_ARGS_DEF_END(OP_CLEAR_COLOR);

DC_ARGS_DEF_START(OP_CLEAR_DEPTH)
	GLclampf d;
DC_ARGS_DEF_END(OP_CLEAR_DEPTH);

DC_ARGS_DEF_START(OP_CLEAR_STENCIL)
	GLint s;
DC_ARGS_DEF_END(OP_CLEAR_STENCIL);

static const struct size_align_pair gl__func_args_sz_al_LUT[] = {
	DC_ARGS_SZ_AL_DEF(OP_DEPTH_RANGE),
	DC_ARGS_SZ_AL_DEF(OP_VIEWPORT),
	DC_ARGS_SZ_AL_DEF(OP_LINE_WIDTH),
	DC_ARGS_SZ_AL_DEF(OP_FRONT_FACE),
	DC_ARGS_SZ_AL_DEF(OP_CULL_FACE),
	DC_ARGS_SZ_AL_DEF(OP_ENABLE),
	DC_ARGS_SZ_AL_DEF(OP_DISABLE),
	DC_ARGS_SZ_AL_DEF(OP_POLYGON_OFFSET),
	DC_ARGS_SZ_AL_DEF(OP_SCISSOR),
	DC_ARGS_SZ_AL_DEF(OP_SAMPLE_COVERAGE),
	DC_ARGS_SZ_AL_DEF(OP_STENCIL_FUNC),
	DC_ARGS_SZ_AL_DEF(OP_STENCIL_FUNC_SEPARATE),
	DC_ARGS_SZ_AL_DEF(OP_STENCIL_OP),
	DC_ARGS_SZ_AL_DEF(OP_STENCIL_OP_SEPARATE),
	DC_ARGS_SZ_AL_DEF(OP_DEPTH_FUNC),
	DC_ARGS_SZ_AL_DEF(OP_BLEND_EQUATION),
	DC_ARGS_SZ_AL_DEF(OP_BLEND_EQUATION_SEPARATE),
	DC_ARGS_SZ_AL_DEF(OP_BLEND_FUNC_SEPARATE),
	DC_ARGS_SZ_AL_DEF(OP_BLEND_FUNC),
	DC_ARGS_SZ_AL_DEF(OP_BLEND_COLOR),
	DC_ARGS_SZ_AL_DEF(OP_COLOR_MASK),
	DC_ARGS_SZ_AL_DEF(OP_DEPTH_MASK),
	DC_ARGS_SZ_AL_DEF(OP_STENCIL_MASK),
	DC_ARGS_SZ_AL_DEF(OP_STENCIL_MASK_SEPARATE),
	DC_ARGS_SZ_AL_DEF(OP_CLEAR),
	DC_ARGS_SZ_AL_DEF(OP_CLEAR_COLOR),
	DC_ARGS_SZ_AL_DEF(OP_CLEAR_DEPTH),
	DC_ARGS_SZ_AL_DEF(OP_CLEAR_STENCIL),
	/* For uniforms, use helper functions instead! */
	{ 0, 0 },
	{ 0, 0 },
	{ 0, 0 },
	{ 0, 0 },
	{ 0, 0 },
	{ 0, 0 },
	{ 0, 0 },
	/* For uniforms, use helper functions instead! */

	{ 0, 0 }
};

/* Uniform arrays */
DC_ARGS_DEF_START(UNIFORMV)
	GLint loc;
	GLsizei count;
DC_ARGS_DEF_END(UNIFORMV);

DC_ARGS_DEF_START(UNIFORMFLOATV)
	DC_ARGS_DEF(UNIFORMV) header;
	GLfloat data[1];
DC_ARGS_DEF_END(UNIFORMFLOATV);

DC_ARGS_DEF_START(UNIFORMINTV)
	DC_ARGS_DEF(UNIFORMV) header;
	GLint data[1];
DC_ARGS_DEF_END(UNIFORMINTV);

/* Entry points */
DC_EP_DEF(OP_DEPTH_RANGE)
{
	DC_ARGS(args, OP_DEPTH_RANGE);

	GLES2_glDepthRangef(args->n, args->f);
}

DC_EP_DEF(OP_VIEWPORT)
{
	DC_ARGS(args, OP_VIEWPORT);

	GLES2_glViewport(args->x, args->y, args->w, args->h);
}

DC_EP_DEF(OP_LINE_WIDTH)
{
	DC_ARGS(args, OP_LINE_WIDTH);

	GLES2_glLineWidth(args->width);
}

DC_EP_DEF(OP_FRONT_FACE)
{
	DC_ARGS(args, OP_FRONT_FACE);

	GLES2_glFrontFace(args->dir);
}

DC_EP_DEF(OP_CULL_FACE)
{
	DC_ARGS(args, OP_CULL_FACE);

	GLES2_glCullFace(args->mode);
}

DC_EP_DEF(OP_ENABLE)
{
	DC_ARGS(args, OP_ENABLE);

	GLES2_glEnable(args->cap);
}

DC_EP_DEF(OP_DISABLE)
{
	DC_ARGS(args, OP_DISABLE);

	GLES2_glDisable(args->cap);
}

DC_EP_DEF(OP_POLYGON_OFFSET)
{
	DC_ARGS(args, OP_POLYGON_OFFSET);

	GLES2_glPolygonOffset(args->factor, args->units);
}

DC_EP_DEF(OP_SCISSOR)
{
	DC_ARGS(args, OP_SCISSOR);

	GLES2_glScissor(args->left, args->bottom, args->width, args->height);
}

DC_EP_DEF(OP_SAMPLE_COVERAGE)
{
	DC_ARGS(args, OP_SAMPLE_COVERAGE);

	GLES2_glSampleCoverage(args->value, args->invert);
}

DC_EP_DEF(OP_STENCIL_FUNC)
{
	DC_ARGS(args, OP_STENCIL_FUNC);

	GLES2_glStencilFunc(args->func, args->ref, args->mask);
}

DC_EP_DEF(OP_STENCIL_FUNC_SEPARATE)
{
	DC_ARGS(args, OP_STENCIL_FUNC_SEPARATE);

	GLES2_glStencilFuncSeparate(args->face, args->func, args->ref, args->mask);
}

DC_EP_DEF(OP_STENCIL_OP)
{
	DC_ARGS(args, OP_STENCIL_OP);

	GLES2_glStencilOp(args->sfail, args->dpfail, args->dppass); 
}

DC_EP_DEF(OP_STENCIL_OP_SEPARATE)
{
	DC_ARGS(args, OP_STENCIL_OP_SEPARATE);

	GLES2_glStencilOpSeparate(args->face, args->sfail, args->dpfail, args->dppass);
}

DC_EP_DEF(OP_DEPTH_FUNC)
{
	DC_ARGS(args, OP_DEPTH_FUNC);

	GLES2_glDepthFunc(args->func);
}

DC_EP_DEF(OP_BLEND_EQUATION)
{
	DC_ARGS(args, OP_BLEND_EQUATION);

	GLES2_glBlendEquation(args->mode);
}

DC_EP_DEF(OP_BLEND_EQUATION_SEPARATE)
{
	DC_ARGS(args, OP_BLEND_EQUATION_SEPARATE);

	GLES2_glBlendEquationSeparate(args->modeRGB, args->modeAlpha);
}

DC_EP_DEF(OP_BLEND_FUNC_SEPARATE)
{
	DC_ARGS(args, OP_BLEND_FUNC_SEPARATE);

	GLES2_glBlendFuncSeparate(args->srcRGB, args->dstRGB, args->srcAlpha, args->dstAlpha);
}

DC_EP_DEF(OP_BLEND_FUNC)
{
	DC_ARGS(args, OP_BLEND_FUNC);

	GLES2_glBlendFunc(args->src, args->dst);
}

DC_EP_DEF(OP_BLEND_COLOR)
{
	DC_ARGS(args, OP_BLEND_COLOR);

	GLES2_glBlendColor(args->red, args->green, args->blue, args->alpha);
}

DC_EP_DEF(OP_COLOR_MASK)
{
	DC_ARGS(args, OP_COLOR_MASK);

	GLES2_glColorMask(args->r, args->g, args->b, args->a);
}

DC_EP_DEF(OP_DEPTH_MASK)
{
	DC_ARGS(args, OP_DEPTH_MASK);

	GLES2_glDepthMask(args->mask);
}

DC_EP_DEF(OP_STENCIL_MASK)
{
	DC_ARGS(args, OP_STENCIL_MASK);

	GLES2_glStencilMask(args->mask);
}

DC_EP_DEF(OP_STENCIL_MASK_SEPARATE)
{
	DC_ARGS(args, OP_STENCIL_MASK_SEPARATE);

	GLES2_glStencilMaskSeparate(args->face, args->mask);
}

DC_EP_DEF(OP_CLEAR)
{
	DC_ARGS(args, OP_CLEAR);

	GLES2_glClear(args->buf);
}

DC_EP_DEF(OP_CLEAR_COLOR)
{
	DC_ARGS(args, OP_CLEAR_COLOR);

	GLES2_glClearColor(args->r, args->g, args->b, args->a);
}

DC_EP_DEF(OP_CLEAR_DEPTH)
{
	DC_ARGS(args, OP_CLEAR_DEPTH);

	GLES2_glClearDepthf(args->d);
}

DC_EP_DEF(OP_CLEAR_STENCIL)
{
	DC_ARGS(args, OP_CLEAR_STENCIL);

	GLES2_glClearStencil(args->s);
}

#define DC_SPEC_UVEC_CALL(func) func(args->header.loc, args->header.count, args->data)
#define DC_SPEC_UMAT_CALL(func) func(args->header.loc, args->header.count, 0, args->data)
#ifndef MK1_ENABLE_EXTRA_CHECKS
#define DC_EXEC_SET_UNIFORMV(func, op) do {\
												DC_ARGS(args, op);\
												func;\
											} while(0)
#else
#define DC_EXEC_SET_UNIFORMV(func, op) do {\
												DC_ARGS(args, op);\
												func;\
											} while(0)
#endif

DC_EP_DEF(OP_UNIFORM1FV)
{
	DC_EXEC_SET_UNIFORMV(DC_SPEC_UVEC_CALL(GLES2_glUniform1fv), UNIFORMFLOATV);
}

DC_EP_DEF(OP_UNIFORM2FV)
{
	DC_EXEC_SET_UNIFORMV(DC_SPEC_UVEC_CALL(GLES2_glUniform2fv), UNIFORMFLOATV);
}

DC_EP_DEF(OP_UNIFORM3FV)
{
	DC_EXEC_SET_UNIFORMV(DC_SPEC_UVEC_CALL(GLES2_glUniform3fv), UNIFORMFLOATV);
}

DC_EP_DEF(OP_UNIFORM4FV)
{
	DC_EXEC_SET_UNIFORMV(DC_SPEC_UVEC_CALL(GLES2_glUniform4fv), UNIFORMFLOATV);
}

DC_EP_DEF(OP_UNIFORM1IV)
{
	DC_EXEC_SET_UNIFORMV(DC_SPEC_UVEC_CALL(GLES2_glUniform1iv), UNIFORMINTV);
}

DC_EP_DEF(OP_UNIFORMMATRIX3FV)
{
	DC_EXEC_SET_UNIFORMV(DC_SPEC_UMAT_CALL(GLES2_glUniformMatrix3fv), UNIFORMFLOATV);
}

DC_EP_DEF(OP_UNIFORMMATRIX4FV)
{
	DC_EXEC_SET_UNIFORMV(DC_SPEC_UMAT_CALL(GLES2_glUniformMatrix4fv), UNIFORMFLOATV);
}

dc_compiler_t rsc_compiler_create(void)
{
	return dc_compiler_create();
}

int rsc_stream_exec(vec_array_t rs)
{
	return dc_stream_exec(rs);
}

#define GL__EP_ARGS_INSTANCE(args, op, dcc) DC_ARGS_INSTANCE(args, op, gl__func_args_sz_al_LUT[op], dcc)
void rsc_enable_blend(dc_compiler_t dcc)
{
	GL__EP_ARGS_INSTANCE(args, OP_ENABLE, dcc);

	args->cap = GL_BLEND;
}

void rsc_alpha_blend(dc_compiler_t dcc)
{
	GL__EP_ARGS_INSTANCE(args, OP_BLEND_FUNC, dcc);

	args->src = GL_SRC_ALPHA;
	args->dst = GL_ONE_MINUS_SRC_ALPHA;
}

void rsc_blend_additive(dc_compiler_t dcc)
{
	GL__EP_ARGS_INSTANCE(args, OP_BLEND_FUNC, dcc);

	args->src = GL_ONE;
	args->dst = GL_ONE;
}

void rsc_viewport(const int x, const int y, const int w, const int h, dc_compiler_t dcc)
{
	GL__EP_ARGS_INSTANCE(args, OP_VIEWPORT, dcc);

	args->x = x;
	args->y = y;
	args->w = w;
	args->h = h;
}

/* Uniforms */
/* Uniform spec helpers */
static struct size_align_pair gl__uniformv_sz_al(const struct size_align_pair array_sz_al)
{
	const static struct size_align_pair header_sz_al = DC_ARGS_SZ_AL_DEF(UNIFORMV);
	struct size_align_pair uniformv_vas[2];

	uniformv_vas[0] = header_sz_al;
	uniformv_vas[1] = array_sz_al;

	return sizealignof_vas(uniformv_vas, 2, va_decl_pack_size_align_pair());
}

static const dc_exec_proc rsc_uniformTypeToOp_LUT[] = {
	DC_EP_NAME(OP_UNIFORM1FV),
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DC_EP_NAME(OP_UNIFORM2FV),
	DC_EP_NAME(OP_UNIFORM3FV),
	DC_EP_NAME(OP_UNIFORM4FV),
	DC_EP_NAME(OP_UNIFORMMATRIX3FV),
	DC_EP_NAME(OP_UNIFORMMATRIX4FV),
	DC_EP_NAME(OP_UNIFORM1IV),
};

/*
	In this example, uniform data is king, and attemps to bind its parameters if the
	requested shader has them.
*/
static void rsc_addSetUniformDataProc(const uint_32 loc, const struct vector_array_decl va_decl, const void* data, dc_compiler_t dcc)
{
	const dc_exec_proc uniform_cmd = rsc_uniformTypeToOp_LUT[va_decl.type.type];

	if(uniform_cmd != NULL) {
		const struct size_align_pair data_sz_al = va_decl_pack_default().decl_to_sz_al(&va_decl);
		DC_ARGS_DEF(UNIFORMV)* args;
		void* cmd_data;

		if(va_decl.type.type == VEC_ARRAY_UINT) {
			args = (DC_ARGS_DEF(UNIFORMV)*)dcc_add_cmd(uniform_cmd, gl__uniformv_sz_al(data_sz_al), dcc);
			cmd_data = ((DC_ARGS_DEF(UNIFORMINTV)*)args)->data;
		}
		else {
			args = (DC_ARGS_DEF(UNIFORMV)*)dcc_add_cmd(uniform_cmd, gl__uniformv_sz_al(data_sz_al), dcc);
			cmd_data = ((DC_ARGS_DEF(UNIFORMFLOATV)*)args)->data;
		}

		args->loc = loc;
		args->count = va_decl.type.size;
		memcpy(cmd_data, data, data_sz_al.size);
	}
}

void rsc_vector_data_uniforms(const vec_array_t uniform_data, const gl__prog_p gl_prg, dc_compiler_t dcc)
{
	if(uniform_data != NULL && gl_prg != NULL) {
		size_t i;

		for(i = 0; i < va_desc_vas_size((va_desc_t)uniform_data); ++i) {
			/* Dynamic attribute mapping */
			struct vector_array_decl* curr_ufmt = &(((struct vector_array_decl*)va_desc_vas((va_desc_t)uniform_data))[i]);
			GLint uniform_index = gl__locate_prog_param(curr_ufmt, &(gl_prg->uniforms));

			if(uniform_index >= 0)
				rsc_addSetUniformDataProc(gl_prg->uniforms.locs[uniform_index], *curr_ufmt, vec_array_get(i, uniform_data), dcc);
		}
	}
}


/* Silence unused function warnings! */
static const dc_exec_proc gl__func_cmds_LUT[] = {
	DC_EP_NAME(OP_DEPTH_RANGE),
	DC_EP_NAME(OP_VIEWPORT),
	DC_EP_NAME(OP_LINE_WIDTH),
	DC_EP_NAME(OP_FRONT_FACE),
	DC_EP_NAME(OP_CULL_FACE),
	DC_EP_NAME(OP_ENABLE),
	DC_EP_NAME(OP_DISABLE),
	DC_EP_NAME(OP_POLYGON_OFFSET),
	DC_EP_NAME(OP_SCISSOR),
	DC_EP_NAME(OP_SAMPLE_COVERAGE),
	DC_EP_NAME(OP_STENCIL_FUNC),
	DC_EP_NAME(OP_STENCIL_FUNC_SEPARATE),
	DC_EP_NAME(OP_STENCIL_OP),
	DC_EP_NAME(OP_STENCIL_OP_SEPARATE),
	DC_EP_NAME(OP_DEPTH_FUNC),
	DC_EP_NAME(OP_BLEND_EQUATION),
	DC_EP_NAME(OP_BLEND_EQUATION_SEPARATE),
	DC_EP_NAME(OP_BLEND_FUNC_SEPARATE),
	DC_EP_NAME(OP_BLEND_FUNC),
	DC_EP_NAME(OP_BLEND_COLOR),
	DC_EP_NAME(OP_COLOR_MASK),
	DC_EP_NAME(OP_DEPTH_MASK),
	DC_EP_NAME(OP_STENCIL_MASK),
	DC_EP_NAME(OP_STENCIL_MASK_SEPARATE),
	DC_EP_NAME(OP_CLEAR),
	DC_EP_NAME(OP_CLEAR_COLOR),
	DC_EP_NAME(OP_CLEAR_DEPTH),
	DC_EP_NAME(OP_CLEAR_STENCIL),
	DC_EP_NAME(OP_UNIFORM1FV),
	DC_EP_NAME(OP_UNIFORM2FV),
	DC_EP_NAME(OP_UNIFORM3FV),
	DC_EP_NAME(OP_UNIFORM4FV),
	DC_EP_NAME(OP_UNIFORM1IV),
	DC_EP_NAME(OP_UNIFORMMATRIX3FV),
	DC_EP_NAME(OP_UNIFORMMATRIX4FV),

	NULL
};

dc_exec_proc silence_warnings_dummy(const int i)
{
	return gl__func_cmds_LUT[i];
}
/* Silence unused function warnings! */

