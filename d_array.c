/* Dynamic arrays */
/* Author: Miguel A. Osorio - Oct 19 2001 */

/*
	Copyright (c) 2001-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
/*** Experimental ***/
#include <stddef.h>
#include <stdarg.h>
/*** Experimental ***/

#include "d_array.h"




#define DA_DEFAULT_FACTOR 2


/* The d_array_t data type */
struct d_array_struct {

	mem_int_t _currentSize;
	mem_int_t _nItems;
	mem_int_t _itemSize;
	mem_int_t _factor;
	int _flags;

	char* _buffer;

};


/* Creating an array */
d_array_t da_create_ex(mem_int_t n_items, mem_int_t item_size, mem_int_t factor, int flags)
{
	d_array_t new_array;

	/* Create new array */
	new_array = (d_array_t)calloc(1, sizeof(struct d_array_struct));
	if(new_array == NULL)
		return(new_array);

	/* Buffer storage */
	new_array->_buffer = (char*)calloc(n_items, item_size);
	if(new_array->_buffer == NULL) {
		free(new_array);
		return(NULL);
	}

	/* Set up attributes */
	new_array->_currentSize = n_items;
	new_array->_nItems = 0;
	new_array->_itemSize = item_size;
	new_array->_factor = factor;
	new_array->_flags = flags;

	return(new_array);
}

d_array_t da_create(mem_int_t n_items, mem_int_t item_size)
{
	return(da_create_ex(n_items, item_size, DA_DEFAULT_FACTOR, DA_NIL));
}

/* Deleting an array */
void da_delete(d_array_t array)
{
	if(array == NULL)
		return;

	/* Free buffer */
	if(array->_buffer != NULL)
		free(array->_buffer);
	
	/* Free array */
	free(array);

	return;
}

/* Adding stuff */
static int auxAddItemsAtIndex(const void* items, mem_int_t n, mem_int_t index, d_array_t array, int overwrite)
{
	char* newbuffer;
	mem_int_t new_nItems;
	mem_int_t delta;

	/* Perfectly valid */
	if(n == 0)
		return(DA_OK);

	/* Sanity check on index */
	if((index > array->_nItems) || (index < 0) || (n < 0))
		return(DA_OUT_OF_BOUNDS);

	/* Expected size */
	if(!overwrite)
		delta = n;

	else {
		delta = (index + n) - array->_nItems;
		if(delta < 0)
			delta = 0;
	}

	/* How many in effect */
	new_nItems = array->_nItems + delta;

	/* Check array capacity */
	if(new_nItems > array->_currentSize) {
		/* How larger does the new buffer have to be? */
		mem_int_t power = array->_factor; /* Don't start at 1 here because it's already known the buffers' current size cannot hold the new additions */
		while((array->_currentSize * power) < new_nItems)
			power *= array->_factor;

		/* Create new, larger buffer */
		newbuffer = (char*)realloc(array->_buffer, array->_currentSize * power * array->_itemSize);
		if(newbuffer == NULL)
			return(DA_RESIZE_ERROR);

		/* Assign new buffer to array */
		array->_buffer = newbuffer;
		/* Update current size */
		array->_currentSize *= power;
	}

	/* Should we move some memory around first? */
	if((index != array->_nItems) && (!overwrite))
		memmove(array->_buffer + ((index + n) * array->_itemSize), array->_buffer + (index * array->_itemSize), (array->_nItems - index) * array->_itemSize);

	/* Add items to buffer */
	if(items != NULL)
		memcpy(array->_buffer + (index * array->_itemSize), items, n * array->_itemSize);

	/* Update stats */
	array->_nItems = new_nItems;

	return(DA_OK);
}

int da_add(const void* item, d_array_t array)
{
	return(da_addn(item, 1, array));
}

int da_addn(const void* items, mem_int_t n, d_array_t array)
{
	if(array == NULL)
		return(DA_ERROR);

	return(auxAddItemsAtIndex(items, n, array->_nItems, array, 0));
}

int da_addi(const void* item, mem_int_t index, d_array_t array)
{
	return(da_addni(item, 1, index, array));
}

int da_addni(const void* items, mem_int_t n, mem_int_t index, d_array_t array)
{
	if(array == NULL)
		return(DA_ERROR);

	return(auxAddItemsAtIndex(items, n, index, array, 0));
}

/* Item access */
static int auxGrabItemsAtIndex(mem_int_t index, mem_int_t* n, void* items, d_array_t array)
{
	/* Perfectly valid operation */
	if((*n) == 0)
		return(DA_OK);

	/* Check initial conditions */
	if(array == NULL)
		return(DA_ERROR);

	/* Bounds checking */
	if((index >= array->_nItems) || (index < 0) || (*n < 0))
		return(DA_OUT_OF_BOUNDS);

	/* Attempts to grab data past end-of-buffer will be truncated! */
	if((index + (*n)) > array->_nItems)
		*n = array->_nItems - index;

	/* Copy data */
	if(items != NULL)
		memcpy(items, array->_buffer + (index * array->_itemSize), array->_itemSize * (*n));

	/* Success */
	return(DA_OK);
}

int da_getn(mem_int_t index, mem_int_t n, void* items, d_array_t array)
{
	return(auxGrabItemsAtIndex(index, &n, items, array));
}

int da_get(mem_int_t index, void* item, d_array_t array)
{
	return(da_getn(index, 1, item, array));
}

/* Removing stuff */
int da_removen(mem_int_t index, mem_int_t n, void* items, d_array_t array)
{
	int rc;

	/* Grab items if necessary, then go ahead and remove them */
	if((rc = auxGrabItemsAtIndex(index, &n, items, array)) != DA_OK)
		return(rc);

	/* Re-arrange buffer */
	if(index != (array->_nItems - n))
		memmove(array->_buffer + (index * array->_itemSize), array->_buffer + ((index + n) * array->_itemSize), (array->_nItems - n - index) * array->_itemSize);

	/* Update number of items */
	array->_nItems -= n;

	/* Success */
	return(DA_OK);
}

int da_remove(mem_int_t index, void* item, d_array_t array)
{
	return(da_removen(index, 1, item, array));
}

/* Overwriting stuff */
int da_replace(const void* item, mem_int_t index, d_array_t array)
{
	return(da_replacen(item, 1, index, array));
}


int da_replacen(const void* items, mem_int_t n, mem_int_t index, d_array_t array)
{
	if(array == NULL)
		return(DA_ERROR);

	return(auxAddItemsAtIndex(items, n, index, array, 1));
}

/* Get number of items stored in array */
mem_int_t da_num_items(d_array_t array)
{
	if(array == NULL)
		return(0);

	return(array->_nItems);
}

mem_int_t da_item_size(d_array_t array)
{
	if(array == NULL)
		return(0);

	return(array->_itemSize);
}

/* Exporting the buffer */
void* da_buffer(d_array_t array)
{
	if(array == NULL)
		return(NULL);

	return(array->_buffer);
}

const char* da_buffer_str(d_array_t array)
{
	char null_terminator = '\0';

	if(array == NULL)
		return(NULL);

	/* Sanity check */
	if(array->_itemSize != sizeof(char))
		return(NULL);

	/* Add terminator */
	if(da_add(&null_terminator, array) != DA_OK)
		return(NULL);

	/* Cheat */
	--(array->_nItems);

	return(array->_buffer);
}

/* Clearing an array */
void da_clear(d_array_t array)
{
	if(array == NULL)
		return;

	/* Reset number of items */
	array->_nItems = 0;

	return;	
}

void da_reset(d_array_t array)
{
	da_clear(array);

	return;
}

void da_truncate(mem_int_t n, d_array_t array)
{
	if(array == NULL)
		return;

	/* Truncate to number of items */
	if(n >= 0 && n <= array->_nItems)
		array->_nItems = n;

	return;
}

/* Get current size */
mem_int_t da_curr_size(d_array_t array)
{
	if(array == NULL)
		return(0);

	return(array->_currentSize);
}

/*** Experimental ***/
static int auxPutFormatted(d_array_t array, mem_int_t index, const char* fmt, va_list v_args1, va_list v_args2)
{
	mem_int_t w_size, prev_n_items;
	int rc;

	/* Sanity checks */
	if(array == NULL || da_item_size(array) != sizeof(char))
		return(DA_ERROR);

	if(index < 0 || index > array->_nItems)
		return(DA_ERROR);

	/* Find out how much space we need */
	w_size = vsnprintf(NULL, 0, fmt, v_args1);

	/* Make room (include null terminator!) */
	prev_n_items = array->_nItems;
	if((rc = da_addni(NULL, w_size + 1, index, array)) != DA_OK)
		return(rc);

	/* Put formatted string */
	if(vsnprintf(array->_buffer + index, array->_currentSize - prev_n_items, fmt, v_args2) != w_size)
		return(DA_ERROR);

	/* Remove null terminator */
	da_remove(index + w_size, NULL, array);

	return(DA_OK);
}

int da_add_formatted(d_array_t array, mem_int_t index, const char* fmt, ...)
{
	va_list v_args1, v_args2;
	int rc;

	va_start(v_args1, fmt);
	va_start(v_args2, fmt);
	rc = auxPutFormatted(array, index, fmt, v_args1, v_args2);
	va_end(v_args1);
	va_end(v_args2);

	return(rc);
}

int da_cat_formatted(d_array_t array, const char* fmt, ...)
{
	va_list v_args1, v_args2;
	int rc;

	va_start(v_args1, fmt);
	va_start(v_args2, fmt);
	rc = auxPutFormatted(array, da_num_items(array), fmt, v_args1, v_args2);
	va_end(v_args1);
	va_end(v_args2);

	return(rc);
}

/*
	Pop remove - swap removed item(s) with array's tail, then truncate.
	This *does* obviously mess up item ordering. It is also very fast.
*/
int da_pop_removen(mem_int_t index, mem_int_t n, void* items, d_array_t array)
{
	mem_int_t index_from_tail;
	int rc;

	if(array == NULL)
		return(DA_ERROR);

	if((index + n) > array->_nItems)
		return(da_removen(index, n, items, array));

	if((rc = da_getn(index, n, items, array)) != DA_OK)
		return(rc);

	index_from_tail = array->_nItems - n;
	if(index_from_tail < (index + n))
		index_from_tail = index + n;

	memcpy(array->_buffer + (index * array->_itemSize), array->_buffer + (index_from_tail * array->_itemSize), (array->_nItems - index_from_tail) * array->_itemSize);

	array->_nItems -= n;

	return(DA_OK);
}

int da_pop_remove(mem_int_t index, void* item, d_array_t array)
{
	return(da_pop_removen(index, 1, item, array));
}



#if 0
int daRemoveItem(int index, void* item, DArray array)
{
	char* newbuffer;
	int downsize;

#ifdef DA_CHECK_NULL_POINTERS
	if(array == NULL)
		return(DA_ERROR);
#endif

	downsize = array->CurrentSize / (array->Factor * array->Factor);
	
	/* Check initial conditions */
	if(array->nItems == 0 || index >= array->nItems || index < 0)
		return(DA_ERROR);

	/* Save removed item */
	if(item)
		memcpy(item, array->Buffer + (index * array->ItemSize), array->ItemSize);

	/* Re-arrange buffer */
	memmove(array->Buffer + (index * array->ItemSize), array->Buffer + ((index + 1) * array->ItemSize), (array->nItems - 1 - index) * array->ItemSize);

	/* Update number of items */
	--(array->nItems);

	/* Check if array has auto-downsize enabled and if downsizing is required */
	if(((array->Flags & DA_AUTO_DOWNSIZE) == DA_AUTO_DOWNSIZE) &&
	   (array->nItems <= downsize) && (array->CurrentSize / array->Factor >= array->InitialSize)) {

		/* Allocate new, smaller buffer */
		newbuffer = (char*)realloc(array->Buffer, (array->CurrentSize / array->Factor) * array->ItemSize);

		/* Check for allocation errors */
		if(newbuffer == NULL)
			return(DA_ERROR);

		/* Assign new buffer to array */
		array->Buffer = newbuffer;

		/* Update current size */
		array->CurrentSize /= array->Factor;

	}

	/* Success */
	return(DA_OK);
}
#endif


#if 0
int daRunTests(void)
{
	DArray main_string = daCreate(1, sizeof(char), 2, DA_NO_OPTION);
	char sentence[] = "apples";
	char append[] = "oranges";
	char* match;
	DArray new_string = daCreate(1, sizeof(char), 3, DA_NO_OPTION);
	DArray s1, s2;

	daAddItems(sentence, strlen(sentence), main_string);
	daAddItems(":", 1, main_string);
	daAddItems(append, strlen(append), main_string);

	printf("main string is:\n%s\nsize is: %d - total storage is: %d\n", daExportBufferAsString(main_string), daGetNumItems(main_string), daGetCurrentSize(main_string));

	s1 = daCreate(1, sizeof(char), 2, DA_NO_OPTION);
	s2 = daCreate(1, sizeof(char), 2, DA_NO_OPTION);
//	auxSplit(daExportBufferAsString(main_string), ":", s1, s2);
	auxSplitByLen(daExportBuffer(main_string), strlen(sentence), s1, s2);

	printf("s1 is:\n\"%s\"\nsize is: %d - total storage is: %d\n", daExportBufferAsString(s1), daGetNumItems(s1), daGetCurrentSize(s1));
	printf("s2 is:\n\"%s\"\nsize is: %d - total storage is: %d\n", daExportBufferAsString(s2), daGetNumItems(s2), daGetCurrentSize(s2));

	daDelete(s1);
	daDelete(s2);

	daDelete(main_string);
	daDelete(new_string);

	return 0;
}
#endif

