/*	Vector data descriptors.
	Author: Miguel A. Osorio - Mar 20 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#ifndef _VDATA_H_
#define _VDATA_H_

#ifdef __cplusplus
extern "C" {
#endif


enum vector_array_t {
	VEC_ARRAY_FLOAT = 0,
	VEC_ARRAY_BYTE,
	VEC_ARRAY_UBYTE,
	VEC_ARRAY_SHORT,
	VEC_ARRAY_USHORT,
	VEC_ARRAY_FIXED,

	VEC_ARRAY_FLOAT2,
	VEC_ARRAY_FLOAT3,
	VEC_ARRAY_FLOAT4,

	VEC_ARRAY_MFLOAT3,
	VEC_ARRAY_MFLOAT4,

	VEC_ARRAY_UINT,

	NUM_VEC_ARRAY_TYPES
};

typedef unsigned int va_size_t;
#define VA_INVALID_INDEX (UINT_MAX)

struct vector_array_type {
	va_size_t type;
	va_size_t size;
};

struct vector_array_name {
	unsigned int id;
};

/* Vector array declaration */
struct vector_array_decl {
	struct vector_array_type type;
	struct vector_array_name name;
};

struct size_align_pair {
	va_size_t size;
	va_size_t align;
};

typedef struct size_align_pair (*va_decl_to_sz_al_mapper)(const void* va_decl);

/* Custom va_decl packing */
struct va_decl_pack {
	struct size_align_pair decl_sz_al;
	va_decl_to_sz_al_mapper decl_to_sz_al;
};

struct va_decl_pack va_decl_pack_default(void);
struct va_decl_pack va_decl_pack_size_align_pair(void);

/* vas - vector arrays specification */
struct size_align_pair sizealignof_vas(const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack);
va_size_t offsetof_vas(const va_size_t i, const void* vas, const struct va_decl_pack decl_pack);
void offsetsof_vas(va_size_t* offsets, const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack);

/* vector arrays descriptor - vas + stored/pre-computed:

		vas_size,
		offsetsof(vas),
		sizeof(vas),
		alignof(vas)
*/
typedef struct vector_array_descriptor* va_desc_t;

va_desc_t va_desc_create(const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack);
void va_desc_delete(va_desc_t va_desc);

struct size_align_pair sizealignof_va_desc(const va_desc_t va_desc);

void* va_desc_vas(const va_desc_t va_desc);
va_size_t va_desc_vas_size(const va_desc_t va_desc);
va_size_t va_desc_array_offset(const va_size_t i, const va_desc_t va_desc);
va_size_t va_desc_sizeof_vas(const va_desc_t va_desc);
va_size_t va_desc_alignof_vas(const va_desc_t va_desc);

va_desc_t va_desc_clone(const va_desc_t src);

/*
	vector arrays
*/
typedef struct vector_array_instance* vec_array_t;

struct size_align_pair sizealignof_vec_array_ex(const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack);

vec_array_t vec_array_in_place(void* buffer, const void* vas, const va_size_t vas_size, const struct size_align_pair sz_al, const struct va_decl_pack decl_pack);
vec_array_t vec_array_create(const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack);
void vec_array_delete(vec_array_t vec_array);

struct size_align_pair sizealignof_vec_array(const vec_array_t vec_array);

void* vec_array_store(const vec_array_t vec_array);
void* vec_array_get(const va_size_t i, const vec_array_t vec_array);

/* vector array compiler */
typedef struct vec_array_compiler* va_compiler_t;

va_compiler_t va_compiler_create(const struct va_decl_pack decl_pack);
void va_compiler_delete(va_compiler_t vac);

va_size_t va_compiler_add(const void* vas, const va_size_t vas_size, va_compiler_t vac);
va_desc_t va_desc_compile(va_compiler_t vac);
vec_array_t va_compile(va_compiler_t vac);
void va_compiler_reset(va_compiler_t vac);

/* Vector array type namespaces */
typedef unsigned int va_name_t;
typedef struct va_type_namespace* vat_namespace;

vat_namespace vat_names_create(void);
void vat_names_delete(vat_namespace vat_names);

/* 0 is NULL id! */
va_name_t vat_name_to_id(const char* name, const vat_namespace vat_names);
va_name_t vat_register_name_to_id(const char* name, vat_namespace vat_names);

int vat_names_map_to_vas(struct vector_array_decl* vas, const va_size_t vas_size, const char* names[], vat_namespace vat_names);

#ifdef __cplusplus
}
#endif

#endif

