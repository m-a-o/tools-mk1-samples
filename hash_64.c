/*	64 bit hashes.
	Author: Miguel A. Osorio - Mar 21 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#include <stddef.h>

#include "lookup3.h"
#include "hash_64.h"




struct hash_64 hash_buffer(const void* buf, const size_t buf_size)
{
	struct hash_64 hash_from_buf = { 0, 0 };

	hashlittle2(buf, buf_size, &(hash_from_buf.low), &(hash_from_buf.high));

	return hash_from_buf;
}

