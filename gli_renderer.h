/*	GLI renderer.
	Author: Miguel A. Osorio - Mar 20 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#ifndef _GLI_RENDERER_H_
#define _GLI_RENDERER_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Vertex arrays store (buffer objects for vertex attributes) */
typedef struct gl__vertex_array_store* gl__vertex_array_store_p;

gl__vertex_array_store_p gl__vertex_data_create(const uint_32 n_verts, const va_desc_t vdata);
void gl__vertex_data_update(gl__vertex_array_store_p vdata_gl, const size_t offset, const void* data, const size_t size);
void gl__vertex_data_delete(gl__vertex_array_store_p vdata_gl);

/* Shaders & Programs */
/* Shaders */
enum gl__shader_type {
	VERT_SHADER = 0,
	FRAG_SHADER
};

const char* gl__shader_type_str(const enum gl__shader_type sh_type);

typedef struct gl__shader* gl__shader_p;

gl__shader_p gl__shader_create(const enum gl__shader_type sh_type);
void gl__shader_delete(gl__shader_p gl_sh);
int gl__shader_file_src(const char* filename, gl__shader_p gl_sh);
int gl__shader_compile(gl__shader_p gl_sh, d_array_t log);
size_t gl__shader_h(const gl__shader_p gl_sh);

/* Programs */
typedef struct gl__prog* gl__prog_p;

gl__prog_p gl__prog_create(gl__shader_p* sh_components, size_t n_components, vat_namespace _namespace, d_array_t log);
void gl__prog_refresh_params(gl__prog_p gl_prg);
size_t gl__prog_handle(const gl__prog_p gl_prg);

enum gl__prog_param_type {
	PARAM_VERTEX_ATTRIB = 0,
	PARAM_UNIFORM
};

/*
va_block_t* gl__prog_params(const gl__prog_p gl_prg, const enum gl__prog_param_type param_type);
*/

/* Experimental / placeholders */
/* Draw */
void gl__vertex_data_render_single_raw(const gl__vertex_array_store_p vdata_gl, const gl__prog_p gl_prg);


/* Textures */
typedef struct gl__texture* gl__texture_p;

gl__texture_p gl__texture_create_2d(const struct tex2d_params src);
gl__texture_p gl__texture_create_cube(const struct texCube_params src);
void gl__texture_update_2d(gl__texture_p tex, const uint_32 x, const uint_32 y, const uint_32 w, const uint_32 h, const void* data);
void gl__texture_delete(gl__texture_p tex);

void gl__bind_textures(const active_set_t tex_set, const size_t base);
void gl__bind_tex_uniforms(const active_set_t tex_set, const size_t base, gl__prog_p gl_prg);


/* Render streams */
dc_compiler_t rsc_compiler_create(void);
int rsc_stream_exec(vec_array_t rs);

void rsc_enable_blend(dc_compiler_t dcc);
void rsc_alpha_blend(dc_compiler_t dcc);
void rsc_blend_additive(dc_compiler_t dcc);
void rsc_viewport(const int x, const int y, const int w, const int h, dc_compiler_t dcc);
void rsc_vector_data_uniforms(const vec_array_t uniform_data, const gl__prog_p gl_prg, dc_compiler_t dcc);

#ifdef __cplusplus
}
#endif

#endif

