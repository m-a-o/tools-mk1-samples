/*	Aligned memory allocation.
	Author: Miguel A. Osorio - Nov 24 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "a_packer.h"
#include "d_array.h"
#include "custom_types.h"
#include "d_hash_t2.h"
#include "mk1_aligned_alloc.h"




void* mk1_aligned_alloc(const size_t size, const unsigned char alignment, void* (*alloc_f)(size_t size))
{
	if(alignment > 0) {
		void* buf = alloc_f(size + alignment);

		if(buf != NULL)
		{
			const size_t align_offset = offsetToAligned(alignment, (const unsigned char*)buf + 1) + 1;

			buf = (void*)alignToOffset(align_offset, buf);
			*(((unsigned char*)buf) - 1) = (unsigned char)(align_offset);
		}

		return buf;
	}

	return NULL;
}

void mk1_aligned_free(void* buf, void (*free_f)(void * buf))
{
	if(buf != NULL)
	{
		const size_t align_offset = *(((unsigned char*)buf) - 1);

		free_f((unsigned char*)buf - align_offset);
	}
}

/*** Page allocator ***/

/*
	Number of leading zeroes, Harley's algorithm.
	As found in 'Hacker's Delight' by Henry S. Warren, Jr..
*/
static int_32 nlz(uint_32 u)
{
	static char _nlz[64] = {
		32, 31, -1, 16, -1, 30, 3, -1, 15, -1, -1, -1, 29, 10, 2, -1,
		-1, -1, 12, 14, 21, -1, 19, -1, -1, 28, -1, 25, -1, 9, 1, -1,
		17, -1, 4, -1,-1, -1, 11, -1, 13, 22, 20, -1, 26, -1, -1, 18,
		5, -1, -1, 23, -1, 27, -1, 6, -1, 24, 7, -1, 8, -1, 0, -1
	};

	u = u | (u >> 1);
	u = u | (u >> 2);
	u = u | (u >> 4);
	u = u | (u >> 8);
	u = u | (u >> 16);
	u = u * 0x06eb14f9;

	return _nlz[u >> 26];
}

/* Defined for (x > 0) */
static uint_32 _log2(uint_32 x)
{
	return (31 - (uint_32)nlz(x));
}

static uint_32 rs_best_fit_page_bucket_index(const size_t req_size)
{
	return _log2(ceilP2((uint_32)req_size));
}

static d_array_t rs_create_page_bucket(void)
{
	return da_create(1, sizeof(void*));
}

static void rs_delete_page_bucket(d_array_t b)
{
	if(b != NULL) {
		const uint_32 n_pages = (uint_32)da_num_items(b);
		void** pages = (void**)da_buffer(b);
		uint_32 i;

		for(i = 0; i < n_pages; ++i)
			free(pages[i]);

		da_delete(b);
	}
}

struct rs_page {
	void* page;
	size_t free_size;
};

struct rs_page_buckets {
	struct rs_page curr_page;
	d_array_t free[32];
	d_array_t used[32];
	size_t min_page_size;
	size_t total_alloc_req;
	size_t total_alloc_waste;
};

static struct rs_page rs_null_page(void)
{
	struct rs_page null_page = { NULL, 0 };

	return null_page;
}

static void rs_page_buckets_init(const size_t min_page_size, page_allocator_t pa)
{
	pa->curr_page = rs_null_page();
	memset(pa->free, 0, sizeof(pa->free));
	memset(pa->used, 0, sizeof(pa->used));
	pa->min_page_size = min_page_size;
	pa->total_alloc_req = 0;
	pa->total_alloc_waste = 0;
}

static void rs_delete_parallel_page_buckets(const size_t i, page_allocator_t pa)
{
	rs_delete_page_bucket(pa->free[i]);
	pa->free[i] = NULL;
	rs_delete_page_bucket(pa->used[i]);
	pa->used[i] = NULL;
}

static void rs_page_buckets_delete(page_allocator_t pa)
{
	uint_32 i;

	for(i = 0; i < 32; ++i)
		rs_delete_parallel_page_buckets(i, pa);
}

#define PAGE_SIZE_FROM_BUCKET_INDEX(i) (((size_t)(1)) << ((size_t)(i)))
static void* rs_best_fit_free_page(const size_t req_size, page_allocator_t pa, size_t* page_size)
{
	uint_32 i;

	for(i = rs_best_fit_page_bucket_index(req_size); i < 32; ++i) {
		const uint_32 n_pages = da_num_items(pa->free[i]);
		void* page = NULL;

		if((n_pages > 0) && \
			(da_remove(n_pages - 1, &page, pa->free[i]) == DA_OK) && \
			(da_add(&page, pa->used[i]) == DA_OK)) {
			*page_size = PAGE_SIZE_FROM_BUCKET_INDEX(i);

			return page;
		}
	}

	return NULL;
}

static int rs_create_parallel_page_buckets(const size_t bucket_i, page_allocator_t pa)
{
	if(pa->free[bucket_i] == NULL) {
		pa->free[bucket_i] = rs_create_page_bucket();
		pa->used[bucket_i] = rs_create_page_bucket();

		if((pa->free[bucket_i] == NULL) || (pa->used[bucket_i] == NULL)) {
			rs_delete_parallel_page_buckets(bucket_i, pa);

			return 0;
		}
	}

	return 1;
}

#ifndef MAX
#define MAX(x, y) ((x) > (y) ? (x) : (y))
static void* rs_create_page(const size_t req_size, page_allocator_t pa, size_t* _page_size)
{
	const size_t page_size = MAX(ceilP2(req_size), pa->min_page_size);
	const uint_32 bucket_i = rs_best_fit_page_bucket_index(page_size);
	void* page;

	/* Lazy page bucket creation */
	if(rs_create_parallel_page_buckets(bucket_i, pa)) {
		page = malloc(page_size);
		if(page != NULL) {
			if(da_add(&page, pa->used[bucket_i]) == DA_OK) {
				*_page_size = page_size;
				return page;
			}

			free(page);
		}
	}

	return NULL;
}
#undef MAX
#endif

static void* rs_request_page(const size_t req_size, page_allocator_t pa, size_t* page_size)
{
	void* page = rs_best_fit_free_page(req_size, pa, page_size);

	return (page != NULL) ? page : rs_create_page(req_size, pa, page_size);
}

static void rs_reset_page_buckets(page_allocator_t pa)
{
	uint_32 i;

	for(i = 0; i < 32; ++i) {
		if(pa->used[i] != NULL) {
			d_array_t used = pa->used[i];

			da_addn(da_buffer(used), da_num_items(used), pa->free[i]);
			da_truncate(0, used);
		}
	}
}

page_allocator_t page_allocator_create(const size_t min_page_size)
{
	page_allocator_t pa = (page_allocator_t)malloc(sizeof(struct rs_page_buckets));

	if(pa != NULL)
		rs_page_buckets_init(min_page_size, pa);

	return pa;
}

void page_allocator_delete(page_allocator_t pa)
{
	if(pa != NULL) {
		rs_page_buckets_delete(pa);
		free(pa);
	}
}

static struct rs_page rs_next_page(size_t size, page_allocator_t pa)
{
	struct rs_page page;

	page.page = rs_request_page(size, pa, &(page.free_size));
	if(page.page != NULL)
		return page;

	return rs_null_page();
}

static void* rs_alloc_in_page(const size_t size, struct rs_page* page)
{
	void* buf = page->page;

	page->page = (char*)(page->page) + size;
	page->free_size -= size;

	return buf;
}

void* paged_alloc(const size_t size, const size_t align, page_allocator_t pa)
{
	if(pa != NULL) {
		const size_t alloc_size = size + (align - 1);
		const size_t curr_free_size = pa->curr_page.free_size;

		if(alloc_size > pa->curr_page.free_size) {
			pa->curr_page = rs_next_page(alloc_size, pa);
			if(pa->curr_page.page == NULL)
				return NULL;

			pa->total_alloc_waste += curr_free_size;
		}

		pa->total_alloc_req += alloc_size;

		return (void*)alignTo(align, rs_alloc_in_page(alloc_size, &(pa->curr_page)));
	}

	return NULL;
}

void page_allocator_reset(page_allocator_t pa)
{
	if(pa != NULL) {
		pa->curr_page = rs_null_page();
		rs_reset_page_buckets(pa);
		pa->total_alloc_req = 0;
		pa->total_alloc_waste = 0;
	}
}


static void rs_page_totals(const page_allocator_t pa, struct page_allocator_info* info)
{
	uint_32 i;

	for(i = 0; i < 32; ++i) {
		info->page_buckets[i].free = da_num_items(pa->free[i]);
		info->page_buckets[i].used = da_num_items(pa->used[i]);
	}

	info->total_alloc_req = pa->total_alloc_req;
	info->total_alloc_waste = pa->total_alloc_waste;
}

struct page_allocator_info page_allocator_info(page_allocator_t pa)
{
	struct page_allocator_info info;

	memset(&info, 0, sizeof(info));
	if(pa != NULL)
		rs_page_totals(pa, &info);

	return info;
}

void page_allocator_info_dump(const page_allocator_t pa)
{
	struct page_allocator_info info = page_allocator_info(pa);
	uint_32 i;

	printf("page allocator info:\n");
	for(i = 0; i < 32; ++i) {
		printf("\t");
		printf("%lu free, %lu used - page size (%lu)\n", info.page_buckets[i].free, info.page_buckets[i].used, \
				PAGE_SIZE_FROM_BUCKET_INDEX(i));
	}

	printf("\t%lu total req alloc size\n", info.total_alloc_req);
	printf("\t%lu total alloc wasted\n", info.total_alloc_waste);
	printf("\n");
}

