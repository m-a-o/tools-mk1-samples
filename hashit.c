/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "d_hash_t2.h"
#include "d_array.h"


int main(int argc, char* argv[])
{

	FILE* fp;
	char strbuf[1024];
	dh_table_t map;
	dh_ipair_t ipair;
	int_32 total_word_count = 0;

	if(argc < 2) {
		printf("usage: hashit <filename>\n");
		return -1;
	}

	if((fp = fopen(argv[1], "rt")) == NULL) {
		printf("error opening file <%s>\n", argv[1]);
		return -2;
	}

	map = dh_createv(10, DH_DYNAMIC);

	while(fscanf(fp, "%s", strbuf) != EOF) {
		int_32 word_sz = strlen(strbuf) + 1;
		int_32 word_count;

		if(dh_ipairv(strbuf, word_sz, map, &ipair) == DH_NOT_FOUND) {
			word_count = 1;
			dh_addv(strbuf, word_sz, &word_count, sizeof(word_count), map);
		}

		else {
			dh_ipair_get(NULL, NULL, &word_count, NULL, &ipair);
			++word_count;
			dh_ipair_updatev(&word_count, sizeof(word_count), &ipair);
		}

		++total_word_count;
	}

	fclose(fp);

	{
		dh_table_t freq_map = dh_createf(10, sizeof(int_32), sizeof(d_array_t), DH_DYNAMIC);
		dh_ipair_t ipair_freq;
		int_32 word_count, word_sz;
		d_array_t words;

		for(ipair = dh_ipair_begin(map); dh_ipair_chk(&ipair) == DH_OK; dh_ipair_next(&ipair)) {
			dh_ipair_get(strbuf, &word_sz, &word_count, NULL, &ipair);

			if(dh_ipairf(&word_count, freq_map, &ipair_freq) == DH_NOT_FOUND) {
				words = da_create(word_sz, sizeof(char));

				da_addn(strbuf, word_sz - 1, words);
				dh_addf(&word_count, &words, freq_map);
			}

			else {
				dh_ipair_get(NULL, NULL, &words, NULL, &ipair_freq);
				da_cat_formatted(words, ", %s", strbuf);
			}
		}

		printf("-=-=-= freq map =-=-=-\n\n");

		for(ipair = dh_ipair_begin(freq_map); dh_ipair_chk(&ipair) == DH_OK; dh_ipair_next(&ipair)) {
			dh_ipair_get(&word_count, NULL, &words, NULL, &ipair);

			printf("%d / %s\n", word_count, da_buffer_str(words));
			da_delete(words);
		}

		dh_delete(freq_map);
	}

	dh_delete(map);

	printf("\n-=-=-= read a total of %d words =-=-=-\n", total_word_count);

	return 0;

}

