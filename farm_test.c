/*
	-=- Farm parallelism pattern tests -=-
*/

/*
	Copyright (c) 2010-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stddef.h>

#include <unistd.h>

#include <pthread.h>

#include "d_array.h"
#include "farm.h"


#define N_JOBS 4
#define SLEEP_TIME 2	/* In seconds */

int worker(int id, void* data)
{
	printf("worker %d - data: %p\n", id, data);
	sleep(SLEEP_TIME * id);
	printf("worker %d done!\n", id);

	return(0);
}

#if 1
int main(int argc, char* argv[])
{

	int i;
	farm_payload_t* payload;
	farm_sync_t* my_farm = farm_create(N_JOBS);
	printf("farm status is: %d\n", farm_check(my_farm));

	/* Jobs */
	payload = (farm_payload_t*)malloc(N_JOBS * sizeof(farm_payload_t));
	for(i = 0; i < N_JOBS; ++i) {
		payload[i].data = &payload[i];
		payload[i].job = worker;
	}

	farm_start(my_farm, payload, 0);
	printf("farm spawning thread here!\n");

	/* Wait for farm */
	for(i = 0; farm_check(my_farm) != FARM_STAT_DONE; ++i) {
		printf("farm spawning thread waiting on farm... %d\n", i);
		sleep(1);
	}

	printf("all done!\n");

	return 0;

}
#else
int main(int argc, char* argv[])
{
	farm_sync_t* farm;
	int job_data[N_JOBS];

	farm = farm_create(N_JOBS);

	farm_go(farm, job_data, worker, FARM_OPT_NO_WAIT);

#if 0
	farm_wait_for(farm);
#else
	{
		int i;

		/* Wait for farm */
		for(i = 0; farm_check(farm) != FARM_STAT_DONE; ++i) {
			printf("farm spawning thread waiting on farm... %d\n", i);
			sleep(1);
		}
	}
#endif

	farm_delete(farm);

	return 0;
}
#endif

