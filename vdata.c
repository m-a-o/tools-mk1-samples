/*	Vector data descriptors.
	Author: Miguel A. Osorio - Mar 20 2016
*/

/*
	Copyright (c) 2016-2017 Miguel A. Osorio

	This software is provided 'as-is', without any express
	or implied warranty. In no event will the authors be held
	liable for any damages arising from the use of this software.

	Permission is granted to anyone to use this software for any
	purpose, including commercial applications, and to alter it
	and redistribute it freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented;
	you must not claim that you wrote the original software. If
	you use this software in a product, an acknowledgment in the
	product documentation would be appreciated but is not required.

	2. Altered source versions must be plainly marked as such,
	and must not be misrepresented as being the original software.

	3. This notice may not be removed or altered from any source
	distribution.

	Miguel A. Osorio
	osorio.ma@gmail.com
*/




#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#ifdef MK1_ENABLE_EXTRA_CHECKS
#include <assert.h>
#endif

#include "mk1_aligned_alloc.h"
#include "custom_types_align.h"
#include "d_array.h"
#include "hash_64.h"
#include "d_hash_t2.h"
#include "vdata.h"




/*
	Please note that the funtion pointer added by
	MK1_ENABLE_EXTRA_CHECKS ruins the pure data
	approach of va_block_t's!
*/
struct vector_array_descriptor {
	va_size_t vas_size;
	struct size_align_pair decl_sz_al;
	va_size_t sizeof_vas;
	va_size_t alignof_vas;
#ifdef MK1_ENABLE_EXTRA_CHECKS
	va_decl_to_sz_al_mapper sz_align;
#endif
};

DEFINE_ALIGNOF_STRUCT(vector_array_decl);
DEFINE_ALIGNOF_TYPE(va_size_t);
DEFINE_ALIGNOF_STRUCT(vector_array_descriptor);
DEFINE_ALIGNOF_TYPE(char);

static const struct size_align_pair VAT_SZ_AL[] = {
	{ sizeof(float_32), ALIGNOF(float_32) },
	{ sizeof(int_8), ALIGNOF(int_8) },
	{ sizeof(uint_8), ALIGNOF(uint_8) },
	{ sizeof(int_16), ALIGNOF(int_16) },
	{ sizeof(uint_16), ALIGNOF(uint_16) },
	{ sizeof(float_32), ALIGNOF(float_32) },

	{ sizeof(float_32) * 2,	ALIGNOF(float_32) },
	{ sizeof(float_32) * 3,	ALIGNOF(float_32) },
	{ sizeof(float_32) * 4,	ALIGNOF(float_32) },

	{ sizeof(float_32) * 3 * 3, ALIGNOF(float_32) },
	{ sizeof(float_32) * 4 * 4, ALIGNOF(float_32) },

	{ sizeof(uint_32), ALIGNOF(uint_32) }
};

static struct size_align_pair va_decl_to_sz_al_default(const void* va_decl)
{
	const struct vector_array_type va_type = ((const struct vector_array_decl*)va_decl)->type;
	struct size_align_pair sz_al = VAT_SZ_AL[va_type.type];

	sz_al.size *= va_type.size;

	return sz_al;
}

struct va_decl_pack va_decl_pack_default(void)
{
	static const struct va_decl_pack _default = { { sizeof(struct vector_array_decl), ALIGNOF(vector_array_decl) }, va_decl_to_sz_al_default };

	return _default;
}

static struct size_align_pair dc_cmd_sz_align(const void* va_decl)
{
	return *((struct size_align_pair*)va_decl);
}

DEFINE_ALIGNOF_STRUCT(size_align_pair);
struct va_decl_pack va_decl_pack_size_align_pair(void)
{
	static const struct va_decl_pack _default = {
		{ sizeof(struct size_align_pair), ALIGNOF(size_align_pair) }, dc_cmd_sz_align
	};

	return _default;
}

/* Vector array packing */
static struct packed_array va_decl_to_packed_array(const void* va_decl, va_decl_to_sz_al_mapper decl_to_sz_al)
{
	struct size_align_pair sz_al = decl_to_sz_al(va_decl);
	struct packed_array p_head;

	p_head.size = sz_al.size;
	p_head.alignment = sz_al.align;

	return p_head;
}

static va_size_t va_max(const va_size_t a, const va_size_t b)
{
	return (a > b) ? a : b;
}

typedef void (*pa_offset_and_walk_func)(const va_size_t i, va_size_t* offsets, const struct packed_array vat_pa, struct packed_array_it* it);
static struct size_align_pair sizealignof_vas_helper(
	va_size_t* offsets,
	pa_offset_and_walk_func pa_offset_and_walk,
	const char* vas,
	const va_size_t vas_size,
	const struct va_decl_pack decl_pack)
{
	struct size_align_pair sz_al = { 0, 0 };
	struct packed_array_it it = packed_array_it_start((void*)0);
	va_size_t i;

	for(i = 0; i < vas_size; ++i) {
		const struct packed_array vat_pa = va_decl_to_packed_array(vas + (decl_pack.decl_sz_al.size * i), decl_pack.decl_to_sz_al);

		pa_offset_and_walk(i, offsets, vat_pa, &it);
		sz_al.align = va_max(sz_al.align, vat_pa.alignment);
	}

	sz_al.size = (it.offset + offsetToAligned(sz_al.align, (void*)(it.offset)));

	return sz_al;
}

static struct va_decl_pack VA_DECL_PACK_OR_DEFAULT(const struct va_decl_pack decl_pack)
{
	return (decl_pack.decl_to_sz_al != NULL) ? decl_pack : va_decl_pack_default();
}

static void sizealignof_vas_pa_offset_and_walk(const va_size_t i, va_size_t* offsets, const struct packed_array vat_pa, struct packed_array_it* it)
{
	packed_array_walk(vat_pa, it);
}

struct size_align_pair sizealignof_vas(const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack)
{
	return sizealignof_vas_helper(NULL, sizealignof_vas_pa_offset_and_walk, vas, vas_size, VA_DECL_PACK_OR_DEFAULT(decl_pack));
}

static void offsetof_vas_pa_offset_and_walk(const va_size_t i, va_size_t* offsets, const struct packed_array vat_pa, struct packed_array_it* it)
{
	offsets[0] = packed_array_walk(vat_pa, it);
}

va_size_t offsetof_vas(const va_size_t i, const void* vas, const struct va_decl_pack decl_pack)
{
	va_size_t offset;

	sizealignof_vas_helper(&offset, offsetof_vas_pa_offset_and_walk, vas, i + 1, VA_DECL_PACK_OR_DEFAULT(decl_pack));

	return offset;
}

static void offsetsof_vas_pa_offset_and_walk(const va_size_t i, va_size_t* offsets, const struct packed_array vat_pa, struct packed_array_it* it)
{
	offsets[i] = packed_array_walk(vat_pa, it);
}

void offsetsof_vas(va_size_t* offsets, const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack)
{
	sizealignof_vas_helper(offsets, offsetsof_vas_pa_offset_and_walk, vas, vas_size, VA_DECL_PACK_OR_DEFAULT(decl_pack));
}

/* (struct vector_array_descriptor) packing */
static struct packed_array desc_header_to_packed_array(void)
{
	struct packed_array packed_va_desc_head;

	packed_va_desc_head.size = sizeof(struct vector_array_descriptor);
	packed_va_desc_head.alignment = ALIGNOF(vector_array_descriptor);

	return packed_va_desc_head;
}

/* (va_decl)[] packing */
static struct packed_array vas_to_packed_array(const va_size_t size, const struct size_align_pair decl_sz_al)
{
	struct packed_array packed_va_decl;

	packed_va_decl.size = decl_sz_al.size * size;
	packed_va_decl.alignment = decl_sz_al.align;

	return packed_va_decl;
}

/* va_size_t[] offsets to data arrays packing */
static struct packed_array offsets_to_packed_array(const va_size_t size)
{
	struct packed_array packed_offsets;

	packed_offsets.size = sizeof(va_size_t) * size;
	packed_offsets.alignment = ALIGNOF(va_size_t);

	return packed_offsets;
}

/*
	va_desc_t packed arrays layout {

		struct vector_array_desc header;
		va_decl vas[vas_size];
		va_size_t va_offsets[vas_size];

	}
*/

static va_size_t walk_desc_header(const va_size_t vas_size, const struct size_align_pair decl_sz_al, struct packed_array_it* it)
{
	return packed_array_walk(desc_header_to_packed_array(), it);
}

static va_size_t walk_vas(const va_size_t vas_size, const struct size_align_pair decl_sz_al, struct packed_array_it* it)
{
	return packed_array_walk(vas_to_packed_array(vas_size, decl_sz_al), it);
}

static va_size_t walk_offsets(const va_size_t vas_size, const struct size_align_pair decl_sz_al, struct packed_array_it* it)
{
	return packed_array_walk(offsets_to_packed_array(vas_size), it);
}

enum {
	VA_DESC_HEADER = 0,
	VA_DESC_VAS,
	VA_DESC_OFFSETS,

	NUM_PACKED_ARRAYS
};

typedef va_size_t (*packed_array_walker)(const va_size_t vas_size, const struct size_align_pair decl_sz_al, struct packed_array_it* it);
static struct packed_array_it walk_packed_array(const va_size_t array_index, const va_size_t vas_size, const struct size_align_pair decl_sz_al, va_size_t* array_offset)
{
	static const packed_array_walker array_walker[] = {
		walk_desc_header,
		walk_vas,
		walk_offsets,
	};
	const void* ref_addr = (void*)0; /* Enforce constant layouts */
	struct packed_array_it it = packed_array_it_start(ref_addr);
	va_size_t i;

	/*
		TODO: can we force loop unrolling here? Maybe loop with NUM_PACKED_ARRAYS as a max, and test
		inside + break? Try compiling to assembler and check for differences!
	*/
	for(i = 0; i <= array_index; ++i)
		*array_offset = array_walker[i](vas_size, decl_sz_al, &it);

	return it;
}

static va_size_t packed_array_offset(const va_size_t i, const va_size_t vas_size, const struct size_align_pair decl_sz_al)
{
	va_size_t offset;

	walk_packed_array(i, vas_size, decl_sz_al, &offset);

	return offset;
}

static void* packed_array_pointer(const va_size_t i, const va_desc_t va_desc)
{
	return (char*)va_desc + packed_array_offset(i, va_desc->vas_size, va_desc->decl_sz_al);
}

static va_size_t alignof_va_desc(const struct size_align_pair decl_sz_al)
{
	const va_size_t max_align = va_max(desc_header_to_packed_array().alignment, vas_to_packed_array(1, decl_sz_al).alignment);

	return va_max(max_align, offsets_to_packed_array(1).alignment);
}

static struct size_align_pair sizealignof_va_desc_helper(const va_size_t vas_size, const struct size_align_pair decl_sz_al)
{
	va_size_t offset;
	struct packed_array_it it = walk_packed_array(VA_DESC_OFFSETS, vas_size, decl_sz_al, &offset);
	struct size_align_pair sz_al;
	const va_size_t va_desc_alignof = alignof_va_desc(decl_sz_al);

	sz_al.align = va_desc_alignof;
	sz_al.size = it.offset + offsetToAligned(va_desc_alignof, (void*)(it.offset));

	return sz_al;
}

struct size_align_pair sizealignof_va_desc(const va_desc_t va_desc)
{
	return sizealignof_va_desc_helper(va_desc->vas_size, va_desc->decl_sz_al);
}

void* va_desc_vas(const va_desc_t va_desc)
{
	return packed_array_pointer(VA_DESC_VAS, va_desc);
}

va_size_t va_desc_vas_size(const va_desc_t va_desc)
{
	return va_desc->vas_size;
}

va_size_t va_desc_array_offset(const va_size_t i, const va_desc_t va_desc)
{
	return ((const va_size_t*)packed_array_pointer(VA_DESC_OFFSETS, va_desc))[i];
}

va_size_t va_desc_sizeof_vas(const va_desc_t va_desc)
{
	return va_desc->sizeof_vas;
}

va_size_t va_desc_alignof_vas(const va_desc_t va_desc)
{
	return va_desc->alignof_vas;
}

static va_desc_t va_desc_init(void* buf, const va_size_t buf_size, const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack)
{
	va_desc_t va_desc = (va_desc_t)buf;
	va_size_t* va_desc_offsets = (va_size_t*)((char*)va_desc + packed_array_offset(VA_DESC_OFFSETS, vas_size, decl_pack.decl_sz_al));
	struct size_align_pair vas_sz_al;

	memset(va_desc, 0, buf_size);
	va_desc->vas_size = vas_size;
	va_desc->decl_sz_al = decl_pack.decl_sz_al;
	vas_sz_al = sizealignof_vas_helper(va_desc_offsets, offsetsof_vas_pa_offset_and_walk, vas, vas_size, decl_pack);
	va_desc->sizeof_vas = vas_sz_al.size;
	va_desc->alignof_vas = vas_sz_al.align;
#ifdef MK1_ENABLE_EXTRA_CHECKS
	va_desc->sz_align = decl_pack.decl_to_sz_al;
#endif
	memcpy(va_desc_vas(va_desc), vas, va_desc->vas_size * decl_pack.decl_sz_al.size);

	return va_desc;
}

va_desc_t va_desc_create(const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack)
{
	if((vas != NULL) && (vas_size > 0)) {
		const struct va_decl_pack _decl_pack = VA_DECL_PACK_OR_DEFAULT(decl_pack);
		const struct size_align_pair desc_sz_al = sizealignof_va_desc_helper(vas_size, _decl_pack.decl_sz_al);
		void* buffer = mk1_aligned_alloc(desc_sz_al.size, desc_sz_al.align, malloc);

		if(buffer != NULL)
			return va_desc_init(buffer, desc_sz_al.size, vas, vas_size, _decl_pack);
	}

	return NULL;
}

va_desc_t va_desc_clone(const va_desc_t src)
{
	if(src != NULL) {
		const struct size_align_pair src_sz_al = sizealignof_va_desc(src);
		void* buffer = mk1_aligned_alloc(src_sz_al.size, src_sz_al.align, malloc);

		if(buffer != NULL) {
			memcpy(buffer, src, src_sz_al.size);

			return (va_desc_t)buffer;
		}
	}

	return NULL;
}

void va_desc_delete(va_desc_t va_desc)
{
	mk1_aligned_free(va_desc, free);
}

struct vector_array_instance {
	struct vector_array_descriptor desc;
};

static struct size_align_pair sizealignof_vec_array_helper(const va_size_t vas_size, const struct size_align_pair decl_sz_al, const va_size_t vas_sizeof, const va_size_t vas_alignof)
{
	const struct size_align_pair header_sz_al = sizealignof_va_desc_helper(vas_size, decl_sz_al);
	struct size_align_pair vec_array_sz_al;

	vec_array_sz_al.size = header_sz_al.size + offsetToAligned(vas_alignof, (void*)(size_t)(header_sz_al.size)) + vas_sizeof;
	vec_array_sz_al.align = va_max(header_sz_al.align, vas_alignof);

	return vec_array_sz_al;
}

struct size_align_pair sizealignof_vec_array_ex(const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack)
{
	const struct size_align_pair vas_sz_al = sizealignof_vas(vas, vas_size, VA_DECL_PACK_OR_DEFAULT(decl_pack));

	return sizealignof_vec_array_helper(vas_size, decl_pack.decl_sz_al, vas_sz_al.size, vas_sz_al.align);
}

struct size_align_pair sizealignof_vec_array(const vec_array_t vec_array)
{
	return sizealignof_vec_array_helper(vec_array->desc.vas_size, vec_array->desc.decl_sz_al, vec_array->desc.sizeof_vas, vec_array->desc.alignof_vas);
}

void* vec_array_store(const vec_array_t vec_array)
{
	return (char*)vec_array + sizealignof_vec_array_helper(vec_array->desc.vas_size, vec_array->desc.decl_sz_al, 0, vec_array->desc.alignof_vas).size;
}

void* vec_array_get(const va_size_t i, const vec_array_t vec_array)
{
	void* va = (char*)vec_array_store(vec_array) + va_desc_array_offset(i, &(vec_array->desc));

#ifdef MK1_ENABLE_EXTRA_CHECKS
	struct size_align_pair sz_al = vec_array->desc.sz_align((char*)va_desc_vas(&(vec_array->desc)) + (i * vec_array->desc.decl_sz_al.size));

	assert(offsetToAligned(sz_al.align, va) == 0);
#endif

	return va;
}

vec_array_t vec_array_in_place(void* buffer, const void* vas, const va_size_t vas_size, const struct size_align_pair sz_al, const struct va_decl_pack decl_pack)
{
	if(buffer != NULL)
		return (vec_array_t)va_desc_init(buffer, sz_al.size, vas, vas_size, VA_DECL_PACK_OR_DEFAULT(decl_pack));

	return NULL;
}

vec_array_t vec_array_create(const void* vas, const va_size_t vas_size, const struct va_decl_pack decl_pack)
{
	if((vas != NULL) && (vas_size > 0)) {
		const struct size_align_pair vec_array_sz_al = sizealignof_vec_array_ex(vas, vas_size, decl_pack);
		void* buffer = mk1_aligned_alloc(vec_array_sz_al.size, vec_array_sz_al.align, malloc);

		return vec_array_in_place(buffer, vas, vas_size, vec_array_sz_al, decl_pack);
	}

	return NULL;
}

void vec_array_delete(vec_array_t vec_array)
{
	va_desc_delete((va_desc_t)vec_array);
}

struct vec_array_compiler {
	d_array_t vas;
	struct va_decl_pack decl_pack;
};

va_compiler_t va_compiler_create(const struct va_decl_pack decl_pack)
{
	va_compiler_t vac = (va_compiler_t)malloc(sizeof(struct vec_array_compiler));

	if(vac != NULL) {
		vac->vas = da_create(1, decl_pack.decl_sz_al.size);
		if(vac->vas != NULL) {
			vac->decl_pack = decl_pack;

			return vac;
		}

		free(vac);
	}

	return NULL;
}

void va_compiler_delete(va_compiler_t vac)
{
	if(vac != NULL) {
		da_delete(vac->vas);
		free(vac);
	}
}

va_size_t va_compiler_add(const void* vas, const va_size_t vas_size, va_compiler_t vac)
{
	if((vas != NULL) && (vas_size > 0) && (vac != NULL)) {
		const mem_int_t offset = da_num_items(vac->vas);

		if(da_addn(vas, vas_size, vac->vas) == DA_OK)
			return (va_size_t)offset;
	}

	return VA_INVALID_INDEX;
}

va_desc_t va_desc_compile(va_compiler_t vac)
{
	if(vac != NULL)
		return va_desc_create(da_buffer(vac->vas), da_num_items(vac->vas), vac->decl_pack);

	return NULL;
}

vec_array_t va_compile(va_compiler_t vac)
{
	if(vac != NULL)
		return vec_array_create(da_buffer(vac->vas), da_num_items(vac->vas), vac->decl_pack);

	return NULL;
}

void va_compiler_reset(va_compiler_t vac)
{
	if(vac != NULL)
		da_clear(vac->vas);
}

/* Vector array type namespaces */
struct va_type_namespace {
	dh_table_t name_to_id;
	va_name_t next_name;
};

vat_namespace vat_names_create(void)
{
	vat_namespace vat_names = (vat_namespace)malloc(sizeof(struct va_type_namespace));

	if(vat_names != NULL) {
		vat_names->name_to_id = dh_createv(1, DH_DYNAMIC);
		if(vat_names->name_to_id != NULL) {
			vat_names->next_name = 1;

			return vat_names;
		}

		free(vat_names);
	}

	return NULL;
}

void vat_names_delete(vat_namespace vat_names)
{
	if(vat_names != NULL) {
		dh_delete(vat_names->name_to_id);
		free(vat_names);
	}
}

/* 0 is NULL id! */
va_name_t vat_name_to_id(const char* name, const vat_namespace vat_names)
{
	va_name_t id;

	if((vat_names != NULL) && (dh_getv(name, strlen(name), &id, NULL, vat_names->name_to_id) == DH_OK))
		return id;

	return 0;
}

va_name_t vat_register_name_to_id(const char* name, vat_namespace vat_names)
{
	va_name_t id = vat_name_to_id(name, vat_names);

	if(id == 0) {
		if(dh_addv(name, strlen(name), &(vat_names->next_name), sizeof(vat_names->next_name), vat_names->name_to_id) == DH_OK) {
			id = vat_names->next_name;
			++(vat_names->next_name);
		}
	}

	return id;
}

int vat_names_map_to_vas(struct vector_array_decl* vas, const va_size_t vas_size, const char* names[], vat_namespace vat_names)
{
	va_size_t i;

	for(i = 0; i < vas_size; ++i) {
		const va_name_t id = vat_register_name_to_id(names[i], vat_names);

		if(id == 0)
			return 0;

		vas[i].name.id = id;
	}

	return 1;
}

